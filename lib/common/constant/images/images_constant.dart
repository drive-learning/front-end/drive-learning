class ImagesConstant {
  static final listSetQuestionLogo = [
    "assets/images/categories/moto_01.png",
    "assets/images/categories/moto_02.png",
    "assets/images/categories/moto_03.png",
    "assets/images/categories/moto_04.png",
  ];

  static final bannerbackground = "assets/images/banner/banner_01.png";
  static final bgNoticeFinish = "assets/images/banner/bg_notice_finish.png";
  static final bgHomeRecomendationBanner =
      "assets/images/home/home_recommendation_banner.png";

  static final listLearningBanners = [
    "assets/images/learning_banner/part_1.png",
    "assets/images/learning_banner/part_2.png",
    "assets/images/learning_banner/part_3.png"
  ];

  static final icPlay = "assets/images/home/ic_play.png";

  static final imgHistoriesBanner = "assets/images/learning_banner/part_2.png";

  static final imOnBoardingFirstPage =
      "assets/images/on_boarding/on_boarding_1st.png";
  static final imOnBoardingSecondPage =
      "assets/images/on_boarding/on_boarding_2nd.png";
  static final imOnBoardingThirtPage =
      "assets/images/on_boarding/on_boarding_3th.png";
  static final imOnBoardingFouthPage =
      "assets/images/on_boarding/on_boarding_4th.png";

  static final bgUserName = "assets/images/banner/bg_user_name.png";
  static final bgUserAge = "assets/images/banner/bg_user_age.png";
  static final bgUserLicense = "assets/images/banner/bg_user_license.png";
  static final bgUserGender = "assets/images/banner/bg_gender.png";

  static final bgProfilePlacholder =
      "assets/images/banner/bg_profile_placeholder.png";

  static final bgEmptyState = "assets/images/banner/bg_empty_state.png";
}
