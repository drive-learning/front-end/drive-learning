class IconsConstant {
  static final ic_1st = "assets/icons/ranking/1st.png";
  static final ic_2rd = "assets/icons/ranking/2rd.png";
  static final ic_3rd = "assets/icons/ranking/3rd.png";

  static final a1Category = "assets/icons/categories/a1_base.png";
  static final a2Category = "assets/icons/categories/a2_base.png";
  static final b2Category = "assets/icons/categories/b2_base.png";

  static final gem = "assets/icons/account/ic_gem.png";
  static final notification = "assets/icons/alert/ic_notification.png";
}
