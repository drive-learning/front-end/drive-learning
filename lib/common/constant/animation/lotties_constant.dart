class Lottiesconstant {
  static final menAvatar = "assets/lottie/home/male_avatar.json";
  static final womenAvatar = "assets/lottie/home/women_avatar.json";
  static final defaultAvatar = "assets/lottie/home/default_avatar.json";
  static final winnerCup = "assets/lottie/home/winner_cup.json";
  static final losedEmotion = "assets/lottie/home/losed_emotion.json";
  static final winnerBackground = "assets/lottie/home/winner_background.json";
  static final bannerQuestions = "assets/lottie/home/banner_questions.json";
  static final buttonPlayNow = "assets/lottie/home/button_play_now.json";
  static final loadingAnimation = "assets/lottie/loading/loading.json";
  static final loadMoreAnimation = "assets/lottie/loading/loadmore.json";
  static final emptyState = "assets/lottie/loading/empty_state.json";

  static final preLoadingBanner = "assets/lottie/loading/loading_banner.json";
  static final preLoadingProgressBar =
      "assets/lottie/loading/loading_processbar.json";
}
