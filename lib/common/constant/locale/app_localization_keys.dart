class AppLocalizationKeys {
  static final String successTitle = "success_title";
  static final String nearestTestHistories = "nearest_test_histories";
  static final String nearestTestHistoriesDesGood =
      "nearest_test_histories_des_good";
  static final String nearestTestMostOfAll = "nearest_test_most_of_all";
  static final String testAgain = "test_again";

  ///`Setting Localization Keys`
  static final String upgrade = "upgrade";
  static final String settings = "settings";
  static final String reset = "reset";
  static final String resetDescription = "reset_description";

  /// question preview
  static final String skip = "skip";
  static final String answer = "answer";
  static final String next = "next";

  static final String completionTest = "completion_test";
  static final String completionTestConfirm = "completion_test_confirm";
  static final String reviewTest = "review_test";

  static final String lastQuestionNotifyTitle = "last_question_notify_title";
  static final String lastQuestionNotifyMes = "last_question_notify_mes";
  static final String listQuestion = "list_question";

  static final String gender = "gender";
  static final String licenseType = "license_type";
}
