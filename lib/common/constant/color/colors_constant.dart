import 'package:flutter/material.dart';

class ColorsContant {
  static final appPrimaryColor = Colors.redAccent;
  static final appTextColor = Colors.grey[850];
  static final appBackgroundColor = Colors.white;

  static final recommendBackgroundColor = Colors.yellow[800];
  static final quickTestBackgroundColor = Colors.green;
  static final viewMoreButtonClor = Colors.yellow[800];

  static final sectionTextColor = Colors.grey[600];

  static final listCategoriesColors = [
    Colors.redAccent,
    Colors.yellow[800],
    Colors.blue[300],
    Colors.cyan[600],
    Colors.purple[900],
    Colors.deepOrange[300],
    Colors.green[400]
  ];

  static final listRecommendationCategoriesColors = [
    Colors.yellow[800],
    Colors.pink[400],
    Colors.teal
  ];

  static final seletedAnswerColor = Colors.lightBlueAccent;
  static final unSelectedAnswerColor = Colors.white;
  static final correctSelectedAnswerColor = Colors.greenAccent[400];
  static final wrongSelectedAnswerColor = Colors.redAccent[400];

  static final selectedAnswerTextColor = Colors.white;
  static final unSelectedAnswerTextColor = Colors.grey[800];

  static final passsedColor = Color(0xff2ECC71);
  static final failedColor = Colors.pinkAccent[400];
}
