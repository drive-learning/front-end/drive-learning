class FontsConstant {
  static final String southerArie = "SouthernAire";
  static final String blackBones = "Black_Bones";
  static final String fireBrathers = "FireBrathers-PersonalUse";
  static final String angelineVintage = "Angeline_Vintage";
  static final String dancingScript = "DancingScript";
}
