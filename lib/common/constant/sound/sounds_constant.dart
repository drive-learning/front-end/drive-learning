class Soundsconstant {
  static final loserSound = "sounds/failed.mp3";
  static final passedSounds = "sounds/passed.mp3";
  static final selectedSounds = "sounds/selected.mp3";
  static final correctSounds = "sounds/correct.mp3";
  static final wrongSounds = "sounds/wrong.mp3";
}
