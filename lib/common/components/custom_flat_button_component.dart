import 'package:flutter/material.dart';

class CustomFlatButtonComponent extends StatelessWidget {
  final Color backgrouundColor;
  final Color titleColor;
  final VoidCallback onTap;
  final String title;
  const CustomFlatButtonComponent(
      {Key key, this.backgrouundColor, this.titleColor, this.onTap, this.title})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: InkWell(
        onTap: () {
          onTap();
        }, // needed
        child: Ink(
          // use Ink
          height: 56,
          decoration: BoxDecoration(
              color: backgrouundColor,
              borderRadius: BorderRadius.circular(16),
              boxShadow: [
                BoxShadow(
                    color: Colors.grey.withOpacity(0.5),
                    offset: Offset(0, 10),
                    blurRadius: 20),
              ],
              border:
                  Border.all(color: Colors.grey.withOpacity(0.7), width: 0.25)),
          child: Center(
            child: Text(
              title.toUpperCase(),
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 16,
                  fontWeight: FontWeight.w600),
            ),
          ),
        ),
      ),
    );
  }
}
