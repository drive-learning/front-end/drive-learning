import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:vn_drive_learning/common/constant/animation/lotties_constant.dart';

class LoadingComponent extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: Lottie.asset(Lottiesconstant.loadingAnimation,
            width: 128, height: 128),
      ),
    );
  }
}
