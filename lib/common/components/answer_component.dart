import 'package:flutter/material.dart';
import 'package:vn_drive_learning/common/constant/color/colors_constant.dart';
import 'package:vn_drive_learning/common/components/question_view_type.dart';
import 'package:vn_drive_learning/services/entity/answer.dart';

class AnswerComponent extends StatelessWidget {
  final Answer answer;
  final bool isSelected;
  final QuestionViewType questionViewType;
  final VoidCallback onTap;

  const AnswerComponent(
      {Key key,
      this.answer,
      this.isSelected,
      this.questionViewType = QuestionViewType.testing,
      this.onTap})
      : super(key: key);

  Color _getBackgroundColor() {
    switch (questionViewType) {
      case QuestionViewType.testing:
        return isSelected
            ? ColorsContant.seletedAnswerColor
            : ColorsContant.unSelectedAnswerColor;
      case QuestionViewType.viewResult:
        if (isSelected) {
          if (answer.isCorrect) {
            return ColorsContant.correctSelectedAnswerColor;
          }

          return ColorsContant.wrongSelectedAnswerColor;
        }

        if (answer.isCorrect) {
          return ColorsContant.correctSelectedAnswerColor;
        }

        return ColorsContant.unSelectedAnswerColor;
    }

    return ColorsContant.unSelectedAnswerColor;
  }

  Color _getTextColor() {
    if (isSelected ||
        (questionViewType == QuestionViewType.viewResult && answer.isCorrect)) {
      return ColorsContant.selectedAnswerTextColor;
    }

    return ColorsContant.unSelectedAnswerTextColor;
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.all(0),
        child: Material(
          color: Colors.transparent,
          child: Container(
            padding:
                const EdgeInsets.only(left: 20, right: 20, top: 10, bottom: 10),
            child: InkWell(
              onTap: () {
                if (questionViewType == QuestionViewType.testing) {
                  onTap();
                }
              },
              child: Ink(
                // height: 56,

                decoration: BoxDecoration(
                    color: _getBackgroundColor(),
                    borderRadius: BorderRadius.circular(8),
                    border: Border.all(
                        color: Colors.blueGrey.withOpacity(0.5), width: 0.25),
                    boxShadow: [
                      BoxShadow(
                          offset: Offset(0, 1),
                          blurRadius: 1,
                          color: Colors.blueGrey.withOpacity(0.25))
                    ]),
                padding: const EdgeInsets.all(20),
                child: Row(
                  children: [
                    Expanded(
                        child: Column(
                      children: [
                        Text(
                          answer.content,
                          style: TextStyle(
                              color: _getTextColor(),
                              fontSize: 18,
                              fontWeight: FontWeight.w400),
                        )
                      ],
                    ))
                  ],
                ),
              ),
            ),
          ),
        ));
  }
}
