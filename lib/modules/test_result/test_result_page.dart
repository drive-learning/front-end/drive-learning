import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:vn_drive_learning/common/constant/color/colors_constant.dart';
import 'package:vn_drive_learning/common/components/custom_flat_button_component.dart';
import 'package:vn_drive_learning/common/constant/animation/lotties_constant.dart';
import 'package:vn_drive_learning/modules/questions_preview/questions_preview_page.dart';
import 'package:vn_drive_learning/modules/questions_preview/strategies/question/question_group_presentation_mode.dart';
import 'package:vn_drive_learning/modules/tabbar/tabbar_view.dart';
import 'package:vn_drive_learning/services/entity/set_question.dart';
import 'package:vn_drive_learning/services/entity/test_result.dart';

class TestResultPageView extends StatefulWidget {
  final TestResult testResult;
  final SetQuestion setQuestion;

  TestResultPageView({@required this.testResult, this.setQuestion});
  @override
  State<StatefulWidget> createState() {
    return _TestResultPageState();
  }
}

class _TestResultPageState extends State<TestResultPageView> {
  @override
  Widget build(Object context) {
    return Scaffold(
        body: Stack(
      children: [
        Column(
          children: [
            Visibility(
              visible: widget.testResult.isPassed,
              child: Container(
                height: 200,
                child: Center(
                    child: Lottie.asset(Lottiesconstant.winnerCup,
                        fit: BoxFit.fitHeight)),
              ),
            ),
            Visibility(
              visible: widget.testResult.isFailed,
              child: Container(
                height: 200,
                child: Center(
                    child: Lottie.asset(Lottiesconstant.losedEmotion,
                        fit: BoxFit.fitHeight, repeat: false)),
              ),
            ),
            Container(
              child: Text(
                widget.testResult.isPassed
                    ? "Chúc mừng BigSUnPham đã hoàn thành bài thi"
                    : "Thử lần sau để tốt hơn thật nhiều bạn nhé <3",
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(20),
              child: Container(
                padding: const EdgeInsets.all(20),
                height: 240,
                width: MediaQuery.of(context).size.width - 40,
                decoration: BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                          color: Colors.grey[800],
                          offset: Offset(0, 10),
                          blurRadius: 20)
                    ],
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(16)),
                child: CircularPercentIndicator(
                  radius: 160,
                  animation: true,
                  backgroundColor: Colors.grey[400],
                  linearGradient: LinearGradient(colors: [
                    Colors.pink[200],
                    Colors.red,
                    Colors.yellow[800],
                    Colors.tealAccent
                  ]),
                  lineWidth: 16,
                  percent: widget.testResult.totalCorrectQuestion /
                      widget.testResult.totalQuestion,
                  circularStrokeCap: CircularStrokeCap.round,
                  center: Text(
                    "${widget.testResult.totalCorrectQuestion}/${widget.testResult.totalQuestion}",
                    style: TextStyle(
                        color: Colors.grey[600],
                        fontSize: 24,
                        fontWeight: FontWeight.bold),
                  ),
                  header: new Text(
                    widget.testResult.isPassed
                        ? "thông qua".toUpperCase()
                        : "Chưa đạt".toUpperCase(),
                    style: new TextStyle(
                        color: widget.testResult.isPassed
                            ? ColorsContant.correctSelectedAnswerColor
                            : ColorsContant.wrongSelectedAnswerColor,
                        fontWeight: FontWeight.bold,
                        fontSize: 16.0),
                  ),
                ),
              ),
            )
          ],
        ),
        Visibility(
          visible: widget.testResult.isPassed,
          child: Container(
            child: Lottie.asset(
              Lottiesconstant.winnerBackground,
              fit: BoxFit.fitHeight,
              animate: true,
              repeat: false,
            ),
          ),
        ),
        _buildPinBottomWidget()
      ],
    ));
  }

  Positioned _buildPinBottomWidget() {
    return Positioned(
        bottom: 0,
        left: 0,
        right: 0,
        height: 100,
        child: Material(
          color: Colors.transparent,
          child: Container(
            child: Container(
              margin: const EdgeInsets.all(20),
              child: Row(children: [
                CustomFlatButtonComponent(
                  backgrouundColor: ColorsContant.wrongSelectedAnswerColor,
                  titleColor: Colors.white,
                  onTap: () {
                    Navigator.push(context, MaterialPageRoute(
                      builder: (context) {
                        return QuestionsPreviewPage(
                          setQuestion: widget.setQuestion,
                          primaryColor: ColorsContant.appPrimaryColor,
                          presentationMode:
                              QuestionGroupPresentationMode.testing,
                        );
                      },
                    ));
                  },
                  title: "Thi lại",
                ),
                SizedBox(
                  width: 24,
                ),
                CustomFlatButtonComponent(
                  backgrouundColor: ColorsContant.correctSelectedAnswerColor,
                  titleColor: Colors.white,
                  onTap: () {
                    Navigator.push(context, MaterialPageRoute(
                      builder: (context) {
                        return MainTabbarView();
                      },
                    ));
                  },
                  title: "Về trang chủ",
                ),
              ]),
              // color: Colors.transparent,
            ),
          ),
        ));
  }
}
