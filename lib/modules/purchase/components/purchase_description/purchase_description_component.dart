import 'package:flutter/material.dart';

class PurchaseDescriptionConponent extends StatelessWidget {
  final String title;
  const PurchaseDescriptionConponent({@required this.title});
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 28, vertical: 8),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Icon(
            Icons.check_circle,
            color: Colors.greenAccent[400],
            size: 24,
          ),
          SizedBox(
            width: 16,
          ),
          Expanded(
            child: Text(title,
                style: TextStyle(
                    color: Colors.grey[800],
                    fontSize: 16,
                    fontWeight: FontWeight.w500)),
          )
        ],
      ),
    );
  }
}
