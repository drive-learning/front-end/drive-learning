import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';
import 'package:vn_drive_learning/modules/purchase/purchase_page.dart';

part 'best_package_component.dart';
part 'normal_package_component.dart';

class _PurchasePackageUIConfig {
  final double componentHeight = 56.0;
  final double bestPackageComponentHeight = 64.0;
  final double borderRadius = 16.0;
  final Color primaryColor = Colors.greenAccent[400];
  final double padding = 16.0;
  final double verticalMargin = 8.0;
  final TextStyle primaryTextStyle = TextStyle(
      color: Colors.greenAccent[400],
      fontWeight: FontWeight.bold,
      fontSize: 16);

  final TextStyle specialTextStyle =
      TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 16);
}

class PurchasePackageComponent extends StatefulWidget {
  final bool isBestPackage;
  final String title;
  final int price;
  String get _priceInString {
    return CurrencyFormatUtils.hanlder().asCurrencyFormat(value: price);
  }

  PurchasePackageComponent(
      {this.isBestPackage = false, @required this.title, @required this.price});

  @override
  State<StatefulWidget> createState() {
    return _PurchasePackageComponentState();
  }
}

class _PurchasePackageComponentState extends State<PurchasePackageComponent>
    with TickerProviderStateMixin {
  final config = _PurchasePackageUIConfig();
  AnimationController _zoomInAmiationController;
  final Tween<double> _tween = Tween(begin: 0.75, end: 1);

  @override
  void initState() {
    super.initState();
    _zoomInAmiationController =
        AnimationController(duration: Duration(milliseconds: 800), vsync: this)
          ..repeat(reverse: true);
  }

  @override
  void dispose() {
    _zoomInAmiationController.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (widget.isBestPackage) {
      return _buildBestPackage();
    }

    return _buildNormalPackage();
  }
}
