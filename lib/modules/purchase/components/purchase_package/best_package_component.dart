part of 'purchase_package_component.dart';

extension _BestPackage on _PurchasePackageComponentState {
  Widget _buildBestPackage() {
    return ScaleTransition(
      scale: _tween.animate(CurvedAnimation(
          parent: _zoomInAmiationController, curve: Curves.elasticOut)),
      // child:

      child: Stack(
        children: [
          Container(
            margin: EdgeInsets.symmetric(vertical: config.verticalMargin + 16),
            padding: EdgeInsets.all(config.padding),
            height: config.bestPackageComponentHeight,
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.centerLeft,
                    end: Alignment.centerRight,
                    colors: [
                      Colors.teal[400],
                      config.primaryColor,
                      config.primaryColor
                    ]),
                borderRadius: BorderRadius.circular(config.borderRadius)),
            child: Row(
              children: [
                Text(widget.title, style: config.specialTextStyle),
                Spacer(),
                Text(widget._priceInString, style: config.specialTextStyle)
              ],
            ),
          ),
          Shimmer(
              child: Container(
                margin:
                    EdgeInsets.symmetric(vertical: config.verticalMargin + 16),
                padding: EdgeInsets.all(config.padding),
                height: config.bestPackageComponentHeight,
                decoration: BoxDecoration(
                    color: config.primaryColor,
                    borderRadius: BorderRadius.circular(config.borderRadius)),
              ),
              gradient: LinearGradient(
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                  stops: [
                    0.4,
                    0.5,
                    0.6
                  ],
                  colors: [
                    Colors.white.withOpacity(0),
                    Colors.white.withOpacity(0.7),
                    Colors.white.withOpacity(0)
                  ])),
          Positioned(
              top: 12,
              left: 64,
              right: 64,
              height: 24,
              child: Center(
                  child: Container(
                padding:
                    const EdgeInsets.symmetric(horizontal: 32, vertical: 4),
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                      begin: Alignment.centerLeft,
                      end: Alignment.centerRight,
                      colors: [
                        Colors.orange[200],
                        Colors.orange[600],
                      ]),
                  borderRadius: BorderRadius.circular(12),
                ),
                child: Text("Best offer",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 12,
                        fontWeight: FontWeight.w600)),
              )))
        ],
      ),
    );
  }
}
