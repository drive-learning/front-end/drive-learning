part of 'purchase_package_component.dart';

extension _NormalPackage on _PurchasePackageComponentState {
  Widget _buildNormalPackage() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: config.verticalMargin),
      padding: EdgeInsets.all(config.padding),
      height: config.componentHeight,
      decoration: BoxDecoration(
          border: Border.all(
            color: config.primaryColor,
            width: 1.5,
          ),
          borderRadius: BorderRadius.circular(config.borderRadius)),
      child: Row(
        children: [
          Text(widget.title, style: config.primaryTextStyle),
          Spacer(),
          Text(
            widget._priceInString,
            style: config.primaryTextStyle,
          )
        ],
      ),
    );
  }
}
