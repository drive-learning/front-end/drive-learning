import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:in_app_purchase/in_app_purchase.dart';
import 'package:intl/intl.dart';
import 'package:vn_drive_learning/main.dart';
import 'package:vn_drive_learning/modules/purchase/components/purchase_description/purchase_description_component.dart';
import 'package:vn_drive_learning/modules/purchase/components/purchase_package/purchase_package_component.dart';
import 'package:vn_drive_learning/services/entity/language_type.dart';
import 'package:vn_drive_learning/services/storage/local_storage_service.dart';

part 'sections/banner_section.dart';
part 'sections/description_section.dart';
part 'sections/purchase_package_section.dart';

class PurchasePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _PurchaseState();
  }
}

class _PurchaseState extends State<PurchasePage> {
  final double viewportFraction = 0.8;
  double pageOffset = 0.0;
  PageController _bannerPageController;

  StreamSubscription<List<PurchaseDetails>> _subscription;
  final InAppPurchaseConnection _connection = InAppPurchaseConnection.instance;
  List<String> _notFoundIds = [];
  List<ProductDetails> _products = [];
  List<PurchaseDetails> _purchases = [];
  List<String> _consumables = [];
  bool _isAvailable = false;
  bool _purchasePending = false;
  bool _loading = true;
  String _queryProductError;

  @override
  void dispose() {
    _bannerPageController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    _bannerPageController =
        PageController(initialPage: 999, viewportFraction: viewportFraction);
    initializePurchaseInApp();
    super.initState();

    Timer.periodic(Duration(milliseconds: 2500), (timer) {
      setState(() {
        if (_bannerPageController.hasClients) {
          pageOffset += 1;
          _bannerPageController.animateToPage(pageOffset.round(),
              duration: Duration(milliseconds: 300), curve: Curves.easeIn);
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: [
          _buildBannerSection(),
          _buildDescriptionsSection(),
          _buildPurchasePackageSection()
        ],
      ),
    );
  }
}

const bool _kAutoConsume = true;

const String _kConsumableId = 'consumable';
const List<String> _kProductIds = <String>[
  _kConsumableId,
  'upgrade',
  'subscription'
];

extension _PurchaseInAppConfiguration on _PurchaseState {
  initializePurchaseInApp() {
    final Stream purchaseUpdates =
        InAppPurchaseConnection.instance.purchaseUpdatedStream;
    _subscription = purchaseUpdates.listen((purchaseDetailsList) {
      _listenToPurchaseUpdated(purchaseDetailsList);
    }, onDone: () {
      _subscription.cancel();
    }, onError: (err) {
      debugPrint("🧨🧨🧨 Error purchase happenned ${err.toString()} 🧨🧨🧨");
    });
    initStoreInfo();
  }
}

extension _ListenerPurchaseUpdated on _PurchaseState {
  void _listenToPurchaseUpdated(List<PurchaseDetails> purchaseDetailsList) {}
}

extension _PurchaseStoreInfo on _PurchaseState {
  Future<void> initStoreInfo() async {
    final bool isAvailable = await _connection.isAvailable();
    final bool isNotAvailable = !isAvailable;
    if (isNotAvailable) {
      setState(() {
        _products = [];
        _purchases = [];
        _notFoundIds = [];
        _consumables = [];
        _purchasePending = false;
        _loading = false;
      });
      return;
    }

    ProductDetailsResponse productDetailsResponse =
        await _connection.queryProductDetails(_kProductIds.toSet());
    if (productDetailsResponse.error != null) {
      setState(() {
        _queryProductError = productDetailsResponse.error.message;
        _isAvailable = isAvailable;
        _products = productDetailsResponse.productDetails;
        _purchases = [];
        _notFoundIds = productDetailsResponse.notFoundIDs;
        _consumables = [];
        _purchasePending = false;
        _loading = false;
      });
      return;
    }

    if (productDetailsResponse.productDetails.isEmpty) {
      setState(() {
        _queryProductError = null;
        _isAvailable = isAvailable;
        _products = productDetailsResponse.productDetails;
        _purchases = [];
        _notFoundIds = productDetailsResponse.notFoundIDs;
        _consumables = [];
        _purchasePending = false;
        _loading = false;
      });
      return;
    }

    final QueryPurchaseDetailsResponse purchaseResponse =
        await _connection.queryPastPurchases();
    if (purchaseResponse.error != null) {}
    final List<PurchaseDetails> verifiedPurchases = [];
    for (PurchaseDetails purchase in purchaseResponse.pastPurchases) {
      if (await _verifyPurchase(purchase)) {
        verifiedPurchases.add(purchase);
      }
    }
    // List<String> consumables = await ConsumableStore.load();
    // setState(() {
    //   _isAvailable = isAvailable;
    //   _products = productDetailsResponse.productDetails;
    //   _purchases = verifiedPurchases;
    //   _notFoundIds = productDetailsResponse.notFoundIDs;
    //   _consumables = consumables;
    //   _purchasePending = false;
    //   _loading = false;
    // });
  }

  Future<bool> _verifyPurchase(PurchaseDetails purchaseDetails) {
    // IMPORTANT!! Always verify a purchase before delivering the product.
    // For the purpose of an example, we directly return true.
    return Future<bool>.value(true);
  }
}
