part of '../purchase_page.dart';

extension _DescriptionSection on _PurchaseState {
  SliverToBoxAdapter _buildDescriptionsSection() {
    return SliverToBoxAdapter(
      child: Column(
        children: [
          SizedBox(
            height: 32,
          ),
          Container(
            child: Column(
              children: [
                PurchaseDescriptionConponent(
                  title: "No admob & no spam in all time of learning",
                ),
                PurchaseDescriptionConponent(
                  title: "No limited your quick learning time",
                ),
                PurchaseDescriptionConponent(
                  title: "See question result explantation",
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
