part of '../purchase_page.dart';

extension _PurchasePackageSection on _PurchaseState {
  SliverToBoxAdapter _buildPurchasePackageSection() {
    return SliverToBoxAdapter(
      child: Container(
        margin: const EdgeInsets.symmetric(horizontal: 24, vertical: 32),
        child: Column(
          children: [
            PurchasePackageComponent(
              title: "Weekly",
              price: 49000,
            ),
            PurchasePackageComponent(
              title: "Monthly",
              price: 129000,
            ),
            PurchasePackageComponent(
              title: "Lifetime",
              price: 399000,
              isBestPackage: true,
            )
          ],
        ),
      ),
    );
  }
}

class Currency {
  static final String VND = "VND";
  static final String USD = "USD";
}

class CurrencyHandleale {
  String asCurrencyFormat({@required int value}) => throw UnimplementedError();
}

class VNDHandler implements CurrencyHandleale {
  @override
  String asCurrencyFormat({@required int value}) {
    final curencyFormatter = new NumberFormat("#,##0", "en_US");
    return curencyFormatter.format(value) + Currency.VND;
  }
}

class USDHanlder implements CurrencyHandleale {
  @override
  String asCurrencyFormat({@required int value}) {
    final curencyFormatter = new NumberFormat("#,##0", "en_US");
    return curencyFormatter.format(value) + Currency.USD;
  }
}

class CurrencyFormatUtils {
  static CurrencyHandleale hanlder() {
    final storageService = localtor<LocalStorageServiceType>();
    switch (storageService.settingsData.languageType) {
      case LanguageType.vietnamess:
        return VNDHandler();
      case LanguageType.english:
        return USDHanlder();
    }

    return VNDHandler();
  }
}
