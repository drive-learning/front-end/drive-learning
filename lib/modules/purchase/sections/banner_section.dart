part of '../purchase_page.dart';

extension _BannerSection on _PurchaseState {
  SliverToBoxAdapter _buildBannerSection() {
    List<Color> _colors = [Colors.red, Colors.yellow[800], Colors.teal];
    return SliverToBoxAdapter(
      child: Column(
        children: [
          SizedBox(
            height: 56,
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              IconButton(
                icon: Icon(Icons.close),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 32),
                child: Text(
                  "Get license fast  \n with Our App".toUpperCase(),
                  style: TextStyle(
                      color: Colors.grey[800],
                      fontSize: 24,
                      fontWeight: FontWeight.w900),
                  textAlign: TextAlign.center,
                ),
              ),
            ],
          ),
          SizedBox(
            height: 24,
          ),
          Container(
            height: 196,
            color: Colors.white,
            child: PageView.builder(
              scrollDirection: Axis.horizontal,
              controller: _bannerPageController,
              itemBuilder: (context, index) {
                double scale = max(viewportFraction,
                    (1 - (pageOffset - index).abs()) + viewportFraction);
                return Container(
                  padding: EdgeInsets.only(
                      right: 8,
                      left: 8,
                      top: 32 - 16 * scale,
                      bottom: 32 - 16 * scale),
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(16),
                      color: _colors[index % 3],
                    ),
                  ),
                );
              },
            ),
          )
        ],
      ),
    );
  }
}
