part of '../test_histories_page.dart';

extension TopTrendingSection on _TestHistoriesState {
  SliverToBoxAdapter _buildTopTrendingHistoriesSection() {
    final List<HistoryViewModel> viewModels = _bloc.topTrendingHistories
        .map((e) => HistoryViewModel(history: e))
        .toList();
    if (viewModels.isNotEmpty) {
      return SliverToBoxAdapter(
          child: Container(
              padding: const EdgeInsets.symmetric(vertical: 24),
              height: MediaQuery.of(context).size.height / 3,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  HeaderSectionComponent(
                    title: AppLocalizations.of(context)
                        .translate(AppLocalizationKeys.nearestTestMostOfAll),
                    description: "Gần đây bạn thực sư quan tâm đến lái xe A1",
                    paddingValue: 24,
                  ),
                  Expanded(
                    child: Container(
                        child: PageView.builder(
                      controller: _topTrendingPageController,
                      scrollDirection: Axis.horizontal,
                      itemBuilder: (context, index) {
                        double scale = max(
                            viewportFraction,
                            (1 - (pageOffset - index).abs()) +
                                viewportFraction);

                        Color backgroundColor = listColors.loop(index);
                        HistoryViewModel viewModel = viewModels.loop(index);
                        return Container(
                          padding: EdgeInsets.only(
                              right: 8,
                              left: 8,
                              top: 32 - 16 * scale,
                              bottom: 32 - 16 * scale),
                          child: TrendingHistoryComponent(
                            backgroundColor: backgroundColor,
                            viewModel: viewModel,
                            onTap: () {
                              handleTapTryAgainTrendingTopic(
                                  viewModel: viewModel,
                                  backgroundColor: backgroundColor);
                            },
                          ),
                        );
                      },
                    )),
                  )
                ],
              )));
    }

    return SliverToBoxAdapter(
        child: Container(
            padding: const EdgeInsets.symmetric(vertical: 24),
            height: MediaQuery.of(context).size.height / 3,
            color: Colors.red));
  }

  void handleTapTryAgainTrendingTopic(
      {HistoryViewModel viewModel, Color backgroundColor}) {
    _bloc.setCurrentSelectedSetQuestionID(
        id: viewModel.history.setQuestionId,
        onSuccess: () {
          RewardedVideoAdManager.instance.onReload = () {
            _bloc.reload();
          };
          RewardedVideoAdManager.instance.onRewarded = () {
            _bloc.fetchSetQuestion(
                id: viewModel.history.setQuestionId,
                onCompletion: (setQuestion) {
                  Navigator.push(context, MaterialPageRoute(
                    builder: (context) {
                      return QuestionsPreviewPage(
                        presentationMode: QuestionGroupPresentationMode.testing,
                        primaryColor: backgroundColor,
                        isFromNotification: false,
                        setQuestion: setQuestion,
                      );
                    },
                  ));
                });
          };
          RewardedVideoAd.instance.show();
        });
  }
}
