import 'dart:async';
import 'dart:math';

import 'package:firebase_admob/firebase_admob.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:vn_drive_learning/common/constant/locale/app_localization_keys.dart';
import 'package:vn_drive_learning/common/constant/color/colors_constant.dart';
import 'package:vn_drive_learning/common/constant/animation/lotties_constant.dart';
import 'package:vn_drive_learning/locale/applocalizations.dart';
import 'package:vn_drive_learning/modules/histories/components/header_section/header_section_component.dart';
import 'package:vn_drive_learning/modules/histories/components/list_histories_component.dart';
import 'package:vn_drive_learning/modules/histories/components/trending_history/trending_history.dart';
import 'package:vn_drive_learning/modules/histories/test_histories_bloc.dart';
import 'package:vn_drive_learning/modules/questions_preview/questions_preview_page.dart';
import 'package:vn_drive_learning/modules/questions_preview/strategies/question/question_group_presentation_mode.dart';
import 'package:vn_drive_learning/modules/tabbar/tabbar_view.dart';
import 'package:vn_drive_learning/services/entity/history.dart';
import 'package:vn_drive_learning/utils/adds/ad_manager/ad_id_manager.dart';
import 'package:vn_drive_learning/utils/adds/ad_manager/rewarded_ads_manager.dart';
import 'package:vn_drive_learning/extensions/loop_list.dart';

part 'sections/top_trending_histories.dart';
part 'extensions/admobs.dart';

class TestHistoriesPage extends StatefulWidget {
  final bool hasFloatingButton;
  final double bannerHeight = 320;

  TestHistoriesPage({@required this.hasFloatingButton});

  // TestHistoriesPage({this.hasFloatingButton});
  @override
  State<StatefulWidget> createState() {
    return _TestHistoriesState(hasFloatingButton: hasFloatingButton);
  }
}

class _TestHistoriesState extends State<TestHistoriesPage> {
  final bool hasFloatingButton;

  _TestHistoriesState({this.hasFloatingButton});

  var offset = 0;
  PageController _topTrendingPageController;
  final double viewportFraction = 0.8;
  double pageOffset = 0.0;

  int currentSelectedHistoryIndex = 0;

  final List<Color> listColors = [
    Colors.green,
    Colors.yellow[800],
    Colors.pinkAccent[400],
  ];

  TestHistoriesBloc _bloc;

  BannerAd _bannerAd;

  bool _isInterstitialAdReady;
  InterstitialAd _interstitialAd;
  // InterstitialAd interstitialAd;
  @override
  void initState() {
    super.initState();
    _bloc = TestHistoriesBloc()
      ..fetchTopTrendingHistories()
      ..fetchHistories(offset);
    _topTrendingPageController =
        PageController(initialPage: 0, viewportFraction: viewportFraction);

    Timer.periodic(Duration(milliseconds: 2500), (timer) {
      setState(() {
        if (_topTrendingPageController.hasClients) {
          pageOffset += 1;
          _topTrendingPageController.animateToPage(pageOffset.round(),
              duration: Duration(milliseconds: 300), curve: Curves.easeIn);
        }
      });
    });
    // loadRewardedAd();

    // _initializeBannerAd();
    _initializeInterstitialAd();
  }

  @override
  void dispose() {
    _bloc.dispose();
    _topTrendingPageController.dispose();
    _bannerAd?.dispose();
    _interstitialAd.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        // extendBodyBehindAppBar: true,

        floatingActionButton: Visibility(
          visible: hasFloatingButton,
          child: FloatingActionButton(
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) {
                        return MainTabbarView();
                      },
                      fullscreenDialog: true));
            },
            child: Icon(
              Icons.home,
              color: Colors.white,
            ),
            backgroundColor: ColorsContant.appPrimaryColor,
          ),
        ),
        body: NotificationListener(
          child: StreamBuilder(
            stream: _bloc.historiesStream,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                // builder: (context, state) {

                return CustomScrollView(
                  slivers: [
                    SliverToBoxAdapter(
                        child: SizedBox(
                      height: 80,
                    )),
                    _buildTopTrendingHistoriesSection(),
                    SliverToBoxAdapter(
                        child: HeaderSectionComponent(
                            title: AppLocalizations.of(context).translate(
                                AppLocalizationKeys.nearestTestHistories),
                            description: AppLocalizations.of(context).translate(
                                AppLocalizationKeys
                                    .nearestTestHistoriesDesGood),
                            paddingValue: 24)),
                    ListHistoriesComponent(
                      histories: _bloc.histories,
                      onTapHistoryItem: (history) {
                        _bloc.setCurrentSelectedSetQuestionID(
                            id: history.setQuestionId,
                            onSuccess: () {
                              if (_isInterstitialAdReady) {
                                _interstitialAd.show();
                              }
                            });
                      },
                    )
                  ],
                );
              }

              return Container(
                  child: Lottie.asset(Lottiesconstant.loadingAnimation,
                      width: 128, height: 128));
            },
          ),
          onNotification: (ScrollNotification notification) {
            if (notification.metrics.pixels >=
                    notification.metrics.maxScrollExtent * 0.7 &&
                _bloc.isLoadMoreAvailable) {
              loadMore();
            }
            return false;
          },
        ));
    // Stack(children: [_buildBannerWidget(), ListHistoriesComponent()]));
  }

  loadMore() {
    offset += 10;
    _bloc.fetchHistories(offset);
  }
}
