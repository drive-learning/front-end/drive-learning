import 'dart:async';
import 'package:flutter/material.dart';
import 'package:vn_drive_learning/common/base/bloc_base.dart';
import 'package:vn_drive_learning/services/entity/history.dart';
import 'package:vn_drive_learning/services/entity/set_question.dart';
import 'package:vn_drive_learning/services/repositories/questions_repository.dart';

class TestHistoriesBloc extends BlocBase {
  StreamController _controller = StreamController();
  Stream get historiesStream => _controller.stream;

  StreamController _trendingHistoriesController = StreamController();
  Stream get trendingHistoriesStream => _trendingHistoriesController.stream;

  HistoriesRepository _historiesRepository = HistoriesRepository();
  List<History> _histories = List();
  List<History> _topTrendingHistories = List();

  List<History> get histories => _histories;
  List<History> get topTrendingHistories => _topTrendingHistories;

  bool get isLoadMoreAvailable {
    return _histories.length % 10 == 0;
  }

  int _currentSelectedSetQuestionID = 0;
  int get currentSelectedSetQuestionID => _currentSelectedSetQuestionID;

  fetchTopTrendingHistories() {
    _historiesRepository.fetchTopTrendingHistories(
        completion: DatabaseHandleCallBack(
      onSuccess: (histories) {
        _topTrendingHistories = histories;
        _controller.sink.add(true);
      },
      onFailure: () {},
    ));
  }

  fetchHistories(int offset) async {
    final newHistories = await _historiesRepository.fetchHistories(offset);
    if (newHistories.isNotEmpty) {
      _histories.addAll(newHistories);
    }

    _controller.sink.add(true);
  }

  fetchSetQuestion(
      {@required int id, @required Function(SetQuestion) onCompletion}) {
    _historiesRepository.fetchSetQuestion(
        id: id,
        completion: DatabaseHandleCallBack(onSuccess: (setQuestion) {
          onCompletion(setQuestion);
        }, onFailure: () {
          debugPrint("💔💔💔 Error for get set question 💔💔💔");
        }));
  }

  setCurrentSelectedSetQuestionID(
      {@required int id, @required VoidCallback onSuccess}) {
    _currentSelectedSetQuestionID = id;
    _controller.sink.add(true);
    onSuccess();
  }

  reload() {
    _controller.sink.add(true);
  }

  @override
  void dispose() {
    _controller.close();
    _trendingHistoriesController.close();
  }
}
