part of '../trending_history.dart';

extension Deconration on TrendingHistoryComponent {
  BoxDecoration get _mainBoxDecoration => BoxDecoration(
          color: backgroundColor,
          borderRadius: BorderRadius.circular(16),
          boxShadow: [
            BoxShadow(
                offset: Offset(0, 4),
                blurRadius: 8,
                color: backgroundColor.withOpacity(0.35))
          ]);
}
