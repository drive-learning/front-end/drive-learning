part of '../trending_history.dart';

extension Main on TrendingHistoryComponent {
  Widget get _mainView => Container(
        padding: const EdgeInsets.all(16),
        decoration: _mainBoxDecoration,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [_headerView, _titleView, _bottomView],
        ),
      );
}
