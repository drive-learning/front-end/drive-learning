part of '../trending_history.dart';

extension Bottom on TrendingHistoryComponent {
  Widget get _bottomView => Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [_subTitleView, _tryAgainButton],
      );

  Widget get _subTitleView => Text(viewModel.timeInStandartFormator,
      style: TextStyle(
          color: Colors.white, fontSize: 13.5, fontWeight: FontWeight.w500));

  Widget get _tryAgainButton => InkWell(
      onTap: () {
        this.onTap();
      },
      child: _tryAgainButtonView);

  Widget get _tryAgainButtonView => Container(
        alignment: Alignment.center,
        padding: const EdgeInsets.symmetric(horizontal: 16),
        height: 36,
        decoration: BoxDecoration(
            color: Colors.purple[900].withOpacity(0.45),
            borderRadius: BorderRadius.circular(18),
            border: Border.all(
                color: Colors.blueGrey.withOpacity(0.55), width: 0.25)),
        child: Text(
          "Thi lại".toUpperCase(),
          style: TextStyle(
              color: Colors.white, fontSize: 13.5, fontWeight: FontWeight.w800),
        ),
      );
}
