part of '../trending_history.dart';

extension Title on TrendingHistoryComponent {
  Widget get _titleView => Container(
        alignment: Alignment.centerLeft,
        child: Text(
          viewModel.title,
          style: TextStyle(
              color: Colors.white, fontSize: 20, fontWeight: FontWeight.w800),
        ),
      );
}
