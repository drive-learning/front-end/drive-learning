import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:vn_drive_learning/common/constant/images/images_constant.dart';
import 'package:vn_drive_learning/services/entity/history.dart';

part 'sections/main_view.dart';
part 'sections/title_view.dart';
part 'sections/header_view.dart';
part 'sections/bottom_view.dart';
part 'sections/decoration_config.dart';

class TrendingHistoryComponent extends StatelessWidget {
  final HistoryViewModel viewModel;
  final VoidCallback onTap;
  TrendingHistoryComponent(
      {Key key,
      @required this.viewModel,
      @required this.backgroundColor,
      @required this.onTap})
      : super(key: key);

  final Color backgroundColor;

  @override
  Widget build(BuildContext context) {
    return _mainView;
  }
}
