import 'package:flutter/material.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:vn_drive_learning/design_system/ds_test_style/ds_text_style.dart';
import 'package:vn_drive_learning/services/entity/history.dart';

class HistoryComponent extends StatelessWidget {
  final HistoryViewModel historyViewModel;
  final VoidCallback onTap;

  HistoryComponent({@required this.historyViewModel, this.onTap});

  @override
  Widget build(Object context) {
    final double height = 80.0;
    final double padding = 16.0;
    Color backgroundColor = historyViewModel.categoryColor;

    return InkWell(
        onTap: () {
          onTap();
        },
        child: Container(
          color: Colors.white,
          height: height,
          child: Column(
            children: [
              Container(
                  height: height - 1,
                  child: Row(
                    children: [
                      Container(
                        width: height - padding * 2,
                        margin: const EdgeInsets.only(
                            left: 24, top: 16, bottom: 16),
                        decoration: BoxDecoration(
                          color: backgroundColor,
                          borderRadius: BorderRadius.circular(4),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Center(
                            child:
                                Image.asset(historyViewModel.categoryIconPath),
                          ),
                        ),
                      ),
                      SizedBox(width: 16),
                      RichText(
                          text: TextSpan(
                        children: [
                          TextSpan(
                              text: "${historyViewModel.title}\n\n",
                              style: DSTextStyleProvider.shared.titleStyle),
                          TextSpan(children: [
                            TextSpan(
                                text: historyViewModel.timeInStandartFormator,
                                style: DSTextStyleProvider
                                    .shared.descriptionStyle),
                            TextSpan(text: "     "),
                            TextSpan(
                                text: "Result ",
                                style:
                                    DSTextStyleProvider.shared.subTitleStyle),
                            TextSpan(
                                text: historyViewModel.history.description,
                                style:
                                    DSTextStyleProvider.shared.descriptionStyle)
                          ])
                        ],
                      )),
                      Spacer(),
                      Container(
                          margin: const EdgeInsets.only(
                              top: 4, right: 16, bottom: 4),
                          decoration: BoxDecoration(
                            color: Colors.transparent,
                          ),
                          child: Center(
                            child: CircularPercentIndicator(
                              animation: true,
                              backgroundWidth: 5.5,
                              backgroundColor: Colors.grey[300],
                              progressColor: historyViewModel.progressColor,
                              percent: historyViewModel.history.testResult,
                              radius: height / 2 + 14,
                              circularStrokeCap: CircularStrokeCap.round,
                              lineWidth: 6,
                              center: Text(
                                "${(historyViewModel.history.testResult * 100).toInt()}%",
                                style: DSTextStyleProvider.shared.subTitleStyle,
                              ),
                            ),
                          )),
                    ],
                  )),
              Divider(
                height: 1,
                color: Colors.grey[800].withOpacity(0.35),
              )
            ],
          ),
        ));
  }
}

extension LicenseLogo on HistoryComponent {}

extension Infomation on HistoryComponent {}

extension ResultChart on HistoryComponent {}
