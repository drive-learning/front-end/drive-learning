import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:vn_drive_learning/common/constant/animation/lotties_constant.dart';
import 'package:vn_drive_learning/modules/histories/components/history_component/history_component.dart';
import 'package:vn_drive_learning/services/entity/history.dart';

class ListHistoriesComponent extends StatelessWidget {
  final List<History> histories;
  final Function(History) onTapHistoryItem;

  const ListHistoriesComponent(
      {@required this.histories, @required this.onTapHistoryItem});

  @override
  Widget build(Object context) {
    return SliverList(
        delegate: SliverChildBuilderDelegate((BuildContext context, int index) {
      if (index == histories.length) {
        return Container(
          height: 80,
          child: Center(
            child: Lottie.asset(Lottiesconstant.loadMoreAnimation,
                repeat: true, fit: BoxFit.fitHeight),
          ),
        );
      }
      return HistoryComponent(
        historyViewModel: HistoryViewModel(history: histories[index]),
        onTap: () {
          this.onTapHistoryItem(histories[index]);
        },
      );
    }, childCount: histories.length + 1));
  }
}
