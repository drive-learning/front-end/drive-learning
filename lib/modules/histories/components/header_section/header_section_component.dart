import 'package:flutter/widgets.dart';
import 'package:vn_drive_learning/design_system/ds_test_style/ds_text_style.dart';

class HeaderSectionComponent extends StatelessWidget {
  final String title;
  final String description;
  final double paddingValue;

  const HeaderSectionComponent(
      {Key key,
      @required this.title,
      @required this.description,
      @required this.paddingValue})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.symmetric(horizontal: paddingValue),
        height: 44,
        child: RichText(
          text: TextSpan(children: [
            TextSpan(
                text: "${this.title} \n".toUpperCase(),
                style: DSTextStyleProvider.shared.titleStyle),
            TextSpan(
                text: description,
                style: DSTextStyleProvider.shared.descriptionStyle)
          ]),
        ));
  }
}
