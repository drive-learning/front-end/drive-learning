import 'package:flutter/material.dart';

class DSBouncingButton extends StatefulWidget {
  final Widget contentView;
  final VoidCallback onTap;

  DSBouncingButton({@required this.contentView, @required this.onTap});

  @override
  State<StatefulWidget> createState() {
    return _DSBouncingButtonState();
  }
}

class _DSBouncingButtonState extends State<DSBouncingButton>
    with SingleTickerProviderStateMixin {
  double _scale;
  AnimationController _controller;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
        vsync: this,
        duration: Duration(microseconds: 200),
        lowerBound: 0,
        upperBound: 0.1)
      ..addListener(() {
        setState(() {});
      });
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  void _onTapDown(TapDownDetails details) {
    _controller.forward();
  }

  void _onTapUp(TapUpDetails details) {
    _controller.reverse();
    widget.onTap();
  }

  @override
  Widget build(BuildContext context) {
    _scale = 1 - _controller.value;
    return GestureDetector(
      onTapDown: _onTapDown,
      onTapUp: _onTapUp,
      child: Transform.scale(scale: _scale, child: widget.contentView),
    );
  }
}
