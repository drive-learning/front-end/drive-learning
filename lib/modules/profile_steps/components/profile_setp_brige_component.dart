import 'package:flutter/material.dart';

class ProfileStepBrigdeComponent extends StatelessWidget {
  final Widget contentWidget;
  final String imageAssetPath;

  const ProfileStepBrigdeComponent(
      {Key key, @required this.contentWidget, @required this.imageAssetPath})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        padding: const EdgeInsets.all(24),
        child: Column(
          children: [
            Container(
              margin: const EdgeInsets.only(top: 96),
              height: 172,
              color: Colors.transparent,
              child: Center(
                child: AspectRatio(
                  aspectRatio: 1,
                  child: Image.asset(
                    imageAssetPath,
                    fit: BoxFit.fill,
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 16,
            ),
            Text(
              "Enter your name",
              style: TextStyle(color: Colors.grey[800], fontSize: 16),
            ),
            SizedBox(
              height: 12,
            ),
            Text(
              "Enter your name for support app follow you behavior and all your testing",
              textAlign: TextAlign.center,
              style: TextStyle(
                  color: Colors.grey[600],
                  fontSize: 16,
                  fontStyle: FontStyle.italic),
            ),
            SizedBox(height: 16),
            contentWidget,
            SizedBox(
              height: 20,
            ),
          ],
        ),
        color: Colors.white,
      ),
    );
  }
}
