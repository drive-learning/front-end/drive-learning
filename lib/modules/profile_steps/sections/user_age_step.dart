part of '../profile_steps_page.dart';

extension _UserAgeStep on ProfileStepsState {
  ProfileStepBrigdeComponent _buildUserAgeStepComponent() {
    final selectedDecoration = BoxDecoration(
        color: Colors.white,
        border: Border.all(color: Colors.red, width: 0.5),
        borderRadius: BorderRadius.circular(8));
    final userAgePageContentWdiget = Container(
        height: 56,
        width: 96,
        child: PinPut(
          controller: _pinPutController,
          fieldsCount: 2,
          eachFieldHeight: 40,
          eachFieldWidth: 40,
          onSubmit: (value) {
            _bloc.setUserAge(value);
            _controller.jumpToPage(_controller.page.round() + 1);
          },
          followingFieldDecoration: BoxDecoration(
              color: Colors.red, borderRadius: BorderRadius.circular(8)),
          selectedFieldDecoration: selectedDecoration,
          submittedFieldDecoration: selectedDecoration,
        ));

    return ProfileStepBrigdeComponent(
      contentWidget: userAgePageContentWdiget,
      imageAssetPath: ImagesConstant.bgUserAge,
    );
  }
}
