part of '../profile_steps_page.dart';

extension _UserLicenseOptionStep on ProfileStepsState {
  ProfileStepBrigdeComponent _buildUserLicenseOptionComponent() {
    List<LicenseOptionViewModel> licenseModels = _bloc.listLicenseOption();
    List<Widget> children = licenseModels
        .asMap()
        .map((index, element) => MapEntry(
            index,
            DSBouncingButton(
              contentView: OptionComponent(
                title: element.getDisplayedName(),
                isSelected: element.isSelected,
              ),
              onTap: () {
                _bloc.selectedLicense(selectedIndex: index);
                _controller.jumpToPage(_controller.page.round() + 1);
              },
            )))
        .values
        .map<Widget>((e) => e)
        .toList();

    final licensesPageContentWidget = Column(children: children);

    return ProfileStepBrigdeComponent(
      contentWidget: licensesPageContentWidget,
      imageAssetPath: ImagesConstant.bgUserLicense,
    );
  }
}
