part of '../profile_steps_page.dart';

extension _UserNameStep on ProfileStepsState {
  ProfileStepBrigdeComponent _buildUserNameStepComponent() {
    final userNamePageCotentWidget = Theme(
      data: ThemeData(primaryColor: Colors.grey[400]),
      child: Column(
        children: [
          TextField(
            controller: TextEditingController()..text = _bloc.currentName,
            decoration: InputDecoration(
              hintStyle: TextStyle(
                  fontStyle: FontStyle.italic,
                  color: Colors.grey[400],
                  fontWeight: FontWeight.w300),
              hintText: "Enter your name",
              border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                  borderSide: BorderSide(
                      color: Colors.grey[600], style: BorderStyle.solid)),
            ),
            onSubmitted: (value) {
              _bloc.setUserName(value);
              _controller.jumpToPage(_controller.page.round() + 1);
            },
            keyboardType: TextInputType.name,
          ),
        ],
      ),
    );

    return ProfileStepBrigdeComponent(
        contentWidget: userNamePageCotentWidget,
        imageAssetPath: ImagesConstant.bgUserName);
  }
}
