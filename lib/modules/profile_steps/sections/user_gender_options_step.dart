part of '../profile_steps_page.dart';

extension _UserGenderOptionStep on ProfileStepsState {
  ProfileStepBrigdeComponent _buildUserGenderStepComponent() {
    List<GenderOptionViewModel> genderViewModes = _bloc.listGenderOption();
    Color backgroundColor =
        _bloc.isValidUserInfo ? Colors.red : Colors.grey[600];

    List<Widget> children = genderViewModes
            .asMap()
            .map((index, element) => MapEntry(
                index,
                DSBouncingButton(
                  contentView: OptionComponent(
                    title: element.getDisplayedName(),
                    isSelected: element.isSelected,
                  ),
                  onTap: () {
                    _bloc.selectedGender(selectedIndex: index);
                  },
                )))
            .values
            .map<Widget>((e) => e)
            .toList() +
        [
          DSBouncingButton(
            contentView: Container(
              width: 164,
              height: 48,
              margin: const EdgeInsets.only(top: 24),
              padding: const EdgeInsets.symmetric(horizontal: 24),
              decoration: BoxDecoration(
                  color: backgroundColor,
                  borderRadius: BorderRadius.circular(24)),
              child: Row(
                children: [
                  Text(
                    "Next".toUpperCase(),
                    style: TextStyle(color: Colors.white),
                  ),
                  Spacer(),
                  Icon(
                    Icons.arrow_forward,
                    color: Colors.white,
                    size: 24,
                  )
                ],
              ),
            ),
            onTap: () async {
              if (_bloc.isValidUserInfo) {
                await _bloc.updateUserInfo();
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) {
                        return MainTabbarView();
                      },
                      fullscreenDialog: true,
                    ));
              }
            },
          ),
          SizedBox(
            height: 36,
          )
        ];

    final genderPageContentWidget = Column(children: children);

    return ProfileStepBrigdeComponent(
      contentWidget: genderPageContentWidget,
      imageAssetPath: ImagesConstant.bgUserGender,
    );
  }
}
