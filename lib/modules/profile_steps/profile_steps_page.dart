import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:pinput/pin_put/pin_put.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';
import 'package:vn_drive_learning/common/constant/images/images_constant.dart';
import 'package:vn_drive_learning/modules/profile_steps/components/ds_bouncing_button.dart';
import 'package:vn_drive_learning/modules/profile_steps/components/profile_setp_brige_component.dart';
import 'package:vn_drive_learning/modules/profile_steps/profile_steps_bloc.dart';
import 'package:vn_drive_learning/modules/tabbar/tabbar_view.dart';

part 'sections/user_name_step.dart';
part 'sections/user_age_step.dart';
part 'sections/user_licenses_options_step.dart';
part 'sections/user_gender_options_step.dart';

class ProfileStepsPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return ProfileStepsState();
  }
}

class ProfileStepsState extends State<ProfileStepsPage> {
  ProfileStepsBloc _bloc;
  double currentPageIndex = 0;
  PageController _controller = PageController(initialPage: 0, keepPage: true);
  TextEditingController _pinPutController = TextEditingController();
  var _profileSteps;

  @override
  void initState() {
    _bloc = ProfileStepsBloc()
      ..fetchListGender()
      ..fetchListLicense();
    _controller.addListener(() {
      setState(() {
        currentPageIndex = _controller.page;
      });
    });

    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    _pinPutController.dispose();
    super.dispose();
  }

  _buildStepBridgeComponents() {
    return [
      _buildUserNameStepComponent(),
      _buildUserAgeStepComponent(),
      _buildUserLicenseOptionComponent(),
      _buildUserGenderStepComponent()
    ];
  }

  @override
  Widget build(Object context) {
    return Scaffold(
        body: StreamBuilder(
      stream: _bloc.stream,
      builder: (context, snapshot) {
        _profileSteps = _buildStepBridgeComponents();
        return SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(
                height: MediaQuery.of(context).size.height - 36,
                child: PageView.builder(
                  controller: _controller,
                  itemBuilder: (context, index) {
                    if (index == currentPageIndex.floor()) {
                      return Transform(
                          transform: Matrix4.identity()
                            ..rotateX(currentPageIndex - index),
                          child: _profileSteps[index]);
                    }

                    if (index == currentPageIndex.floor() + 1) {
                      return Transform(
                          transform: Matrix4.identity()
                            ..rotateX(currentPageIndex - index),
                          child: _profileSteps[index]);
                    }
                    return _profileSteps[index];
                  },
                  itemCount: _profileSteps.length,
                ),
              ),
              SizedBox(
                height: 36,
                child: SmoothPageIndicator(
                  controller: _controller,
                  count: _profileSteps.length,
                  effect: WormEffect(
                      dotColor: Colors.grey,
                      activeDotColor: Colors.red,
                      dotHeight: 10,
                      dotWidth: 10,
                      paintStyle: PaintingStyle.fill,
                      spacing: 12),
                ),
              ),
            ],
          ),
        );
      },
    ));
  }

  void onNextPage() {}
}

class LicenseOption {
  bool isSelected;
  String title;
}

class OptionComponent extends StatelessWidget {
  final String title;
  final bool isSelected;
  const OptionComponent(
      {Key key, @required this.title, this.isSelected = false})
      : super(key: key);

  Color _getBackgroundColor() {
    if (isSelected) {
      return Colors.red;
    }

    return Colors.white;
  }

  Color _getTextColor() {
    if (isSelected) {
      return Colors.white;
    }

    return Colors.blueGrey;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 56,
      width: 280,
      margin: const EdgeInsets.symmetric(vertical: 8, horizontal: 48),
      decoration: BoxDecoration(
          color: _getBackgroundColor(),
          borderRadius: BorderRadius.circular(8),
          border: Border.all(
              color: Colors.grey[600].withOpacity(0.35), width: 0.75),
          boxShadow: [
            BoxShadow(
                offset: Offset(0, 2), blurRadius: 2, color: Colors.redAccent)
          ]),
      child: Center(
          child: Text(
        title,
        style: TextStyle(fontWeight: FontWeight.w400, color: _getTextColor()),
      )),
    );
  }
}
