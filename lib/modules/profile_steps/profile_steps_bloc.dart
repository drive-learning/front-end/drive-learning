import 'dart:async';

import 'package:flutter/material.dart';
import 'package:vn_drive_learning/common/base/bloc_base.dart';
import 'package:vn_drive_learning/main.dart';
import 'package:vn_drive_learning/services/entity/user_info/gender.dart';
import 'package:vn_drive_learning/services/entity/user_info/license_type.dart';
import 'package:vn_drive_learning/services/entity/user_info/user_info.dart';
import 'package:vn_drive_learning/services/repositories/user_info_repository.dart';
import 'package:vn_drive_learning/services/storage/local_storage_service.dart';

class Selectable {
  bool isSelected;

  String getDisplayedName() {
    return "";
  }
}

class GenderOptionViewModel implements Selectable {
  @override
  bool isSelected;
  Gender gender;

  GenderOptionViewModel({this.isSelected = false, @required this.gender});

  @override
  String getDisplayedName() {
    switch (gender) {
      case Gender.male:
        return "Nam";
        break;
      case Gender.female:
        return "Nữ";
        break;
      case Gender.other:
        return "Giới tính khác";
        break;
    }

    return "";
  }
}

class LicenseOptionViewModel implements Selectable {
  @override
  bool isSelected;
  LicenseType licenseType;

  LicenseOptionViewModel({this.isSelected = false, @required this.licenseType});

  @override
  String getDisplayedName() {
    switch (licenseType) {
      case LicenseType.A1:
        return "Bằng lái xe A1";
        break;
      case LicenseType.A2:
        return "Bằng lái xe A2";
        break;
      case LicenseType.B1:
        return "Bằng lái xe B1";
        break;
    }
    return "";
  }
}

class ProfileStepsBloc extends BlocBase {
  UserInfoRepository _userInfoRepository = UserInfoRepository();
  StreamController _controller = StreamController();
  Stream get stream => _controller.stream;

  String _currentAgeInString = "";
  String get currentAgeInString => _currentAgeInString;

  String _currentName = "";
  String get currentName => _currentName;

  List<GenderOptionViewModel> _genderOptions = List.empty();
  int _currentGenderSelectedIndex = -1;

  List<LicenseOptionViewModel> _licenseOptions = List.empty();
  int _currentLicenseSelectedIndex = -1;

  fetchListGender() async {
    _genderOptions =
        Gender.values.map((e) => GenderOptionViewModel(gender: e)).toList();
    _controller.sink.add(true);
  }

  fetchListLicense() {
    _licenseOptions = LicenseType.values
        .map((e) => LicenseOptionViewModel(licenseType: e))
        .toList();

    _controller.sink.add(true);
  }

  List<GenderOptionViewModel> listGenderOption() {
    return _genderOptions;
  }

  List<LicenseOptionViewModel> listLicenseOption() {
    return _licenseOptions;
  }

  selectedGender({@required int selectedIndex}) async {
    if (_currentGenderSelectedIndex == -1) {
      _genderOptions[selectedIndex].isSelected = true;
      _currentGenderSelectedIndex = selectedIndex;
    } else if (_currentGenderSelectedIndex == selectedIndex) {
      _genderOptions[selectedIndex].isSelected = false;
      _currentGenderSelectedIndex = -1;
    } else {
      _genderOptions[_currentGenderSelectedIndex].isSelected = false;
      _genderOptions[selectedIndex].isSelected = true;
      _currentGenderSelectedIndex = selectedIndex;
    }

    _controller.sink.add(true);
  }

  selectedLicense({@required int selectedIndex}) async {
    if (_currentLicenseSelectedIndex == -1) {
      _licenseOptions[selectedIndex].isSelected = true;
      _currentLicenseSelectedIndex = selectedIndex;
    } else if (_currentLicenseSelectedIndex == selectedIndex) {
      _licenseOptions[selectedIndex].isSelected = false;
      _currentLicenseSelectedIndex = -1;
    } else {
      _licenseOptions[_currentLicenseSelectedIndex].isSelected = false;
      _licenseOptions[selectedIndex].isSelected = true;
      _currentLicenseSelectedIndex = selectedIndex;
    }

    _controller.sink.add(true);
  }

  setUserName(String newValue) {
    _currentName = newValue;
    _controller.sink.add(true);
  }

  setUserAge(String ageInString) {
    _currentAgeInString = ageInString;
    _controller.sink.add(true);
  }

  initializeUserInfo() async {
    // await _userInfoRepository.createUserInfo();
  }

  updateUserInfo() async {
    ///get user license type
    LicenseType userLicenseType =
        _licenseOptions.firstWhere((element) => element.isSelected).licenseType;
    Gender userGender =
        _genderOptions.firstWhere((element) => element.isSelected).gender;

    UserInfoSingleton.shared.setLicenseType(userLicenseType);
    UserInfoSingleton.shared.setGender(userGender);
    UserInfoSingleton.shared.setUserName(_currentName);
    UserInfoSingleton.shared.setTotalGem(0);
    UserInfoSingleton.shared.setAge(int.parse(_currentAgeInString));
    localtor<LocalStorageServiceType>().setIsFirstTimeLunching(value: false);
    await _userInfoRepository.createUserInfo();
  }

  bool get isValidUserInfo {
    if (_currentName.isEmpty ||
        _currentAgeInString.isEmpty ||
        _currentGenderSelectedIndex == -1 ||
        _currentLicenseSelectedIndex == -1) {
      return false;
    }
    return true;
  }

  @override
  void dispose() {
    _controller.close();
  }
}
