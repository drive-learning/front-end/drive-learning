// import 'package:admob_flutter/admob_flutter.dart';
import 'package:firebase_admob/firebase_admob.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import 'package:vn_drive_learning/common/constant/locale/app_localization_keys.dart';
import 'package:vn_drive_learning/common/components/answer_component.dart';
import 'package:vn_drive_learning/common/components/custom_flat_button_component.dart';
import 'package:vn_drive_learning/common/constant/animation/lotties_constant.dart';
import 'package:vn_drive_learning/design_system/ds_alert/alerts/test_result_alert/test_result_alert_config.dart';
import 'package:vn_drive_learning/design_system/ds_alert/ds_alert_package.dart';
import 'package:vn_drive_learning/design_system/ds_bottom_sheet/ds_bottom_sheet_package.dart';
import 'package:vn_drive_learning/locale/applocalizations.dart';
import 'package:vn_drive_learning/modules/histories/test_histories_page.dart';
import 'package:vn_drive_learning/modules/questions_preview/components/list_question_sheet.dart';
import 'package:vn_drive_learning/modules/questions_preview/questions_preview_bloc.dart';
import 'package:vn_drive_learning/modules/questions_preview/strategies/question/answered_callback_status.dart';
import 'package:vn_drive_learning/modules/questions_preview/strategies/question/question_group_presentation_mode.dart';
import 'package:vn_drive_learning/modules/tabbar/tabbar_view.dart';
import 'package:vn_drive_learning/services/entity/question.dart';
import 'package:vn_drive_learning/services/entity/set_question.dart';
import 'package:vn_drive_learning/services/entity/test_result.dart';
import 'package:vn_drive_learning/utils/adds/ad_manager/ad_id_manager.dart';
import 'package:vn_drive_learning/utils/adds/ad_manager/rewarded_ads_manager.dart';

part 'sections/appbar_section.dart';
part 'sections/banner_section.dart';
part 'sections/question_cotent_section.dart';
part 'sections/pin_bottom_section.dart';
part 'sections/alert_ui_handler.dart';
part 'sections/loading_view.dart';

part 'extensions/admobs.dart';

class QuestionsPreviewPage extends StatefulWidget {
  final SetQuestion setQuestion;
  final Color primaryColor;
  final double bannerHeight = 200;
  final QuestionGroupPresentationMode presentationMode;
  final bool isFromNotification;

  QuestionsPreviewPage(
      {this.setQuestion,
      this.primaryColor,
      @required this.presentationMode,
      this.isFromNotification = false});
  @override
  State<StatefulWidget> createState() {
    return _LearningQuestionsPageState();
  }
}

class _LearningQuestionsPageState extends State<QuestionsPreviewPage> {
  QuestionsPreviewBloc _bloc;
  int offset = 0;

  BannerAd _bannerAd;

  bool _isInterstitialAdReady;
  InterstitialAd _interstitialAd;

  @override
  void initState() {
    _bloc = QuestionsPreviewBloc(setQuestion: widget.setQuestion)
      ..setPresentMode(widget.presentationMode)
      ..fetQuestions(widget.setQuestion.id);
    super.initState();

    // _initializeBannerAd();
    _initializeInterstitialAd();
  }

  @override
  void dispose() {
    _bloc.dispose();
    _bannerAd?.dispose();
    super.dispose();
  }

  @override
  Widget build(Object context) {
    final size = MediaQuery.of(context).size;

    return StreamBuilder(
        stream: _bloc.questionsStream,
        builder: (context, snapshot) {
          if (snapshot.hasData && _bloc.questionStrategy.isNotEmpty) {
            final currQuestion = _bloc.currQuestion;
            return Scaffold(
                appBar: AppBar(
                  backgroundColor: widget.primaryColor,
                  elevation: 0,
                  leading: IconButton(
                      icon: Icon(Icons.arrow_back),
                      onPressed: () {
                        if (widget.isFromNotification) {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) {
                                  return MainTabbarView();
                                },
                                fullscreenDialog: true,
                              ));
                        } else {
                          Navigator.pop(context);
                        }
                        // Navigator.pop(context);
                      }),
                  title: _buildPageTitle(width: size.width),
                  actions: [_buildListQuestionPreviewView()],
                ),
                body: Stack(
                  children: [
                    _buildBannerWidget(),
                    _buildQuestionWidget(currQuestion),
                    _buildPinBottomWidget()
                  ],
                ));
          }

          return _buildLoadingView();
        });
  }
}
