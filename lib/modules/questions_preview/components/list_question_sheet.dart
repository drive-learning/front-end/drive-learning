import 'package:flutter/material.dart';
import 'package:vn_drive_learning/common/constant/color/colors_constant.dart';
import 'package:vn_drive_learning/modules/questions_preview/strategies/question/question_group_presentation_mode.dart';
import 'package:vn_drive_learning/services/entity/question.dart';

class ListQuestionSheet extends StatelessWidget {
  final List<Question> questions;
  final Function(int) onTapSheetAt;
  final QuestionGroupPresentationMode presentMode;

  ListQuestionSheet(
      {@required this.questions,
      @required this.onTapSheetAt,
      @required this.presentMode});
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(16),
      height: MediaQuery.of(context).size.height * 0.6,
      child: GridView.builder(
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 4, mainAxisSpacing: 16, crossAxisSpacing: 16),
        itemBuilder: (context, index) {
          final currQuestion = questions[index];
          return InkWell(
            onTap: () {
              this.onTapSheetAt(index);
            },
            child: Container(
              decoration: BoxDecoration(
                  color: _getSheetColor(currQuestion),
                  border: Border.all(color: Colors.grey[800], width: 0.3),
                  borderRadius: BorderRadius.circular(8),
                  boxShadow: [
                    BoxShadow(
                        color: Colors.grey[600],
                        offset: Offset(0, 5),
                        blurRadius: 5)
                  ]),
              child: Center(
                child: Text(
                  "${index + 1}",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 20,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ),
          );
        },
        itemCount: questions.length,
      ),
    );
  }

  Color _getSheetColor(Question currQuestion) {
    if (presentMode == QuestionGroupPresentationMode.testing) {
      if (currQuestion.isAnswered) {
        return ColorsContant.seletedAnswerColor;
      }

      return Colors.grey[400];
    }
    if (currQuestion.isNotAnsweredQuestion) {
      return Colors.grey[400];
    }

    if (currQuestion.isCorrect) {
      return ColorsContant.correctSelectedAnswerColor;
    }

    return ColorsContant.wrongSelectedAnswerColor;
  }
}

enum QuesitonStatus { isCorrect, isWrong, inNotAnswerBefore }
