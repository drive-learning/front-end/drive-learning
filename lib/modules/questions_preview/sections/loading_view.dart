part of '../questions_preview_page.dart';

extension _LoadingView on _LearningQuestionsPageState {
  Widget _buildLoadingView() {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: widget.primaryColor,
        elevation: 0,
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () => Navigator.pop(context),
        ),
      ),
      body: Container(
        child: Center(
          child: Lottie.asset(Lottiesconstant.loadingAnimation,
              width: 128, height: 128),
        ),
      ),
    );
  }
}
