part of '../questions_preview_page.dart';

extension _AppbarSection on _LearningQuestionsPageState {
  Widget _buildListQuestionPreviewView() {
    return IconButton(
        icon: Icon(
          Icons.format_list_bulleted,
          color: Colors.white,
        ),
        onPressed: () {
          DSBottomSheetProvider.shared.showBottomSheet(
              context,
              DSBottomSheetConfig(
                  ListQuestionSheet(
                    questions: _bloc.questionStrategy.questions,
                    onTapSheetAt: (int index) {
                      Navigator.pop(context);
                      _bloc.updateCurrentQuestionIndex(index);
                    },
                    presentMode: _bloc.presentMode,
                  ),
                  title: AppLocalizations.of(context)
                      .translate(AppLocalizationKeys.listQuestion)
                      .toUpperCase()));
        });
  }

  Widget _buildPageTitle({double width}) {
    return Container(
      child: Row(
        children: [
          LinearPercentIndicator(
            width: width * 0.4,
            lineHeight: 12,
            restartAnimation: false,
            percent: _bloc.questionStrategy.percentProcessing,
            clipLinearGradient: true,
            backgroundColor: Colors.white,
            linearGradient:
                LinearGradient(colors: [Colors.yellow[600], Colors.teal[200]]),
          ),
          SizedBox(
            width: 16,
          ),
          Text("${_bloc.questionStrategy.questionIndexTitle}")
        ],
      ),
    );
  }
}
