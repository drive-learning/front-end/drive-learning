part of '../questions_preview_page.dart';

extension _ALertUIHanlder on _LearningQuestionsPageState {
  Future showConfirmAlert(TestResult testResult) async {
    DSAlert.shared.showConfirmAlert(context,
        config: DSConfirmAlertConfig(
            message: AppLocalizations.of(context)
                .translate(AppLocalizationKeys.completionTestConfirm),
            cancelAction: AlertAcitonConfig(
              title: AppLocalizations.of(context)
                  .translate(AppLocalizationKeys.reviewTest),
              onTap: () {
                Navigator.pop(context);
              },
            ),
            confirmAction: AlertAcitonConfig(
              title: AppLocalizations.of(context)
                  .translate(AppLocalizationKeys.completionTest),
              onTap: () async {
                await _bloc.saveHistory();
                Navigator.pop(context);

                DSAlert.shared.showTestResultAlert(
                    context,
                    DSTestResultConfig(
                      type: testResult.type,
                      testResult: testResult.resultInDouble,
                      testResultDescription: testResult.description,
                      onTapTryAgain: () {
                        if (_isInterstitialAdReady) {
                          _interstitialAd.show();
                        }
                      },
                      onTapViewHistories: () {
                        _onViewHistories();
                      },
                      onTapViewResult: () {
                        _onViewResult();
                      },
                    ));
              },
            )));
  }

  _onViewHistories() {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) {
              return TestHistoriesPage(
                hasFloatingButton: true,
              );
            },
            fullscreenDialog: true));
  }

  _onViewResult() {
    if (RewardedVideoAdManager.instance.isRewardedAdReady) {
      RewardedVideoAdManager.instance.onReload = () {
        _bloc.reload();
      };
      RewardedVideoAdManager.instance.onRewarded = () {
        Navigator.pop(context);
        _bloc.viewQuestionGroupResult(isTesting: true);
      };
      RewardedVideoAd.instance.show();
    }
  }
}
