part of '../questions_preview_page.dart';

extension _PinBottomSection on _LearningQuestionsPageState {
  Positioned _buildPinBottomWidget() {
    return Positioned(
        bottom: 0,
        left: 0,
        right: 0,
        height: 100,
        child: Material(
          color: Colors.transparent,
          child: Container(
            child: StreamBuilder(
                stream: _bloc.answersStream,
                builder: (context, snapshot) {
                  return Container(
                    margin: const EdgeInsets.all(20),
                    child: Row(children: [
                      CustomFlatButtonComponent(
                        backgrouundColor: Colors.red[400],
                        titleColor: Colors.white,
                        onTap: () {
                          _bloc.onSubmited(
                            callBack: (status) {
                              switch (status) {
                                case AnsweredCallBackStatus.successful:
                                  break;
                                case AnsweredCallBackStatus.goToNextQuestion:
                                  break;
                                case AnsweredCallBackStatus
                                    .confirmForFinishTest:
                                  showConfirmAlert(
                                      _bloc.questionStrategy.result);
                                  break;
                                case AnsweredCallBackStatus
                                    .confirmFinishLearning:
                                  break;
                                case AnsweredCallBackStatus.showQuestionResult:
                                  break;
                                case AnsweredCallBackStatus.updateQuestionUI:
                                  break;
                              }
                            },
                          );
                        },
                        title: AppLocalizations.of(context).translate(_bloc
                            .questionStrategy.submitButtonTitleLocalizationKey),
                      )
                    ]),
                    // color: Colors.transparent,
                  );
                }),
          ),
        ));
  }
}
