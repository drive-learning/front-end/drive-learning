part of '../questions_preview_page.dart';

extension _QuestionContentSection on _LearningQuestionsPageState {
  Container _buildQuestionWidget(Question question) {
    return Container(
        color: Colors.transparent,
        child: CustomScrollView(
          slivers: [
            SliverToBoxAdapter(
              child: Container(
                height: 64,
              ),
            ),
            SliverToBoxAdapter(
                child: Container(
                    margin: const EdgeInsets.only(
                        left: 16, right: 16, top: 0, bottom: 16),
                    padding: const EdgeInsets.all(20),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        border: Border.all(color: Colors.blueGrey, width: 0.25),
                        borderRadius: BorderRadius.circular(8),
                        boxShadow: [
                          BoxShadow(
                              offset: Offset(0, 2),
                              blurRadius: 4,
                              color: Colors.blueGrey.withOpacity(0.5))
                        ]),
                    child: Column(
                      children: [
                        Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Icon(
                                Icons.arrow_right_sharp,
                                color: Colors.green,
                              ),
                              SizedBox(
                                width: 4,
                              ),
                              Expanded(
                                  child: Column(
                                children: [
                                  Container(
                                    color: Colors.white,
                                    child: Row(
                                      children: [
                                        Container(
                                          alignment: Alignment.center,
                                          decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(4),
                                            color: Colors.green[100],
                                          ),
                                          padding: const EdgeInsets.symmetric(
                                              horizontal: 16, vertical: 6),
                                          child: Text(
                                            _bloc.categoryTitle,
                                            style: TextStyle(
                                                color: Colors.green[600],
                                                fontWeight: FontWeight.bold,
                                                fontSize: 16),
                                            textAlign: TextAlign.center,
                                          ),
                                        ),
                                        SizedBox(
                                          width: 4,
                                        ),
                                        Container(
                                          alignment: Alignment.center,
                                          decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(4),
                                            color: Colors.red[100],
                                          ),
                                          padding: const EdgeInsets.symmetric(
                                              horizontal: 16, vertical: 6),
                                          child: Text(
                                            widget.setQuestion.displayTitle,
                                            style: TextStyle(
                                                color: Colors.red,
                                                fontWeight: FontWeight.bold,
                                                fontSize: 16),
                                            textAlign: TextAlign.center,
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                  SizedBox(
                                    height: 12,
                                  ),
                                  Text(
                                    question.description,
                                    style: TextStyle(
                                        color: Colors.grey[800],
                                        fontSize: 20,
                                        fontWeight: FontWeight.w400),
                                  ),
                                ],
                              ))
                            ]),
                        Visibility(
                          visible: question.isHasImage,
                          child: AspectRatio(
                            aspectRatio: 16 / 9,
                            child: Container(
                              color: Colors.white,
                              child: Image.asset(
                                "assets/images/" + question.imageName,
                                fit: BoxFit.scaleDown,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ))),
            StreamBuilder(
                stream: _bloc.answersStream,
                builder: (context, snapshot) {
                  return SliverList(
                    delegate: SliverChildBuilderDelegate(
                      (BuildContext context, int index) {
                        return AnswerComponent(
                          answer: question.answers[index],
                          isSelected:
                              index == question.currentSelectedAnswerIndex,
                          questionViewType: question.currentViewMode,
                          onTap: () {
                            _bloc.onTapAnswer(index);
                          },
                        );
                      },
                      childCount: question.answers.length,
                    ),
                  );
                }),
            SliverToBoxAdapter(
                child: Visibility(
                    visible: _bloc.presentMode ==
                        QuestionGroupPresentationMode.learning,
                    child: Container(
                        height: 56,
                        decoration: BoxDecoration(
                          color: Colors.green,
                        )))),
            SliverToBoxAdapter(
              child: Container(
                height: 156,
              ),
            )
          ],
        ));
  }
}
