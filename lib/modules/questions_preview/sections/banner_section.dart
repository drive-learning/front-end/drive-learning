part of '../questions_preview_page.dart';

extension _BannerSection on _LearningQuestionsPageState {
  Container _buildBannerWidget() {
    return Container(
      alignment: Alignment.topLeft,
      decoration: BoxDecoration(
          color: widget.primaryColor,
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.elliptical(widget.bannerHeight + 40, 50),
              bottomRight: Radius.elliptical(widget.bannerHeight + 40, 50))),
      padding: EdgeInsets.only(left: 20, right: 20, top: 64),
      height: widget.bannerHeight - 24,
      child: Lottie.asset(Lottiesconstant.bannerQuestions, fit: BoxFit.fill),
    );
  }
}
