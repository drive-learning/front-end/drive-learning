part of '../questions_preview_page.dart';

extension BannerAdMobs on _LearningQuestionsPageState {
  void _initializeBannerAd() {
    // _bannerAd =
    //     BannerAd(adUnitId: AdManager.bannerAdUnitId, size: AdSize.fullBanner);
    // _loadBannerAd();
  }

  void _loadBannerAd() {
    _bannerAd
      ..load()
      ..show(anchorType: AnchorType.bottom, anchorOffset: 88);
  }
}

extension InterstitialAdMobs on _LearningQuestionsPageState {
  void _initializeInterstitialAd() {
    _isInterstitialAdReady = false;

    _interstitialAd = InterstitialAd(
      adUnitId: AdManager.interstitialAdUnitId,
      listener: _onInterstitialAdEvent,
    );

    _loadInterstitialAd();
  }

  void _loadInterstitialAd() {
    _interstitialAd.load();
  }

  void _onInterstitialAdEvent(MobileAdEvent event) {
    debugPrint("🥩🥩🥩 Interstitial ad event ${event.toString()}}");
    switch (event) {
      case MobileAdEvent.loaded:
        _bloc.reload();
        _isInterstitialAdReady = true;
        break;
      case MobileAdEvent.failedToLoad:
        _isInterstitialAdReady = false;
        break;
      case MobileAdEvent.closed:
        _bloc.reload();
        _isInterstitialAdReady = false;
        _initializeInterstitialAd();
        _onTryAgain();

        break;
      default:
      // do nothing
    }
  }

  _onTryAgain() {
    Navigator.push(context, MaterialPageRoute(
      builder: (context) {
        return QuestionsPreviewPage(
          presentationMode: widget.presentationMode,
          primaryColor: widget.primaryColor,
          setQuestion: widget.setQuestion,
        );
      },
    ));
  }
}
