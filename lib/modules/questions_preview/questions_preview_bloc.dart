import 'dart:async';
import 'package:vn_drive_learning/common/base/bloc_base.dart';
import 'package:vn_drive_learning/modules/questions_preview/strategies/question/answered_callback_status.dart';
import 'package:vn_drive_learning/modules/questions_preview/strategies/question/question_group.dart';
import 'package:vn_drive_learning/modules/questions_preview/strategies/question/question_group_presentation_mode.dart';
import 'package:vn_drive_learning/modules/questions_preview/strategies/question/question_strategy.dart';
import 'package:vn_drive_learning/modules/questions_preview/strategies/utils/question_manager.dart';
import 'package:vn_drive_learning/services/entity/history.dart';
import 'package:vn_drive_learning/services/entity/question.dart';
import 'package:vn_drive_learning/services/entity/set_question.dart';
import 'package:vn_drive_learning/services/entity/test_result.dart';
import 'package:vn_drive_learning/services/repositories/questions_repository.dart';

final int limit = 10;

class QuestionsPreviewBloc extends BlocBase {
  StreamController _controller = new StreamController();
  Stream get questionsStream => _controller.stream;

  StreamController _answersController = StreamController.broadcast();
  Stream get answersStream => _answersController.stream;

  QuestionsRepository _questionsRepository = QuestionsRepository();
  HistoriesRepository _historiesRepository = HistoriesRepository();

  String categoryTitle = "";

  final SetQuestion setQuestion;
  QuestionStrategy questionStrategy = QuestionGroupManager.questionConcept(
      questionGroup: QuestionGroup(questions: []),
      presentationMode: QuestionGroupPresentationMode.testing);

  QuestionsPreviewBloc({this.setQuestion});
  List<Question> _questions = List();
  int currQuestionIndex = 0;

  TestResult testResult = TestResult(totalCorrectQuestion: 0, totalQuestion: 0);
  bool isLastQuestion = false;

  QuestionGroupPresentationMode _presentMode;
  setPresentMode(QuestionGroupPresentationMode presentMode) {
    _presentMode = presentMode;
    _controller.sink.add(true);
  }

  QuestionGroupPresentationMode get presentMode => _presentMode;
  Question get currQuestion => questionStrategy.currentQuestion;

  fetQuestions(int setQuestionId) async {
    categoryTitle = await setQuestion.categoryTitle();
    final newQuestions =
        await _questionsRepository.fetchQuestions(setQuestionId);
    if (newQuestions.isNotEmpty) {
      questionStrategy = QuestionGroupManager.questionConcept(
          questionGroup: QuestionGroup(questions: newQuestions),
          presentationMode: presentMode);
      _controller.sink.add(true);
    }
  }

  onTapAnswer(int answerIndex) {
    questionStrategy.onSelectedAnswer(answerIndex);
    _answersController.sink.add(true);
  }

  updateCurrentQuestionIndex(int newQuestionIndex) {
    questionStrategy.updateQuesionIndex(newQuestionIndex);
    _controller.sink.add(true);
  }

  onSubmited({Function(AnsweredCallBackStatus) callBack}) {
    questionStrategy.onSubmited(
      completionHandler: (status) {
        callBack(status);
        switch (status) {
          case AnsweredCallBackStatus.successful:
            break;
          case AnsweredCallBackStatus.goToNextQuestion:
            _controller.sink.add(true);
            break;
          case AnsweredCallBackStatus.confirmForFinishTest:
            break;
          case AnsweredCallBackStatus.confirmFinishLearning:
            break;
          case AnsweredCallBackStatus.showQuestionResult:
            _answersController.sink.add(true);
            break;
          case AnsweredCallBackStatus.updateQuestionUI:
            _answersController.sink.add(true);
            break;
        }
      },
    );
  }

  viewQuestionGroupResult({bool isTesting = false}) {
    if (isTesting) {
      _presentMode = QuestionGroupPresentationMode.learning;
    }
    questionStrategy.viewQuestionGroupResult();
    _controller.sink.add(true);
  }

  reset() {
    _questions = _questions.map<Question>((e) => e.reset()).toList();
    _controller.sink.add(true);
  }

  saveHistory() async {
    final TestResult testResult = questionStrategy.result;
    final history = History(
        setQuestionId: setQuestion.id,
        categoryId: setQuestion.categoryId,
        description: testResult.description,
        setQuestionName: "Bộ đề ${setQuestion.id}",
        testResult: testResult.resultInDouble,
        testResultType: testResult.isPassed ? 1 : 0);

    await _historiesRepository.createHistory(history);
  }

  reload() {
    _controller.sink.add(true);
  }

  @override
  void dispose() {
    _answersController.close();
    _controller.close();
  }
}
