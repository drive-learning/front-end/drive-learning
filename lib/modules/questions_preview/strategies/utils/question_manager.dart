import 'package:vn_drive_learning/main.dart';
import 'package:vn_drive_learning/modules/questions_preview/strategies/question/question_group.dart';
import 'package:vn_drive_learning/modules/questions_preview/strategies/question/question_group_presentation_mode.dart';
import 'package:vn_drive_learning/modules/questions_preview/strategies/question/question_strategy.dart';
import 'package:vn_drive_learning/modules/questions_preview/strategies/question/random_question_strategy.dart';
import 'package:vn_drive_learning/modules/questions_preview/strategies/question/sequential_question_strategy.dart';
import 'package:vn_drive_learning/services/storage/local_storage_service.dart';

class QuestionGroupManager {
  static QuestionStrategy questionConcept(
      {QuestionGroup questionGroup,
      QuestionGroupPresentationMode presentationMode}) {
    final storageService = localtor<LocalStorageServiceType>();
    if (storageService.settingsData.repeatQuestionStatus.isOn) {
      return SequentialQuestionStrategy(
          questionGroup: questionGroup, presentationMode: presentationMode);
    }

    return RandomQuestionStrategy(
        questionGroup: questionGroup, presentationMode: presentationMode);
  }
}
