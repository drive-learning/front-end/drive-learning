import 'package:vn_drive_learning/modules/questions_preview/strategies/question/answered_callback_status.dart';
import 'package:vn_drive_learning/modules/questions_preview/strategies/question/question_group_presentation_mode.dart';
import 'package:vn_drive_learning/services/entity/question.dart';
import 'package:vn_drive_learning/services/entity/test_result.dart';

class QuestionStrategy {
  QuestionGroupPresentationMode presentationMode;
  String title;

  int correctCount;
  int inCorrectCount;
  int get totalQuestion => throw UnimplementedError();

  bool get advanceToNextQuestion => throw UnimplementedError();
  Question get currentQuestion => throw UnimplementedError();
  String get questionIndexTitle => throw UnimplementedError();

  markQuestionCorrect({Question question}) {}
  markQuestionIncorrect({Question question}) {}

  onSubmited({Function(AnsweredCallBackStatus) completionHandler}) {}

  String get submitButtonTitleLocalizationKey => throw UnimplementedError();
  double get percentProcessing => throw UnimplementedError();
  bool get isNotEmpty => throw UnimplementedError();
  List<Question> get questions => throw UnimplementedError();
  TestResult get result => throw UnimplementedError();
  onSelectedAnswer(int answerIndex) => throw UnimplementedError();

  updateQuesionIndex(int newQuestionIndex) {}
  viewQuestionGroupResult() {}
}
