enum AnsweredCallBackStatus {
  successful,
  goToNextQuestion,
  confirmForFinishTest,
  confirmFinishLearning,
  showQuestionResult,
  updateQuestionUI
}
