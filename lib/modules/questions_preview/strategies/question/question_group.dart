import 'package:flutter/material.dart';
import 'package:vn_drive_learning/services/entity/question.dart';

class QuestionGroup {
  List<Question> questions;
  String title;

  QuestionGroup({@required this.questions, this.title});
}
