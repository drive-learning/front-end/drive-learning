import 'package:vn_drive_learning/common/components/question_view_type.dart';
import 'package:vn_drive_learning/common/constant/locale/app_localization_keys.dart';
import 'package:vn_drive_learning/modules/questions_preview/strategies/question/answered_callback_status.dart';
import 'package:vn_drive_learning/modules/questions_preview/strategies/question/question_group.dart';
import 'package:vn_drive_learning/modules/questions_preview/strategies/question/question_group_presentation_mode.dart';
import 'package:vn_drive_learning/modules/questions_preview/strategies/question/question_strategy.dart';
import 'package:vn_drive_learning/services/entity/question.dart';
import 'package:vn_drive_learning/services/entity/test_result.dart';
import 'package:vn_drive_learning/utils/sounds_managaer/sound_type.dart';
import 'package:vn_drive_learning/utils/sounds_managaer/sounds_manager.dart';

class SequentialQuestionStrategy implements QuestionStrategy {
  @override
  int correctCount = 0;

  @override
  int inCorrectCount = 0;

  @override
  String title = "";

  final QuestionGroup questionGroup;

  int questionIndex = 0;

  SequentialQuestionStrategy({this.questionGroup, this.presentationMode});

  @override
  bool get advanceToNextQuestion {
    if (questionIndex + 1 < questionGroup.questions.length) {
      questionIndex += 1;
      return true;
    }

    return false;
  }

  @override
  Question get currentQuestion => questionGroup.questions[questionIndex];

  @override
  markQuestionCorrect({Question question}) {
    correctCount += 1;
  }

  @override
  markQuestionIncorrect({Question question}) {
    inCorrectCount += 1;
  }

  @override
  String get questionIndexTitle =>
      "${questionIndex + 1} / ${questionGroup.questions.length}";

  @override
  onSubmited({Function(AnsweredCallBackStatus) completionHandler}) {
    /**
     * `presentation mode` is `testing`
     *     - update current question to answered and move to next question
     *     
     */
    if (presentationMode == QuestionGroupPresentationMode.testing) {
      if (currentQuestion.isAnswered) {
        currentQuestion.isCorrect
            ? markQuestionCorrect()
            : markQuestionIncorrect();
      }
      if (advanceToNextQuestion) {
        completionHandler(AnsweredCallBackStatus.goToNextQuestion);
        return;
      }

      completionHandler(AnsweredCallBackStatus.confirmForFinishTest);
      return;
    }

    /**`presentation mode` is `learning` `(always show result for user for learning)`
      */

    /// Question was answered
    if (currentQuestion.isAnswered) {
      switch (currentQuestion.currentViewMode) {
        case QuestionViewType.testing:
          if (currentQuestion.isCorrect) {
            SoundsManager.shared.play(soundType: SoundType.correctAnswer);
          } else {
            SoundsManager.shared.play(soundType: SoundType.wrongAnswer);
          }
          questionGroup.questions[questionIndex].currentViewMode =
              QuestionViewType.viewResult;
          completionHandler(AnsweredCallBackStatus.showQuestionResult);
          return;

        case QuestionViewType.viewResult:
          if (advanceToNextQuestion) {
            completionHandler(AnsweredCallBackStatus.goToNextQuestion);
            return;
          }

          completionHandler(AnsweredCallBackStatus.confirmFinishLearning);
          return;
      }
    }

    /// Question was not answered before
    questionGroup.questions[questionIndex].isAnswered = true;
    completionHandler(AnsweredCallBackStatus.updateQuestionUI);
  }

  @override
  String get submitButtonTitleLocalizationKey {
    switch (presentationMode) {
      case QuestionGroupPresentationMode.learning:
        if (currentQuestion.isAnswered) {
          if (currentQuestion.currentViewMode == QuestionViewType.testing) {
            return AppLocalizationKeys.answer;
          }

          return AppLocalizationKeys.next;
        }

        return AppLocalizationKeys.skip;

      case QuestionGroupPresentationMode.testing:
        if (currentQuestion.isAnswered) {
          return AppLocalizationKeys.next;
        }

        if (currentQuestion.currentSelectedAnswerIndex != -1) {
          return AppLocalizationKeys.answer;
        }

        return AppLocalizationKeys.skip;
    }
    return "";
  }

  @override
  QuestionGroupPresentationMode presentationMode =
      QuestionGroupPresentationMode.testing;

  @override
  double get percentProcessing {
    return (correctCount + inCorrectCount).toDouble() /
        totalQuestion.toDouble();
  }

  @override
  int get totalQuestion => questionGroup.questions.length;

  @override
  bool get isNotEmpty => questionGroup.questions.isNotEmpty;

  @override
  List<Question> get questions => questionGroup.questions;

  @override
  TestResult get result {
    return TestResult(
        totalCorrectQuestion: correctCount, totalQuestion: totalQuestion);
  }

  @override
  onSelectedAnswer(int answerIndex) {
    SoundsManager.shared.play(soundType: SoundType.seletedAnswer);
    Question _currentQuestion = currentQuestion;
    if (answerIndex == _currentQuestion.currentSelectedAnswerIndex) {
      _currentQuestion.currentSelectedAnswerIndex = -1;
      _currentQuestion.isAnswered = false;
    } else {
      _currentQuestion.currentSelectedAnswerIndex = answerIndex;
      _currentQuestion.isAnswered = true;
    }

    questionGroup.questions[questionIndex] = _currentQuestion;
  }

  @override
  updateQuesionIndex(int newQuestionIndex) {
    questionIndex = newQuestionIndex;
  }

  @override
  viewQuestionGroupResult() {
    questionGroup.questions = questionGroup.questions.map<Question>((e) {
      e.isAnswered = true;
      e.currentViewMode = QuestionViewType.viewResult;
      return e;
    }).toList();
  }
}
