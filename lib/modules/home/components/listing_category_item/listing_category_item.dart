import 'package:flutter/material.dart';
import 'package:vn_drive_learning/common/constant/images/images_constant.dart';

class ListingCategoryComponent extends StatelessWidget {
  final String title;
  final Color backgroundColor;
  final VoidCallback onTap;
  ListingCategoryComponent(
      {Key key, this.title, this.backgroundColor, this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
        onTap: () {
          onTap();
        },
        child: Container(
          margin: const EdgeInsets.symmetric(horizontal: 16),
          padding: const EdgeInsets.symmetric(horizontal: 16),
          height: 148,
          decoration: BoxDecoration(
              color: backgroundColor, borderRadius: BorderRadius.circular(8)),
          child: Row(
            children: [
              RichText(
                  text: TextSpan(
                      text: title.toUpperCase(),
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 20,
                          fontWeight: FontWeight.bold))),
              Spacer(),
              Image.asset(ImagesConstant.icPlay)
            ],
          ),
        ));
  }
}
