import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:vn_drive_learning/services/entity/category.dart';

class CategoryItemComponent extends StatelessWidget {
  final Category category;
  final Color itemBackgroundColor;
  final Function(Category) onTap;
  CategoryItemComponent(
      {Key key, this.category, this.itemBackgroundColor, this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => onTap(category),
      child: Ink(
          height: 180,
          width: 200,
          decoration: BoxDecoration(
              color: itemBackgroundColor,
              borderRadius: BorderRadius.circular(8),
              border: Border.all(color: Colors.grey, width: 0.5)),
          child: Column(
            children: [
              Expanded(
                  child: Ink(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(8),
                      topRight: Radius.circular(8)),
                ),
                child: Center(
                  child: Lottie.asset(
                      "assets/lottie/home/" + category.imageName,
                      fit: BoxFit.fitHeight,
                      repeat: true),
                ),
              )),
              Expanded(
                child: Container(
                  padding: const EdgeInsets.only(top: 8),
                  child: Text(
                    category.title.toUpperCase(),
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 16,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              )
            ],
          )),
    );
  }
}
