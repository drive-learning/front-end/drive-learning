import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:vn_drive_learning/modules/home/components/titles/section_title/section_title_custom.dart';

class HomeSectionTitle extends StatelessWidget {
  final String title;
  final bool hasViewMore;
  const HomeSectionTitle({Key key, this.hasViewMore = true, this.title = ""})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 16, right: 16, top: 8),
      child: Row(
        children: [
          SectionTitleCustomComponent(
            title: title,
          ),
          Spacer(),
          Visibility(
              visible: hasViewMore,
              child: FlatButton(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(16)),
                  color: Colors.yellow[800],
                  onPressed: () => print("viewmore"),
                  child: Text(
                    "Xem thêm",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 16,
                        fontWeight: FontWeight.w500),
                  )))
        ],
      ),
    );
  }
}
