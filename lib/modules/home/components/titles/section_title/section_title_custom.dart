import 'package:flutter/cupertino.dart';
import 'package:vn_drive_learning/common/constant/color/colors_constant.dart';

class SectionTitleCustomComponent extends StatelessWidget {
  final String title;
  const SectionTitleCustomComponent({Key key, this.title = ""})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 24,
      child: Stack(
        children: [
          Text(
            title,
            style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.bold,
                color: ColorsContant.sectionTextColor),
          )
        ],
      ),
    );
  }
}
