part of '../body.dart';

extension QucikTest on Body {
  SliverToBoxAdapter _buildQuickTestComponent(BuildContext context) {
    return SliverToBoxAdapter(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            height: 16,
          ),
          HeaderSectionComponent(
              title: "Thi nhanh",
              description: "Con đường nhanh nhất để lấy giấy phép nè",
              paddingValue: 16),
          ListingCategoryComponent(
            title: "Thi nhanh",
            backgroundColor: ColorsContant.wrongSelectedAnswerColor,
            onTap: () async {
              if (onUseQuicktestFeature != null) {
                onUseQuicktestFeature();
              }
            },
          ),
        ],
      ),
    );
  }
}
