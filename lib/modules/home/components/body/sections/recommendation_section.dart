part of '../body.dart';

extension Recommendation on Body {
  SliverToBoxAdapter _buildRecommendationComponent() {
    final category = recommendationCategories.first;
    final primaryColor = ColorsContant.listRecommendationCategoriesColors.first;

    return SliverToBoxAdapter(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          HeaderSectionComponent(
              title: "Có thể bạn thích học",
              description: "Gợi ý cho bạn 1 số bộ câu hỏi hay nhất",
              paddingValue: 16),
          ListingCategoryComponent(
            title: category.title,
            backgroundColor: primaryColor,
            onTap: () async {
              debugPrint("☢️☢️☢️ Show admod for some magic ☢️☢️☢️");
              interstitialAd.load().then((value) => interstitialAd.show());
            },
          ),
        ],
      ),
    );
  }
}
