part of '../body.dart';

extension Categories on Body {
  SliverPadding _buildCategoreisSliver() {
    return SliverPadding(
      padding: EdgeInsets.all(16),
      sliver: SliverGrid(
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
            childAspectRatio: 0.75,
            mainAxisSpacing: 16,
            crossAxisSpacing: 16),
        delegate: SliverChildBuilderDelegate(
          (context, index) {
            return CategoryItemComponent(
              category: categories[index],
              itemBackgroundColor: ColorsContant.listCategoriesColors[index],
              onTap: (category) {
                Navigator.push(context, MaterialPageRoute(
                  builder: (context) {
                    return CategoryDetailView(
                      category: category,
                      primaryColor: ColorsContant.listCategoriesColors[index],
                    );
                  },
                ));
              },
            );
          },
          childCount: categories.length,
        ),
      ),
    );
  }
}
