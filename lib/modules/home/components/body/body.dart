import 'package:firebase_admob/firebase_admob.dart';
import 'package:flutter/material.dart';
import 'package:vn_drive_learning/common/constant/color/colors_constant.dart';
import 'package:vn_drive_learning/design_system/ds_alert/alerts/purchase_gem_alert/purchase_point_alert_config.dart';
import 'package:vn_drive_learning/design_system/ds_alert/ds_alert.dart';
import 'package:vn_drive_learning/modules/category_detail/category_detail_view.dart';
import 'package:vn_drive_learning/modules/histories/components/header_section/header_section_component.dart';
import 'package:vn_drive_learning/modules/home/components/special_category_item/special_category_item.dart';
import 'package:vn_drive_learning/modules/home/components/titles/home_section_title/home_section_title.dart';
import 'package:vn_drive_learning/modules/home/components/listing_category_item/listing_category_item.dart';
import 'package:vn_drive_learning/services/entity/category.dart';
import 'package:vn_drive_learning/services/entity/user_info/user_info.dart';
import 'package:vn_drive_learning/utils/gem_package_manager/gem_package_manager.dart';
import '../profile/home_profile.dart';

part 'sections/quick_test_section.dart';
part 'sections/recommendation_section.dart';
part 'sections/categories_section.dart';

class Body extends StatelessWidget {
  final List<Category> categories;
  final List<Category> recommendationCategories;
  final InterstitialAd interstitialAd;
  final VoidCallback onConfirmToQuickTest;
  final VoidCallback onUseQuicktestFeature;

  Body(
      {this.categories,
      this.recommendationCategories,
      this.interstitialAd,
      this.onConfirmToQuickTest,
      this.onUseQuicktestFeature});
  @override
  Widget build(Object context) {
    Size size = MediaQuery.of(context).size;

    return CustomScrollView(
      slivers: [
        SliverToBoxAdapter(
          child: HomeProfileComponent(
            size: size,
          ),
        ),
        _buildRecommendationComponent(),
        _buildQuickTestComponent(context),
        SliverToBoxAdapter(
          child: HomeSectionTitle(
            title: "Danh mục".toUpperCase(),
          ),
        ),
        _buildCategoreisSliver(),
        SliverToBoxAdapter(
            // child: AdmobBanner(
            //     adUnitId: AdManager.bannerAdUnitId,
            //     adSize: AdmobBannerSize.LARGE_BANNER),
            )
      ],
    );
  }
}
