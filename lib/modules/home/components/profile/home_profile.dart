import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import 'package:vn_drive_learning/common/constant/images/icons_constant.dart';
import 'package:vn_drive_learning/services/entity/user_info/user_info.dart';

class HomeProfileComponent extends StatelessWidget {
  const HomeProfileComponent({
    Key key,
    @required this.size,
  }) : super(key: key);

  final Size size;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 200,
      color: Colors.white,
      child: Stack(
        children: [
          Container(
            alignment: Alignment.topLeft,
            color: Colors.white,
            padding: EdgeInsets.only(left: 20, right: 20, top: 64),
            height: 200.0 - 24,
            child: Row(
              children: [
                _buildAvatarView(),
                SizedBox(
                  width: 8,
                ),
                buildUserInfoView(),
                Spacer(),
                buildUserGemView()
              ],
            ),
          ),
        ],
      ),
    );
  }

  Container buildUserGemView() {
    return Container(
      height: 48,
      child: Row(
        children: [
          Text(
            UserInfoSingleton.shared.totalGem.toString(),
            style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.w700,
                color: Colors.grey[600]),
            textAlign: TextAlign.right,
          ),
          SizedBox(
            width: 4,
          ),
          Image.asset(
            IconsConstant.gem,
            height: 32,
            fit: BoxFit.fitHeight,
          )
        ],
      ),
    );
  }

  Container buildUserInfoView() {
    return Container(
      height: 72,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            UserInfoSingleton.shared.userName,
            style: TextStyle(
                fontSize: 16,
                color: Colors.grey[600],
                fontWeight: FontWeight.w800),
          ),
          Text(
            UserInfoSingleton.shared.getLicenseDisplayedName(),
            style: TextStyle(
                fontSize: 14,
                color: Colors.grey[600],
                fontWeight: FontWeight.w400),
          ),
          LinearPercentIndicator(
            width: 140,
            lineHeight: 12,
            animation: true,
            percent: 0.8,
            clipLinearGradient: true,
            backgroundColor: Colors.grey[400],
            linearGradient: LinearGradient(colors: [
              Colors.pinkAccent,
              Colors.yellow[800],
            ]),
          ),
        ],
      ),
    );
  }

  Container _buildAvatarView() {
    return Container(
      height: 72,
      width: 72,
      decoration:
          BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(36))),
      child: Lottie.asset(UserInfoSingleton.shared.avatarLottieAsset),
    );
  }
}
