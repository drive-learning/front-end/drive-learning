import 'package:firebase_admob/firebase_admob.dart';
import 'package:flutter/material.dart';
import 'package:vn_drive_learning/common/constant/color/colors_constant.dart';
import 'package:vn_drive_learning/design_system/ds_alert/alerts/purchase_gem_alert/purchase_point_alert_config.dart';
import 'package:vn_drive_learning/design_system/ds_alert/ds_alert.dart';
import 'package:vn_drive_learning/modules/category_detail/category_detail_view.dart';
import 'package:vn_drive_learning/modules/histories/components/header_section/header_section_component.dart';
import 'package:vn_drive_learning/modules/home/components/special_category_item/special_category_item.dart';
import 'package:vn_drive_learning/modules/home/components/profile/home_profile.dart';
import 'package:vn_drive_learning/modules/home/components/titles/home_section_title/home_section_title.dart';
import 'package:vn_drive_learning/modules/home/components/listing_category_item/listing_category_item.dart';
import 'package:vn_drive_learning/modules/home/home_page_bloc.dart';
import 'package:vn_drive_learning/modules/questions_preview/questions_preview_page.dart';
import 'package:vn_drive_learning/modules/questions_preview/strategies/question/question_group_presentation_mode.dart';
import 'package:vn_drive_learning/utils/adds/ad_manager/ad_id_manager.dart';
import 'package:vn_drive_learning/utils/adds/ad_manager/rewarded_ads_manager.dart';
import 'package:vn_drive_learning/utils/gem_package_manager/gem_package_manager.dart';

part 'sections/main.dart';
part 'sections/profile.dart';
part 'sections/recommendation.dart';
part 'sections/fastest_testing.dart';
part 'sections/special_categories.dart';
part 'sections/admobs/admobs.dart';

class HomePageView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _HomePageState();
  }
}

class _HomePageState extends State<HomePageView> {
  HomePageBloc _bloc;

  GlobalKey<ScaffoldState> scaffoldState = GlobalKey();

  bool _isRecommendationInterstitialAdReady = false;
  InterstitialAd _recommendationInterstitialAd;

  @override
  void initState() {
    super.initState();

    // initializeInterstitialAd();
    _bloc = HomePageBloc()
      ..fetchCategoriesList()
      ..fetchRecommendationCategories();

    _initializeRecommendationInterstitialAd();
    RewardedVideoAdManager.instance.configureRewardedAd();
  }

  void showSnackBar(String content) {
    scaffoldState.currentState.showSnackBar(SnackBar(
      content: Text(content),
      duration: Duration(milliseconds: 1500),
    ));
  }

  @override
  void dispose() {
    _bloc.dispose();
    // interstitialAd.dispose();
    RewardedVideoAd.instance.listener = null;

    super.dispose();
  }

  @override
  Widget build(Object context) {
    return Scaffold(
        body: StreamBuilder(
      stream: _bloc.categoriesStream,
      builder: (context, snapshot) {
        if (snapshot.hasData && _bloc.recommendationCategories.isNotEmpty) {
          final categories = _bloc.categories;
          final recommendationCategories = _bloc.recommendationCategories;
          //     return Body(
          //       categories: categories,
          //       recommendationCategories: recommendationCategories,
          //       interstitialAd: null,
          //       onConfirmToQuickTest: () async {},
          //       onUseQuicktestFeature: () {
          //         if (GemPackageManager.shared.isEnounghtGemForLearn) {
          //           DSAlert.shared.showPointPurchaseGemAlert(context,
          //               config: DSPurchasePointAlertConfig(
          //                   onConfirm: () {
          //                     debugPrint(
          //                         "🥩🥩🥩  Everything OK for confirm 🥩🥩🥩");

          //                     onConfirmToQuickTest();
          //                   },
          //                   neededPaymentTotalGem: GemPackageManager
          //                       .shared.gemPaymentPerLearningIime));
          //         } else {
          //           GemPackageManager.shared.onClosedReward = () {
          //             _bloc.reload();
          //             if (GemPackageManager.shared.isEnounghtGemForLearn) {
          //               DSAlert.shared.showPointPurchaseGemAlert(context,
          //                   config: DSPurchasePointAlertConfig(
          //                       onConfirm: () {
          //                         debugPrint(
          //                             "🥩🥩🥩  Everything OK for confirm 🥩🥩🥩");

          //                         onConfirmToQuickTest();
          //                       },
          //                       neededPaymentTotalGem: GemPackageManager
          //                           .shared.gemPaymentPerLearningIime));
          //             }
          //           };
          //           GemPackageManager.shared.useGemForLearn(() {
          //             if (GemPackageManager.shared.isEnounghtGemForLearn) {
          //               DSAlert.shared.showPointPurchaseGemAlert(context,
          //                   config: DSPurchasePointAlertConfig(
          //                       onConfirm: () {
          //                         debugPrint(
          //                             "🥩🥩🥩  Everything OK for confirm 🥩🥩🥩");

          //                         onConfirmToQuickTest();
          //                       },
          //                       neededPaymentTotalGem: GemPackageManager
          //                           .shared.gemPaymentPerLearningIime));
          //             }
          //           });
          //         }
          //       },
          //     );
          return _buildMainSection();
        }

        return Container(
          color: Colors.red,
        );
      },
    ));
  }
}
