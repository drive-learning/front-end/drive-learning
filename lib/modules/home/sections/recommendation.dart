part of '../home_page_view.dart';

extension RecommendationSection on _HomePageState {
  SliverToBoxAdapter get recommendationSection {
    final category = _bloc.recommendationCategories.first;
    final primaryColor = ColorsContant.listRecommendationCategoriesColors.first;

    return SliverToBoxAdapter(
        child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        HeaderSectionComponent(
            title: "Có thể bạn thích học",
            description: "Gợi ý cho bạn 1 số bộ câu hỏi hay nhất",
            paddingValue: 16),
        ListingCategoryComponent(
          title: category.title,
          backgroundColor: primaryColor,
          onTap: () {
            if (_isRecommendationInterstitialAdReady) {
              _recommendationInterstitialAd.show();
            }
          },
        ),
      ],
    ));
  }
}
