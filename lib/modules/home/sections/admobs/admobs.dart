part of '../../home_page_view.dart';

extension RewardedAdmobs on _HomePageState {}

extension InterstitialAdMobs on _HomePageState {
  void _initializeRecommendationInterstitialAd() {
    _isRecommendationInterstitialAdReady = false;

    _recommendationInterstitialAd = InterstitialAd(
      adUnitId: AdManager.interstitialAdUnitId,
      listener: _onInterstitialAdEvent,
    );

    _loadInterstitialAd();
  }

  void _loadInterstitialAd() {
    _recommendationInterstitialAd.load();
  }

  void _onInterstitialAdEvent(MobileAdEvent event) {
    debugPrint("🥩🥩🥩 Interstitial ad event ${event.toString()}}");
    switch (event) {
      case MobileAdEvent.loaded:
        _bloc.reload();
        _isRecommendationInterstitialAdReady = true;
        break;
      case MobileAdEvent.failedToLoad:
        _isRecommendationInterstitialAdReady = false;
        break;
      case MobileAdEvent.closed:
        _bloc.reload();
        _isRecommendationInterstitialAdReady = false;
        _initializeRecommendationInterstitialAd();
        break;
      default:
      // do nothing
    }
  }
}
