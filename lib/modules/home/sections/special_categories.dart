part of '../home_page_view.dart';

extension SpecialCategoriesGridSection on _HomePageState {
  SliverToBoxAdapter get specialCategoriesTitle => SliverToBoxAdapter(
        child: HomeSectionTitle(
          title: "Danh mục".toUpperCase(),
        ),
      );
  SliverPadding get specialCategoriesGridSection => SliverPadding(
        padding: EdgeInsets.all(16),
        sliver: SliverGrid(
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2,
              childAspectRatio: 0.75,
              mainAxisSpacing: 16,
              crossAxisSpacing: 16),
          delegate: SliverChildBuilderDelegate(
            (context, index) {
              return CategoryItemComponent(
                category: _bloc.categories[index],
                itemBackgroundColor: ColorsContant.listCategoriesColors[index],
                onTap: (category) {
                  Navigator.push(context, MaterialPageRoute(
                    builder: (context) {
                      return CategoryDetailView(
                        category: category,
                        primaryColor: ColorsContant.listCategoriesColors[index],
                      );
                    },
                  ));
                },
              );
            },
            childCount: _bloc.categories.length,
          ),
        ),
      );
}
