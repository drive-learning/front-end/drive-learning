part of '../home_page_view.dart';

extension Main on _HomePageState {
  Widget _buildMainSection() {
    return CustomScrollView(
      slivers: [
        profileSection,
        recommendationSection,
        fastestTestingSection,
        specialCategoriesTitle,
        specialCategoriesGridSection
      ],
    );
  }
}
