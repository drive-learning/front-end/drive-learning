part of '../home_page_view.dart';

extension ProfileSection on _HomePageState {
  SliverToBoxAdapter get profileSection => SliverToBoxAdapter(
        child: HomeProfileComponent(
          size: MediaQuery.of(context).size,
        ),
      );
}
