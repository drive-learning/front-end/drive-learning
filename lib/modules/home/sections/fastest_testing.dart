part of '../home_page_view.dart';

extension FastestTestingSection on _HomePageState {
  SliverToBoxAdapter get fastestTestingSection => SliverToBoxAdapter(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 16,
            ),
            HeaderSectionComponent(
                title: "Thi nhanh",
                description: "Con đường nhanh nhất để lấy giấy phép nè",
                paddingValue: 16),
            ListingCategoryComponent(
              title: "Thi nhanh",
              backgroundColor: ColorsContant.wrongSelectedAnswerColor,
              onTap: () {
                if (RewardedVideoAdManager.instance.isRewardedAdReady &&
                    GemPackageManager.shared.isNotEnounghtGemForLearn) {
                  RewardedVideoAdManager.instance.onReload = () {
                    _bloc.reload();
                  };

                  RewardedVideoAdManager.instance.onRewarded = () {
                    debugPrint("🐶🐶🐶 OK Admobs was rewarded 🐶🐶🐶");
                    GemPackageManager.shared.onIncreasing(
                        gemIncreasing:
                            GemPackageManager.shared.gemPaymentPerLearningIime,
                        callback: () {
                          _bloc.reload();
                          if (GemPackageManager.shared.isEnounghtGemForLearn) {
                            DSAlert.shared.showPointPurchaseGemAlert(context,
                                config: DSPurchasePointAlertConfig(
                                    onConfirm: () {
                                      debugPrint(
                                          "🥩🥩🥩  Everything OK for confirm 🥩🥩🥩");

                                      onConfirmToQuickTest();
                                    },
                                    neededPaymentTotalGem: GemPackageManager
                                        .shared.gemPaymentPerLearningIime));
                          }
                        });
                  };

                  RewardedVideoAd.instance.show();
                  return;
                }

                if (GemPackageManager.shared.isEnounghtGemForLearn) {
                  DSAlert.shared.showPointPurchaseGemAlert(context,
                      config: DSPurchasePointAlertConfig(
                          onConfirm: () {
                            onConfirmToQuickTest();
                          },
                          neededPaymentTotalGem: GemPackageManager
                              .shared.gemPaymentPerLearningIime));
                }
              },
            ),
          ],
        ),
      );

  void onConfirmToQuickTest() async {
    GemPackageManager.shared.onDecrasing(
        callback: () async {
          _bloc.reload();
          await _bloc.onCreatingQuikTest((setQuestion) {
            Future.delayed(Duration(milliseconds: 300), () {
              Navigator.push(context, MaterialPageRoute(
                builder: (context) {
                  return QuestionsPreviewPage(
                    presentationMode: QuestionGroupPresentationMode.testing,
                    isFromNotification: false,
                    primaryColor: Colors.green,
                    setQuestion: setQuestion,
                  );
                },
              ));
            });
          });
        },
        gemDecreasing: GemPackageManager.shared.gemPaymentPerLearningIime);
  }
}
