import 'dart:async';

import 'package:vn_drive_learning/common/base/bloc_base.dart';
import 'package:vn_drive_learning/services/database/tables/database_provider/db_provider.dart';
import 'package:vn_drive_learning/services/entity/set_question.dart';
import 'package:vn_drive_learning/services/entity/user_info/user_info.dart';
import 'package:vn_drive_learning/services/repositories/categories_repository.dart';
import 'package:vn_drive_learning/services/entity/category.dart';
import 'package:vn_drive_learning/services/entity/categoryType.dart';
import 'package:vn_drive_learning/services/repositories/user_info_repository.dart';
import 'package:vn_drive_learning/utils/gem_package_manager/gem_package_manager.dart';

class HomePageBloc extends BlocBase {
  UserInfoRepository _userInfoRepository = UserInfoRepository();
  StreamController _controller = new StreamController();
  Stream get categoriesStream => _controller.stream;

  List<Category> _categories = List();
  List<Category> _recommendationCategories = List();

  CategoriesRepository categoriesRepository = CategoriesRepository();

  List<Category> get categories => _categories;
  List<Category> get recommendationCategories => _recommendationCategories;

  fetchCategoriesList() async {
    _categories =
        await categoriesRepository.fetchCategories(0, CategoryType.testing);
    _controller.sink.add(true);
  }

  fetchRecommendationCategories() async {
    _recommendationCategories =
        await categoriesRepository.fetchCategories(0, CategoryType.learning);
    _controller.sink.add(true);
  }

  refresh() {
    _controller.sink.add(true);
  }

  // onRewardSuccess({@required VoidCallback onUpdateGemSuccess}) {
  //   GemPackageManager.shared.onIncreasing(
  //       30,
  //       DatabaseUpdateResponseCallBack(
  //         onFailed: () {
  //           debugPrint("💔💔💔 On failed reward 💔💔💔");
  //         },
  //         onSuccess: () {
  //           debugPrint("💊💊💊 On successufll reward 💊💊💊");
  //           onUpdateGemSuccess();
  //           _controller.sink.add(true);
  //         },
  //       ));
  // }

  onCreatingQuikTest(Function(SetQuestion setQuestion) onCreatedSuccess,
      {bool isChangedGem = true}) async {
    // GemPackageManager.shared.useGemForLearn(() {});
    final categoryId = UserInfoSingleton.shared.licenseType.index + 1;
    final setQuestion =
        (await DBProvider.db.fetSetQuestions(0, 99, categoryId))[4];

    // if (isChangedGem) {
    //   GemPackageManager.shared.onDecrasing(
    //       30,
    //       DatabaseUpdateResponseCallBack(
    //         onSuccess: () {
    //           _controller.sink.add(true);
    //           onCreatedSuccess(setQuestion);
    //         },
    //         onFailed: () {
    //           debugPrint("🍎🍎🍎 Gem package manager failed 🍎🍎🍎");
    //         },
    //       ));
    // } else {
    _controller.sink.add(true);
    onCreatedSuccess(setQuestion);
  }

  reload() {
    _controller.sink.add(true);
  }

  @override
  void dispose() {
    _controller.close();
  }
}
