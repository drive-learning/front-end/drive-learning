import 'dart:async';

import 'package:vn_drive_learning/common/base/bloc_base.dart';
import 'package:vn_drive_learning/services/database/tables/database_provider/db_provider.dart';
import 'package:vn_drive_learning/services/entity/user_info/user_info.dart';
import 'package:vn_drive_learning/services/repositories/user_info_repository.dart';

class TabbarBloc extends BlocBase {
  UserInfoRepository _userInfoRepository = UserInfoRepository();
  StreamController _controller = new StreamController();
  Stream get tabbarStream => _controller.stream;

  initDatabase() async {
    await DBProvider.db.createDatabse();
    await _fetchUserInfo();
    _controller.sink.add(true);
  }

  _fetchUserInfo() async {
    print("USER INFO ${UserInfoSingleton.shared.userName} ");
    await _userInfoRepository.fetchUserInfo();
  }

  @override
  void dispose() {
    _controller.close();
  }
}
