import 'package:firebase_admob/firebase_admob.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:vn_drive_learning/common/constant/color/colors_constant.dart';
import 'package:vn_drive_learning/modules/histories/test_histories_page.dart';
import 'package:vn_drive_learning/modules/home/home_page_view.dart';
import 'package:vn_drive_learning/modules/search/search_page.dart';
import 'package:vn_drive_learning/modules/settings/settings_page_view.dart';
import 'package:vn_drive_learning/modules/tabbar/tabbar_bloc.dart';
import 'package:vn_drive_learning/utils/adds/ad_manager/ad_id_manager.dart';

class TabbarButtonConfig {
  final TabbarType type;
  final String titile;
  const TabbarButtonConfig({@required this.type, this.titile});
}

class MainTabbarView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _MainTabBarState();
}

class _MainTabBarState extends State<MainTabbarView> {
  int currIndex = 0;
  List<Widget> listWidget;
  TabbarBloc _bloc;

  @override
  void initState() {
    listWidget = [
      HomePageView(),
      SearchPage(),
      TestHistoriesPage(
        hasFloatingButton: false,
      ),
      SettingsPageView()
    ];
    _bloc = TabbarBloc();
    _bloc.initDatabase();
    super.initState();
    FirebaseAdMob.instance.initialize(appId: AdManager.appId);
    // MobileAdMobsManager.configureRewardedAdmobs();
  }

  @override
  void dispose() {
    _bloc.dispose();
    super.dispose();
  }

  @override
  Widget build(Object context) {
    return Scaffold(
      body: FutureBuilder(
        // future: _initAdMob(),
        builder: (context, snapshot) {
          return IndexedStack(
            index: currIndex,
            children: listWidget,
          );
        },
      ),
      bottomNavigationBar: BottomNavigationBar(
        onTap: onTapTabbar,
        selectedItemColor: Colors.redAccent,
        unselectedItemColor: Colors.grey[400],
        currentIndex: currIndex,
        items: _buildBottomNavBarItems([
          TabbarButtonConfig(type: TabbarType.home, titile: "Trang chủ"),
          TabbarButtonConfig(type: TabbarType.searching, titile: "Tìm kiếm"),
          TabbarButtonConfig(type: TabbarType.histories, titile: "Lịch sử"),
          TabbarButtonConfig(type: TabbarType.settings, titile: "Cài đặt")
        ], currIndex),
      ),
    );
  }

  List<BottomNavigationBarItem> _buildBottomNavBarItems(
      List<TabbarButtonConfig> tabbarTypes, currentIndex) {
    List<BottomNavigationBarItem> items = [];
    tabbarTypes.asMap().forEach((index, config) {
      items.add(_buildBottomNavigationBarItem(currentIndex == index, config));
    });

    return items;
  }

  BottomNavigationBarItem _buildBottomNavigationBarItem(
      bool isSelected, TabbarButtonConfig config) {
    return BottomNavigationBarItem(
        title: Text(
          config.titile.toUpperCase(),
          style: TextStyle(
              color: ColorsContant.appPrimaryColor,
              fontSize: 6.5,
              fontWeight: FontWeight.w500),
        ),
        icon: _buildTabbarIcon(isSelected, config.type));
  }

  Widget _buildTabbarIcon(bool isSelected, TabbarType tabbarType) {
    final double selectedSize = 28;
    final double unSelectedSize = 16;

    switch (tabbarType) {
      case TabbarType.home:
        if (isSelected) {
          return Icon(
            Icons.home,
            size: selectedSize,
          );
        }

        return Icon(
          Icons.home,
          size: unSelectedSize,
        );

      case TabbarType.searching:
        if (isSelected) {
          return Icon(
            Icons.search,
            size: selectedSize,
          );
        }

        return Icon(
          Icons.search,
          size: unSelectedSize,
        );

      case TabbarType.settings:
        if (isSelected) {
          return Icon(
            Icons.settings,
            size: selectedSize,
          );
        }

        return Icon(
          Icons.settings,
          size: unSelectedSize,
        );
      case TabbarType.histories:
        if (isSelected) {
          return Icon(
            Icons.av_timer,
            size: selectedSize,
          );
        }

        return Icon(
          Icons.history,
          size: unSelectedSize,
        );
      default:
        return Container();
    }
  }

  void onTapTabbar(int tabbarIndex) {
    FocusScope.of(context).unfocus();
    setState(() {
      this.currIndex = tabbarIndex;
    });
  }
}

enum TabbarType { home, searching, histories, settings }
