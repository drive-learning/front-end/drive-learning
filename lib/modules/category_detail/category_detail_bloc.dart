import 'dart:async';

import 'package:flutter/material.dart';
import 'package:vn_drive_learning/common/base/bloc_base.dart';
import 'package:vn_drive_learning/services/repositories/categories_repository.dart';
import 'package:vn_drive_learning/services/entity/set_question.dart';

final setQuestionLoadLimit = 10;

class CategoryDetailBloc extends BlocBase {
  StreamController _controller = new StreamController();
  Stream get setQuestionsStream => _controller.stream;

  int _currentSelectedSetQuestionIndex;
  int get currentSelectedSetQuestionIndex => _currentSelectedSetQuestionIndex;

  bool validateLoadMore() {
    return _setQuestions.length % setQuestionLoadLimit == 0;
  }

  CategoriesRepository _categoriesRepository = CategoriesRepository();

  List<SetQuestion> _setQuestions = List();

  List<SetQuestion> get setQuestions => _setQuestions;

  fetchSetQuestions(int offset, int categoryId) async {
    final newSetQuestions =
        await _categoriesRepository.fetSetQuestions(offset, categoryId);
    if (newSetQuestions.isNotEmpty) {
      _setQuestions.addAll(newSetQuestions);
      _controller.sink.add(true);
    }
  }

  setCurretnSelectedSetQuestionIndex(int index, {VoidCallback onSuccess}) {
    _currentSelectedSetQuestionIndex = index;
    onSuccess();
    _controller.sink.add(true);
  }

  reload() {
    _controller.sink.add(true);
  }

  @override
  void dispose() {
    _controller.close();
  }
}
