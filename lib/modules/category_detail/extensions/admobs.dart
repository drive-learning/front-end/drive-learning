part of '../category_detail_view.dart';

extension InterstitialAdMobs on _CategoryDetailPage {
  void _initializeInterstitialAd() {
    _isInterstitialAdReady = false;

    _interstitialAd = InterstitialAd(
      adUnitId: AdManager.interstitialAdUnitId,
      listener: _onInterstitialAdEvent,
    );

    _loadInterstitialAd();
  }

  void _loadInterstitialAd() {
    _interstitialAd.load();
  }

  void _onInterstitialAdEvent(MobileAdEvent event) {
    debugPrint("🥩🥩🥩 Interstitial ad event ${event.toString()}}");
    switch (event) {
      case MobileAdEvent.loaded:
        _bloc.reload();
        _isInterstitialAdReady = true;
        break;
      case MobileAdEvent.failedToLoad:
        _isInterstitialAdReady = false;
        break;
      case MobileAdEvent.closed:
        _bloc.reload();
        _isInterstitialAdReady = false;
        _initializeInterstitialAd();
        _bannerAd.dispose();
        Navigator.push(context, MaterialPageRoute(
          builder: (context) {
            return QuestionsPreviewPage(
              presentationMode: QuestionGroupPresentationMode.testing,
              isFromNotification: false,
              primaryColor: ColorsContant.listCategoriesColors
                  .loop(_bloc.currentSelectedSetQuestionIndex),
              setQuestion:
                  _bloc.setQuestions[_bloc.currentSelectedSetQuestionIndex],
            );
          },
        ));
        break;
      default:
      // do nothing
    }
  }
}

extension BannerAdMobs on _CategoryDetailPage {
  void _initializeBannerAd() {
    // _bannerAd =
    //     BannerAd(adUnitId: AdManager.bannerAdUnitId, size: AdSize.fullBanner);
    // _loadBannerAd();
  }

  void _loadBannerAd() {
    _bannerAd
      ..load()
      ..show(anchorType: AnchorType.bottom);
  }
}
