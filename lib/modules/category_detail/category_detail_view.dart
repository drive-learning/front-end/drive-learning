import 'package:firebase_admob/firebase_admob.dart';
import 'package:flutter/material.dart';
import 'package:vn_drive_learning/common/constant/color/colors_constant.dart';
import 'package:vn_drive_learning/common/constant/images/images_constant.dart';
import 'package:vn_drive_learning/modules/category_detail/category_detail_bloc.dart';
import 'package:vn_drive_learning/modules/category_detail/components/set_question_component.dart';
import 'package:vn_drive_learning/modules/questions_preview/questions_preview_page.dart';
import 'package:vn_drive_learning/modules/questions_preview/strategies/question/question_group_presentation_mode.dart';
import 'package:vn_drive_learning/services/entity/category.dart';
import 'package:vn_drive_learning/utils/adds/ad_manager/ad_id_manager.dart';
import 'package:vn_drive_learning/extensions/loop_list.dart';

part 'extensions/admobs.dart';

class CategoryDetailView extends StatefulWidget {
  final Category category;
  final Color primaryColor;
  CategoryDetailView({this.category, this.primaryColor});
  @override
  State<StatefulWidget> createState() {
    return _CategoryDetailPage();
  }
}

class _CategoryDetailPage extends State<CategoryDetailView> {
  CategoryDetailBloc _bloc;
  var offset = 0;

  // BannerAd _bannerAd;
  bool _isInterstitialAdReady;
  InterstitialAd _interstitialAd;
  BannerAd _bannerAd;

  @override
  void initState() {
    _bloc = CategoryDetailBloc()..fetchSetQuestions(offset, widget.category.id);
    super.initState();

    _initializeInterstitialAd();
    // _initializeBannerAd();
  }

  @override
  void dispose() {
    _bloc.dispose();
    _interstitialAd.dispose();
    // _bannerAd?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        extendBodyBehindAppBar: true,
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.white),
          backgroundColor: widget.primaryColor,
          elevation: 0,
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () => Navigator.pop(context),
          ),
          title: Text(
            widget.category.title,
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
        ),
        body: NotificationListener(
          child: StreamBuilder(
            stream: _bloc.setQuestionsStream,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                var setQuestions = _bloc.setQuestions;
                return Stack(
                  children: [
                    ListView.builder(
                        itemCount: setQuestions.length + 1,
                        itemBuilder: (context, index) {
                          if (index == setQuestions.length) {
                            return SizedBox(
                              height: 64,
                            );
                          }
                          final questionNumber = index + 1;
                          return Padding(
                            padding: const EdgeInsets.only(
                                left: 20, top: 10, right: 20, bottom: 10),
                            child: SetQuestionComponent(
                              setQuestion: setQuestions[index],
                              backgroundColor:
                                  ColorsContant.listCategoriesColors[index %
                                      ColorsContant
                                          .listCategoriesColors.length],
                              title: "Đề số 0 $questionNumber ",
                              onTap: (setQuestion) {
                                _bloc.setCurretnSelectedSetQuestionIndex(index,
                                    onSuccess: () {
                                  if (_isInterstitialAdReady) {
                                    _interstitialAd.show();
                                  }
                                });
                              },
                              imageAsset: ImagesConstant.listSetQuestionLogo[
                                  index %
                                      ImagesConstant
                                          .listSetQuestionLogo.length],
                            ),
                          );
                        }),
                    Positioned(
                      bottom: 0,
                      left: 0,
                      right: 0,
                      child: Container(
                        height: 48,
                      ),
                      // child: AdmobBanner(
                      //     adUnitId: AdManager.bannerAdUnitId,
                      //     adSize: AdmobBannerSize.LEADERBOARD)
                    ),
                  ],
                );
              }

              return Container();
            },
          ),
          onNotification: (ScrollNotification notification) {
            if (notification.metrics.pixels >=
                    notification.metrics.maxScrollExtent * 0.7 &&
                _bloc.validateLoadMore()) {
              loadMore();
            }
            return false;
          },
        ));
  }

  loadMore() {
    offset += setQuestionLoadLimit;
    _bloc.fetchSetQuestions(offset, widget.category.id);
  }
}
