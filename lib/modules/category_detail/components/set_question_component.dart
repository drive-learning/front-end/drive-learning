import 'package:flutter/material.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import 'package:vn_drive_learning/common/constant/images/images_constant.dart';
import 'package:vn_drive_learning/services/entity/set_question.dart';

class SetQuestionComponent extends StatelessWidget {
  final SetQuestion setQuestion;
  final Color backgroundColor;
  final String title;
  final Function(SetQuestion) onTap;
  final String imageAsset;
  SetQuestionComponent(
      {Key key,
      this.setQuestion,
      this.backgroundColor,
      this.title,
      this.onTap,
      this.imageAsset})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        onTap(setQuestion);
      },
      child: Stack(
        children: [
          Column(
            children: [
              Ink(
                padding: const EdgeInsets.symmetric(horizontal: 16),
                height: 112,
                decoration: BoxDecoration(
                    color: backgroundColor,
                    borderRadius: BorderRadius.circular(16)),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Container(
                      padding: const EdgeInsets.all(16),
                      width: 80,
                      height: 80,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(8),
                          boxShadow: [
                            BoxShadow(
                                color: Colors.grey[400].withOpacity(0.25),
                                offset: Offset(0, 10),
                                blurRadius: 20)
                          ]),
                      child: FittedBox(
                        fit: BoxFit.fill,
                        child: Image.asset(
                          imageAsset,
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 16,
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          title.toUpperCase(),
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: 20),
                        ),
                        SizedBox(
                          height: 16,
                        ),
                        LinearPercentIndicator(
                          width: 140,
                          lineHeight: 12,
                          animation: true,
                          percent: 0.8,
                          clipLinearGradient: true,
                          backgroundColor: Colors.white,
                          progressColor: Colors.cyanAccent,
                        ),
                      ],
                    ),
                    Spacer(),
                    Container(
                      padding: const EdgeInsets.all(4),
                      width: 56,
                      height: 56,
                      decoration: BoxDecoration(
                        color: Colors.white24,
                        borderRadius: BorderRadius.circular(16),
                      ),
                      child: Icon(
                        Icons.check,
                        color: Colors.white,
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
          Positioned(
            top: 0,
            left: 0,
            right: 0,
            child: Image.asset(
              ImagesConstant.bannerbackground,
              fit: BoxFit.cover,
              height: 80,
            ),
          ),
        ],
      ),
    );
  }
}
