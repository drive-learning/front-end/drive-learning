import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:vn_drive_learning/common/constant/color/colors_constant.dart';
import 'package:vn_drive_learning/common/constant/images/images_constant.dart';
import 'package:vn_drive_learning/common/constant/animation/lotties_constant.dart';
import 'package:vn_drive_learning/modules/questions_preview/questions_preview_page.dart';
import 'package:vn_drive_learning/modules/questions_preview/strategies/question/question_group_presentation_mode.dart';
import 'package:vn_drive_learning/modules/set_question_list/set_question_list_bloc.dart';
import 'package:vn_drive_learning/services/entity/category.dart';
import 'package:vn_drive_learning/services/entity/set_question.dart';

final setQuestionLoadLimit = 10;

class SetQuestionListPage extends StatefulWidget {
  final double bannerHeight = 300;
  final Category category;
  final Color primaryColor;
  SetQuestionListPage({this.category, this.primaryColor});
  @override
  State<StatefulWidget> createState() {
    return _LearningCategoryDetailState();
  }
}

class _LearningCategoryDetailState extends State<SetQuestionListPage> {
  SetQuestionListBloc _bloc;
  var offset = 0;

  @override
  void initState() {
    _bloc = SetQuestionListBloc()
      ..fetchSetQuestions(offset, widget.category.id);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        extendBodyBehindAppBar: true,
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.white),
          backgroundColor: widget.primaryColor,
          elevation: 0,
          leading: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () {
                Navigator.pop(context);
              }),
          title: Text(
            widget.category.title,
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
        ),
        body: NotificationListener(
          child: StreamBuilder(
            stream: _bloc.setQuestionsStream,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                var setQuestions = _bloc.setQuestions;
                return Stack(
                  children: [
                    _buildBannerWidget(),
                    _buildListTopicWidget(setQuestions),
                  ],
                );
              }

              return Center(
                child: Lottie.asset(Lottiesconstant.loadingAnimation,
                    width: 128, height: 128),
              );
            },
          ),
          onNotification: (ScrollNotification notification) {
            if (notification.metrics.pixels >=
                    notification.metrics.maxScrollExtent * 0.7 &&
                _bloc.validateLoadMore()) {
              loadMore();
            }
            return false;
          },
        ));
  }

  Widget _buildListTopicWidget(List<SetQuestion> setQuestions) {
    return CustomScrollView(slivers: [
      SliverToBoxAdapter(
        child: Container(
          height: widget.bannerHeight - 128,
          color: Colors.transparent,
        ),
      ),
      SliverList(
        delegate: SliverChildBuilderDelegate(
          (BuildContext context, int index) {
            final backgroundColor = ColorsContant.listCategoriesColors[
                index % ColorsContant.listCategoriesColors.length];
            return LearningCategoryComponent(
              bannerImageName: ImagesConstant.listLearningBanners[
                  index % ImagesConstant.listLearningBanners.length],
              title: setQuestions[index].title,
              backgroundColor: backgroundColor,
              onTap: () {
                Navigator.push(context, MaterialPageRoute(
                  builder: (context) {
                    return QuestionsPreviewPage(
                      primaryColor: backgroundColor,
                      setQuestion: setQuestions[index],
                      presentationMode: QuestionGroupPresentationMode.learning,
                    );
                  },
                ));
              },
            );
          },
          childCount: 3,
        ),
      ),
    ]);
  }

  loadMore() {
    offset += setQuestionLoadLimit;
    _bloc.fetchSetQuestions(offset, widget.category.id);
  }

  Container _buildBannerWidget() {
    return Container(
      alignment: Alignment.topLeft,
      decoration: BoxDecoration(
          color: widget.primaryColor,
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.elliptical(widget.bannerHeight + 80, 100),
              bottomRight: Radius.elliptical(widget.bannerHeight + 80, 100))),
      padding: EdgeInsets.only(left: 20, right: 20, top: 100),
      height: widget.bannerHeight - 24,
      child:
          Lottie.asset(Lottiesconstant.bannerQuestions, fit: BoxFit.fitHeight),
    );
  }
}

class LearningCategoryComponent extends StatelessWidget {
  final Color backgroundColor;
  final String bannerImageName;
  final String title;
  final VoidCallback onTap;
  const LearningCategoryComponent(
      {Key key,
      this.bannerImageName,
      this.title,
      this.backgroundColor,
      this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 16, right: 16, top: 8, bottom: 8),
      child: Material(
        color: Colors.transparent,
        child: InkWell(
          onTap: () {
            onTap();
          },
          child: Container(
            height: 120,
            decoration: BoxDecoration(
                color: this.backgroundColor,
                border: Border.all(color: Colors.grey[800], width: 0.2),
                borderRadius: BorderRadius.circular(16),
                boxShadow: [
                  BoxShadow(
                      color: Colors.grey[800].withOpacity(0.5),
                      blurRadius: 5,
                      offset: Offset(5, 5))
                ]),
            child: Row(
              children: [
                Expanded(
                    flex: 1,
                    child: Container(
                      child: Image.asset(
                        bannerImageName,
                        width: 80,
                        height: 80,
                      ),
                    )

                    // color: Colors.red,
                    ),
                Expanded(
                  flex: 2,
                  child: Container(
                    padding: const EdgeInsets.all(16),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          title.toUpperCase(),
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 16,
                              fontWeight: FontWeight.w600),
                        ),
                        SizedBox(
                          height: 8,
                        ),
                        Text(
                          "Bộ đề gồm 100 câu hỏi tập trung vào các luật khi tham gia giao thông",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 10,
                              fontWeight: FontWeight.w600),
                        )
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
