import 'package:flutter/material.dart';
import 'package:vn_drive_learning/services/search_service/search_service_type.dart';

part 'main_view.dart';
part 'content_view.dart';
part 'header_view.dart';

class SearchItemComponent extends StatelessWidget {
  final SearchItem item;
  final List<Color> arrowColors = [
    Colors.red,
    Colors.yellow[800],
    Colors.green,
    Colors.purple[300]
  ];

  final List<Color> listSubBackgrounds = [
    Colors.red[100],
    Colors.yellow[200],
    Colors.green[100],
    Colors.purple[50]
  ];

  final int index;
  final VoidCallback onTap;

  SearchItemComponent(
      {@required this.item, @required this.index, @required this.onTap});
  @override
  Widget build(BuildContext context) {
    return _build(context);
  }
}
