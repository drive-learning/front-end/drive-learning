part of 'search_item.dart';

extension _Main on SearchItemComponent {
  Widget _build(BuildContext context) {
    return InkWell(
      onTap: () {
        this.onTap();
      },
      child: Container(
        height: 96,
        margin: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
        padding: EdgeInsets.all(8),
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(8),
            border: Border.all(
                width: 0.5, color: Colors.grey[600].withOpacity(0.25)),
            boxShadow: [
              BoxShadow(
                  offset: Offset(0, 1),
                  blurRadius: 2,
                  color: Colors.grey[600].withOpacity(0.25))
            ]),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
                alignment: Alignment.center,
                height: 20,
                child: _buildHeaderView()),
            SizedBox(
              height: 4,
            ),
            _buildContentView()
          ],
        ),
      ),
    );
  }
}
