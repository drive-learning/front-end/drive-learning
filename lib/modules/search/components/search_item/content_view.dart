part of 'search_item.dart';

extension _Content on SearchItemComponent {
  Widget _buildContentView() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
      child: Text(item.title,
          style: TextStyle(
              color: Colors.blueGrey,
              fontWeight: FontWeight.w300,
              fontSize: 14),
          maxLines: 2,
          overflow: TextOverflow.ellipsis),
    );
  }
}
