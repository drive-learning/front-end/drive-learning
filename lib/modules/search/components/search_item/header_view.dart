part of 'search_item.dart';

extension _Header on SearchItemComponent {
  Widget _buildHeaderView() {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Icon(
          Icons.arrow_right_sharp,
          color: arrowColors[index % arrowColors.length],
        ),
        SizedBox(
          width: 8,
        ),
        Text("Đề thi lái xe",
            style: TextStyle(
                color: Colors.blueGrey,
                fontWeight: FontWeight.w500,
                fontSize: 16)),
        SizedBox(
          width: 8,
        ),
        Container(
            padding: const EdgeInsets.symmetric(horizontal: 12),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(2.5),
                color: listSubBackgrounds[index % listSubBackgrounds.length]),
            height: 32,
            alignment: Alignment.center,
            child: Text(
              "Bộ đề ${item.question.setQuestionId + 1}".toUpperCase(),
              style: TextStyle(
                  color: arrowColors[index % arrowColors.length],
                  fontWeight: FontWeight.w600,
                  fontSize: 12),
            )),
        SizedBox(
          width: 8,
        ),
        Text(
          "To day",
          style: TextStyle(
              color: Colors.blueGrey,
              fontSize: 12,
              fontWeight: FontWeight.w400),
        ),
        SizedBox(
          width: 4,
        ),
        Icon(
          Icons.add_alert,
          color: arrowColors[index % arrowColors.length],
          size: 12,
        )
      ],
    );
  }
}
