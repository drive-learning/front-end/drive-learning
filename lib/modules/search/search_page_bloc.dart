import 'dart:async';

import 'package:vn_drive_learning/common/base/bloc_base.dart';
import 'package:vn_drive_learning/main.dart';
import 'package:vn_drive_learning/services/entity/bool_enum.dart';
import 'package:vn_drive_learning/services/search_service/search_service_type.dart';

class SearchPageBloc extends BlocBase {
  StreamController _searchController = StreamController();
  Stream get searchStream => _searchController.stream;

  SearchDataServiceType searchService = localtor<SearchDataServiceType>();

  List<SearchItem> _searchedItem = [];
  BoolEnum isLoading = BoolEnum(false);

  List<SearchItem> get searchedItems => _searchedItem;

  initialization() {
    _searchController.sink.add(true);
  }

  onSearching({String searchContent = ""}) {
    if (searchContent.isEmpty) {
      _searchedItem = [];
      isLoading.setValue(false);
      _searchController.sink.add(true);
      return;
    }
    isLoading.setValue(true);
    searchService.search(
      text: searchContent,
      callback: (searchItems) {
        _searchedItem = searchItems;
        isLoading.setValue(false);
        _searchController.sink.add(true);
      },
    );
    _searchController.sink.add(true);
    return;
  }

  @override
  void dispose() {
    _searchController.close();
  }
}
