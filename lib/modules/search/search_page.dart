import 'package:firebase_admob/firebase_admob.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';
import 'package:vn_drive_learning/common/constant/color/colors_constant.dart';
import 'package:vn_drive_learning/common/constant/images/images_constant.dart';
import 'package:vn_drive_learning/modules/search/components/search_item/search_item.dart';
import 'package:vn_drive_learning/modules/search/filter_page/filter_page.dart';
import 'package:vn_drive_learning/modules/search/item_detail_page/search_detail_page.dart';
import 'package:vn_drive_learning/modules/search/search_page_bloc.dart';
import 'package:vn_drive_learning/services/database/tables/database_provider/db_provider.dart';
import 'package:vn_drive_learning/utils/adds/ad_manager/ad_id_manager.dart';

part 'sections/empty_state.dart';
part 'sections/loading_state.dart';
part 'sections/search_component.dart';
part 'sections/appbar_extension.dart';
part 'sections/ui_config.dart';
part 'sections/search_results.dart';

part 'extensions/admobs.dart';

class SearchPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _SearchPageState();
  }
}

class _SearchPageState extends State<SearchPage> {
  _SearchPageUIConfig _config = _SearchPageUIConfig();
  SearchPageBloc _bloc;

  BannerAd _bannerAd;

  @override
  void initState() {
    _bloc = SearchPageBloc();
    super.initState();

    _initializeBannerAd();
  }

  @override
  void dispose() {
    _bloc.dispose();
    _bannerAd.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: ColorsContant.appBackgroundColor,
          elevation: 0,
          title: _buildAppBarTitle(),
          actions: [_buildFilterBarButton()],
        ),
        body: StreamBuilder(
          stream: _bloc.searchStream,
          builder: (context, snapshot) {
            return Container(
                color: Colors.white,
                child: Stack(
                  children: [
                    _buildSearchComponent(),
                    _buildSearchSectioTitle(),
                    _buildSearchResultsSection(),
                  ],
                ));
          },
        ));
  }
}
