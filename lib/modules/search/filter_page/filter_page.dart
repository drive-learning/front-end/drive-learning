import 'package:chips_choice/chips_choice.dart';
import 'package:flutter/material.dart';
import 'package:vn_drive_learning/modules/profile_steps/profile_steps_bloc.dart';
import 'package:vn_drive_learning/services/entity/user_info/license_type.dart';

class SearchFilterPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _SearchFilterState();
  }
}

enum SortedType {
  aToZ,
  zToA,
  normalToSpecific,
}

class _SearchFilterState extends State<SearchFilterPage> {
  final licenseOptions = LicenseType.values
      .map((e) => LicenseOptionViewModel(licenseType: e))
      .map((e) => FilteredModel(title: e.getDisplayedName()))
      .toList();

  final sotredOptions = SortedType.values
      .map<FilteredModel>((e) => FilteredModel(title: e.toString()))
      .toList();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          elevation: 0,
          leading: IconButton(
            icon: Icon(
              Icons.close,
              color: Colors.grey[800],
            ),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          backgroundColor: Colors.white,
        ),
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [],
          ),
        ));
  }
}

class FilteredModel {
  final String title;
  const FilteredModel({this.title});
}

class FilteredSectionComponent extends StatelessWidget {
  final String title;
  final List<FilteredModel> filteredModels;

  FilteredSectionComponent({this.title = "", @required this.filteredModels});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 24, vertical: 8),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            title.toUpperCase(),
            style: TextStyle(
                color: Colors.grey[800],
                fontSize: 16,
                fontWeight: FontWeight.w700),
          ),
          ChipsChoice.single(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            padding: const EdgeInsets.symmetric(horizontal: 0, vertical: 16),
            value: 0,
            onChanged: (value) {},
            choiceItems: C2Choice.listFrom<int, FilteredModel>(
              source: filteredModels,
              value: (index, item) => index,
              label: (index, item) => item.title,
            ),
            wrapped: true,
          )
        ],
      ),
    );
  }
}

/// Make some thing in here for make good component desig
