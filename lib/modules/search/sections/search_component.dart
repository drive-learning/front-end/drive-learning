part of '../search_page.dart';

extension _SearchSection on _SearchPageState {
  Widget _buildSearchComponent() {
    return Positioned(
      top: 0,
      left: 0,
      right: 0,
      height: _config.searchComponentHeight,
      child: Center(
          child: Container(
        alignment: Alignment.center,
        margin:
            EdgeInsets.symmetric(horizontal: _config.searchMarginHorizontal),
        padding:
            EdgeInsets.symmetric(horizontal: _config.searchPaddingHorizontal),
        height: 48,
        decoration: _config.searchDecoration,
        child: TextField(
          textAlign: TextAlign.left,
          decoration: InputDecoration(
              hintText: "Tìm kiếm ở đây bạn nhé <3",
              hintStyle: TextStyle(color: Colors.grey[800].withOpacity(0.3)),
              enabledBorder: InputBorder.none,
              focusedBorder: InputBorder.none,
              suffixIcon: Icon(
                Icons.search_rounded,
                color: Colors.grey[800].withOpacity(0.3),
                size: 32,
              )),
          onChanged: (value) {
            _bloc.onSearching(searchContent: value);
          },
          enableInteractiveSelection: false,
        ),
      )),
    );
  }
}
