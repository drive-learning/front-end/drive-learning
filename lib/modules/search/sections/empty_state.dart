part of '../search_page.dart';

extension _EmptyState on _SearchPageState {
  Widget _buildEmptyState() {
    final Size size = MediaQuery.of(context).size;
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          child: Center(
            child: Container(
              width: size.width * 0.6,
              height: size.width * 0.6,
              child: Image.asset(
                ImagesConstant.bgEmptyState,
                fit: BoxFit.fill,
              ),
            ),
          ),
        ),
        Container(
            margin: const EdgeInsets.symmetric(horizontal: 56, vertical: 16),
            child: Text(
              "Không tìm thấy kết  nào phù hợp, thử tìm kiếm lại bạn nhé",
              style: TextStyle(
                color: Colors.grey[800].withOpacity(0.6),
                fontSize: 16,
                wordSpacing: 2,
              ),
              textAlign: TextAlign.center,
            ))
      ],
    );
  }
}
