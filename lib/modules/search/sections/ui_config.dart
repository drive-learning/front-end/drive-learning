part of '../search_page.dart';

class _SearchPageUIConfig {
  final TextStyle appBarTitleStyle = TextStyle(
      fontSize: 24, fontWeight: FontWeight.bold, color: Colors.grey[800]);
  final double searchComponentHeight = 96;
  final double searchMarginHorizontal = 16;
  final double searchMarginVertical = 16;
  final double searchPaddingHorizontal = 16;
  final BoxDecoration searchDecoration = BoxDecoration(
      color: Colors.white,
      borderRadius: BorderRadius.circular(16),
      border: Border.all(color: Colors.grey[400], width: 0.25),
      boxShadow: [
        BoxShadow(
            offset: Offset(0, 4),
            blurRadius: 16,
            color: Colors.grey[800].withOpacity(0.23))
      ]);
}
