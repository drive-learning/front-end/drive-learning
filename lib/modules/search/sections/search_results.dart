part of '../search_page.dart';

extension _SearchResultsSection on _SearchPageState {
  Widget _buildSearchSectioTitle() {
    String title = "No results";
    if (_bloc.searchedItems.isNotEmpty) {
      title = "${_bloc.searchedItems.length} " + "Results";
    }
    return Positioned(
      top: _config.searchComponentHeight,
      left: 0,
      right: 0,
      height: 24,
      child: Container(
          padding:
              EdgeInsets.symmetric(horizontal: _config.searchMarginHorizontal),
          alignment: Alignment.centerLeft,
          child: Text(
            title.toUpperCase(),
            style: TextStyle(
                color: Colors.grey[800],
                fontSize: 14,
                fontWeight: FontWeight.w500),
            textAlign: TextAlign.left,
          )),
    );
  }

  Widget _buildSearchResultsSection() {
    if (_bloc.isLoading.isOn) {
      return _buildLoadingView();
    }

    if (_bloc.searchedItems.isNotEmpty) {
      return Positioned(
        top: _config.searchComponentHeight + 32,
        left: 0,
        right: 0,
        bottom: 0,
        child: Container(
          child: ListView.builder(
            itemCount: _bloc.searchedItems.length,
            itemBuilder: (context, index) {
              final item = _bloc.searchedItems[index];
              return SearchItemComponent(
                item: item,
                index: index,
                onTap: () async {
                  item.question.answers =
                      await DBProvider.db.fetchAnswers(item.question.id);
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => SearchItemDetailPage(
                          item: item,
                        ),
                      ));
                },
              );
            },
          ),
        ),
      );
    }

    return Positioned(
        top: _config.searchComponentHeight + 32,
        left: 0,
        right: 0,
        bottom: 0,
        child: SingleChildScrollView(child: _buildEmptyState()));
  }
}
