part of '../search_page.dart';

extension _AppBarExtension on _SearchPageState {
  Widget _buildAppBarTitle() {
    return Text(
      "Tìm kiếm",
      style: _config.appBarTitleStyle,
      textAlign: TextAlign.left,
    );
  }

  Widget _buildFilterBarButton() {
    return IconButton(
      icon: Icon(Icons.filter_list, color: Colors.grey[800]),
      onPressed: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) {
                  return SearchFilterPage();
                },
                fullscreenDialog: true));
      },
    );
  }
}
