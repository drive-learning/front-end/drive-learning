import 'package:firebase_admob/firebase_admob.dart';
import 'package:flutter/material.dart';
import 'package:vn_drive_learning/modules/search/item_detail_page/components/search_item_answer.dart';
import 'package:vn_drive_learning/modules/search/item_detail_page/search_detail_bloc.dart';
import 'package:vn_drive_learning/services/entity/question.dart';
import 'package:vn_drive_learning/services/search_service/search_service_type.dart';
import 'package:vn_drive_learning/utils/adds/ad_manager/ad_id_manager.dart';

part 'sections/answers_view.dart';
part 'sections/bottom_view.dart';
part 'sections/question_des_view.dart';

part 'extension/admobs.dart';

class SearchItemDetailPage extends StatefulWidget {
  final SearchItem item;

  SearchItemDetailPage({@required this.item});
  @override
  State<StatefulWidget> createState() {
    return _SearchItemDetailState();
  }
}

class _SearchItemDetailState extends State<SearchItemDetailPage> {
  SearchItemDetailBloc _bloc;
  BannerAd _bannerAd;

  @override
  void initState() {
    _bloc = SearchItemDetailBloc(question: widget.item.question)
      ..initialization();
    super.initState();

    // _initializeBannerAd();
  }

  @override
  void dispose() {
    _bloc.dispose();
    // _bannerAd?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 0,
          leading: IconButton(
              icon: Icon(Icons.arrow_back_rounded),
              color: Colors.blueGrey,
              onPressed: () => Navigator.pop(context))),
      body: _buildBodyView(),
    );
  }
}

extension _Body on _SearchItemDetailState {
  Widget _buildBodyView() {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 24, vertical: 16),
      child: CustomScrollView(
        slivers: [
          SliverToBoxAdapter(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                /**
                 * Header Description
                 */
                _buildQuestionDesView(_bloc.question),
                SizedBox(
                  height: 24,
                )
              ],
            ),
          ),
          _buildAnswersView(),
          SliverToBoxAdapter(
            child: StreamBuilder(
              stream: _bloc.viewResultStream,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return Column(
                    children: [
                      Visibility(
                        child: _buildExplainButton(),
                        visible: false,
                      ),
                      Visibility(
                        visible: _bloc.isViewExplaitation,
                        child: Container(
                          margin: const EdgeInsets.symmetric(vertical: 8),
                          padding: const EdgeInsets.all(16),
                          decoration: BoxDecoration(
                              color: Colors.green[200],
                              borderRadius: BorderRadius.circular(8),
                              border: Border.all(
                                  color: Colors.blueGrey.withOpacity(0.5),
                                  width: 0.25),
                              boxShadow: [
                                BoxShadow(
                                    offset: Offset(0, 1),
                                    blurRadius: 1,
                                    color: Colors.blueGrey.withOpacity(0.25))
                              ]),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                '''
<Google> Cannot find an ad network adapter with the name(s): com.google.DummyAdapter.
<Google> Cannot find an ad network adapter with the name(s): com.google.DummyAdapter.
          ''',
                                textAlign: TextAlign.left,
                                style: TextStyle(color: Colors.white),
                              ),
                            ],
                          ),
                        ),
                      )
                    ],
                  );
                }

                return Container();
              },
            ),
          ),
        ],
      ),
    );
  }
}
