import 'dart:async';

import 'package:flutter/material.dart';
import 'package:vn_drive_learning/common/base/bloc_base.dart';
import 'package:vn_drive_learning/services/entity/question.dart';

class SearchItemDetailBloc extends BlocBase {
  StreamController _controller = StreamController();
  Stream get stream => _controller.stream;

  StreamController _viewResultController = StreamController();
  Stream get viewResultStream => _viewResultController.stream;

  final Question question;
  SearchItemDetailBloc({@required this.question});

  bool isViewExplaitation = false;

  bool get isShowResult => question.currentSelectedAnswerIndex != -1;

  initialization() {
    _controller.sink.add(true);
    _viewResultController.sink.add(true);
  }

  onTapAnsers({@required index}) {
    question.currentSelectedAnswerIndex = index;
    _controller.sink.add(true);
  }

  onViewExplaintaioin() {
    isViewExplaitation = true;
    _viewResultController.sink.add(true);
  }

  @override
  void dispose() {
    _controller.close();
    _viewResultController.close();
  }
}
