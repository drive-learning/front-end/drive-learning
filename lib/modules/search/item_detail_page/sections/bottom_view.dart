part of '../search_detail_page.dart';

extension _ExplainButton on _SearchItemDetailState {
  Widget _buildExplainButton() {
    return InkWell(
      onTap: () {
        _bloc.onViewExplaintaioin();
      },
      child: Stack(
        children: [
          Container(
            margin: const EdgeInsets.only(top: 24, bottom: 24),
            height: 48,
            decoration: BoxDecoration(
                color: Colors.red[400], borderRadius: BorderRadius.circular(8)),
            child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text("Xem giải thích".toUpperCase(),
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 16,
                          fontWeight: FontWeight.w600))
                ]),
          ),
        ],
      ),
    );
  }
}
