part of '../search_detail_page.dart';

extension _AnswersView on _SearchItemDetailState {
  Widget _buildAnswersView() {
    return StreamBuilder(
      stream: _bloc.stream,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return SliverList(
            delegate: SliverChildBuilderDelegate(
              (BuildContext context, int index) {
                return SearchItemAnswerComponent(
                  answer: _bloc.question.answers[index],
                  isViewResult: _bloc.isShowResult,
                  isSelected:
                      _bloc.question.currentSelectedAnswerIndex == index,
                  onTap: () {
                    if (!_bloc.isShowResult) {
                      _bloc.onTapAnsers(index: index);
                    }
                  },
                );
              },
              childCount: _bloc.question.answers.length,
            ),
          );
        }

        return SliverToBoxAdapter();
      },
    );
  }
}
