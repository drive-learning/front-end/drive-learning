part of '../search_detail_page.dart';

extension _QuestionDesView on _SearchItemDetailState {
  Widget _buildQuestionDesView(Question item) {
    return Container(
      padding: const EdgeInsets.all(16),
      decoration: BoxDecoration(
          color: Colors.white,
          border: Border.all(color: Colors.blueGrey, width: 0.25),
          borderRadius: BorderRadius.circular(8),
          boxShadow: [
            BoxShadow(
                offset: Offset(0, 2),
                blurRadius: 4,
                color: Colors.blueGrey.withOpacity(0.5))
          ]),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Icon(
            Icons.arrow_right_sharp,
            color: Colors.green,
          ),
          SizedBox(
            width: 4,
          ),
          Expanded(
            child: Column(
              children: [
                _buildQuestionDesHeaderView(item),
                SizedBox(
                  height: 12,
                ),
                _buildQuestionDesContentView(item),
              ],
            ),
          )
        ],
      ),

      ///
    );
  }
}

Text _buildQuestionDesContentView(Question item) {
  return Text(item.description,
      style: TextStyle(
          color: Colors.blueGrey,
          fontSize: 18,
          wordSpacing: 2.5,
          fontWeight: FontWeight.w400));
}

Container _buildQuestionDesHeaderView(Question item) {
  return Container(
    color: Colors.white,
    child: Row(
      children: [
        Container(
          alignment: Alignment.center,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(4),
            color: Colors.green[100],
          ),
          padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 6),
          child: Text(
            "Dề thi lái xe",
            style: TextStyle(
                color: Colors.green[600],
                fontWeight: FontWeight.bold,
                fontSize: 16),
            textAlign: TextAlign.center,
          ),
        ),
        SizedBox(
          width: 4,
        ),
        Container(
          alignment: Alignment.center,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(4),
            color: Colors.red[100],
          ),
          padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 6),
          child: Text(
            "Dề số ${item.setQuestionId + 1}",
            style: TextStyle(
                color: Colors.red, fontWeight: FontWeight.bold, fontSize: 16),
            textAlign: TextAlign.center,
          ),
        )
      ],
    ),
  );
}
