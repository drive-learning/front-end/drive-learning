part of '../search_detail_page.dart';

extension BannerAdMobs on _SearchItemDetailState {
  void _initializeBannerAd() {
    // _bannerAd =
    //     BannerAd(adUnitId: AdManager.bannerAdUnitId, size: AdSize.fullBanner);
    // _loadBannerAd();
  }

  void _loadBannerAd() {
    _bannerAd
      ..load()
      ..show(anchorType: AnchorType.bottom);
  }
}
