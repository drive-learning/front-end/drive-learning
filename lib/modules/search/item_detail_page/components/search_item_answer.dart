import 'package:flutter/material.dart';
import 'package:vn_drive_learning/common/constant/color/colors_constant.dart';
import 'package:vn_drive_learning/services/entity/answer.dart';

class SearchItemAnswerComponent extends StatelessWidget {
  final Answer answer;
  final bool isViewResult;
  final bool isSelected;
  final VoidCallback onTap;
  const SearchItemAnswerComponent({
    @required this.answer,
    this.onTap,
    this.isViewResult,
    this.isSelected,
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        onTap();
      },
      child: Container(
        margin: const EdgeInsets.symmetric(vertical: 8),
        padding: const EdgeInsets.all(16),
        decoration: _decoration,
        child: Row(
          children: [
            Icon(Icons.check_circle_outlined, color: _textColor, size: 24),
            SizedBox(
              width: 8,
            ),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    answer.content,
                    style: TextStyle(color: _textColor, fontSize: 16),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

extension _Config on SearchItemAnswerComponent {
  Color get _backgroundColor {
    if (isViewResult) {
      if (isSelected) {
        if (answer.isCorrect) {
          return ColorsContant.correctSelectedAnswerColor;
        }

        return ColorsContant.wrongSelectedAnswerColor;
      }

      if (answer.isCorrect) {
        return ColorsContant.correctSelectedAnswerColor;
      }
    }

    return Colors.white;
  }

  Color get _textColor {
    if (isViewResult && (answer.isCorrect || isSelected)) {
      return Colors.white;
    }

    return Colors.blueGrey;
  }

  BoxDecoration get _decoration => BoxDecoration(
          color: this._backgroundColor,
          borderRadius: BorderRadius.circular(8),
          border:
              Border.all(color: Colors.blueGrey.withOpacity(0.5), width: 0.25),
          boxShadow: [
            BoxShadow(
                offset: Offset(0, 1),
                blurRadius: 1,
                color: Colors.blueGrey.withOpacity(0.25))
          ]);
}
