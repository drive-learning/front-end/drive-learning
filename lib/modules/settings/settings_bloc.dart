import 'dart:async';

import 'package:vn_drive_learning/common/base/bloc_base.dart';

class SettingsBloc extends BlocBase {
  StreamController _controller = StreamController();
  Stream get stream => _controller.stream;

  initialization() {
    _controller.sink.add(true);
  }

  @override
  void dispose() {
    _controller.close();
  }

  onUpdateUserInfo() {
    _controller.sink.add(true);
  }
}
