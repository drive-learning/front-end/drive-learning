import 'package:flutter/material.dart';

class DescriptionSettingComponent extends StatelessWidget {
  final String title;
  final String description;
  final VoidCallback onTap;
  DescriptionSettingComponent({
    this.title = "",
    this.description = "",
    this.onTap,
    Key key,
  }) : super(key: key);

  final decoration = BoxDecoration(
      color: Colors.white,
      borderRadius: BorderRadius.circular(16),
      border: Border.all(color: Colors.grey[400].withOpacity(0.7), width: 0.1),
      boxShadow: [
        BoxShadow(
            color: Colors.grey[400].withOpacity(0.75),
            offset: Offset(0, 1),
            blurRadius: 1)
      ]);

  final subTitleStyle = TextStyle(
      fontSize: 14, fontWeight: FontWeight.w700, color: Colors.grey[600]);
  @override
  Widget build(BuildContext context) {
    return SliverToBoxAdapter(
        child: InkWell(
      onTap: () => onTap(),
      child: Container(
          decoration: decoration,
          padding: const EdgeInsets.all(24),
          margin: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
          child: Row(children: [
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Visibility(
                    visible: title.isNotEmpty,
                    child: Text(
                      title.toUpperCase(),
                      style: subTitleStyle,
                      textAlign: TextAlign.left,
                    ),
                  ),
                  Visibility(
                    visible: description.isNotEmpty,
                    child: Text(
                      description,
                      style: TextStyle(
                          color: Colors.grey[400],
                          fontSize: 12.5,
                          fontWeight: FontWeight.w600),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(width: 16),
            Icon(
              Icons.arrow_forward_ios,
              color: Colors.grey[600],
            ),
          ])),
    ));
  }
}
