import 'package:custom_switch/custom_switch.dart';
import 'package:flutter/material.dart';

enum SettingComponentType { bottomSheet, switchSelection }

class SettingComponent extends StatelessWidget {
  final String title;
  final Color primaryColor;
  final IconData iconData;
  final bool isOn;
  final SettingComponentType type;
  final Function(bool) onChangeValue;

  // final D
  const SettingComponent({
    @required this.title,
    @required this.primaryColor,
    @required this.iconData,
    @required this.isOn,
    this.type = SettingComponentType.switchSelection,
    @required this.onChangeValue,
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: [
          Icon(
            iconData,
            color: primaryColor,
            size: 32,
          ),
          SizedBox(
            width: 8,
          ),
          Text(
            title.toUpperCase(),
            style: TextStyle(
                color: Colors.grey[400],
                fontSize: 10,
                fontWeight: FontWeight.w600),
          ),
          Spacer(),
          Visibility(
            visible: type == SettingComponentType.switchSelection,
            child: Container(
              height: 28,
              child: CustomSwitch(
                activeColor: primaryColor,
                value: isOn,
                onChanged: (value) {
                  this.onChangeValue(value);
                },
              ),
            ),
          ),
          Visibility(
            visible: type == SettingComponentType.bottomSheet,
            child: InkWell(
              onTap: () {
                print("123456");
              },
              child: Container(
                height: 28,
                child: Row(
                  children: [
                    Text("Tiếng việt",
                        style: TextStyle(
                          color: Colors.grey[400],
                          fontSize: 14,
                          fontWeight: FontWeight.w400,
                        )),
                    SizedBox(
                      width: 8,
                    ),
                    Icon(
                      Icons.arrow_drop_down_sharp,
                      color: Colors.grey[400],
                    )
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
