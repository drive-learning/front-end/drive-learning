part of '../settings_page_view.dart';

extension _ProfileSection on _SettingsPageState {
  SliverToBoxAdapter _buildProfileSection(
      double avatarHeight, TextStyle mainTitleStyle) {
    return SliverToBoxAdapter(
        child: Container(
      height: avatarHeight,
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: Row(
        children: [
          Container(
            width: avatarHeight,
            decoration: BoxDecoration(
                color: Colors.yellow[800],
                borderRadius: BorderRadius.circular(avatarHeight / 2)),
            child: Lottie.asset(UserInfoSingleton.shared.avatarLottieAsset,
                fit: BoxFit.fill),
          ),
          SizedBox(
            width: 16,
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  Text(
                    UserInfoSingleton.shared.userName,
                    style: mainTitleStyle,
                    overflow: TextOverflow.ellipsis,
                    softWrap: true,
                  ),
                  SizedBox(
                    width: 4,
                  ),
                  IconButton(
                    icon: Icon(Icons.edit_outlined,
                        size: 20, color: Colors.grey[800]),
                    onPressed: () {
                      DSAlert.shared.showUserInfoEditForm(context,
                          DSUserInfoEditAlertConfig(
                        onUpdateData: () {
                          _bloc.onUpdateUserInfo();
                        },
                      ));
                    },
                  )
                ],
              ),
              Text(
                UserInfoSingleton.shared.getLicenseDisplayedName(),
                style: TextStyle(
                    fontSize: 14,
                    color: Colors.grey[400],
                    fontWeight: FontWeight.w500),
              ),
            ],
          ),
          SizedBox(
            height: 24,
          )
        ],
      ),
    ));
  }
}
