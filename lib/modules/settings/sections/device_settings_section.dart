part of '../settings_page_view.dart';

extension _DeviceSettingsSection on _SettingsPageState {
  Widget _buildDeviceSettingsSection() {
    final decoration = BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(16),
        border:
            Border.all(color: Colors.grey[400].withOpacity(0.7), width: 0.1),
        boxShadow: [
          BoxShadow(
              color: Colors.grey[400].withOpacity(0.75),
              offset: Offset(0, 1),
              blurRadius: 1)
        ]);

    final subTitleStyle = TextStyle(
        fontSize: 14, fontWeight: FontWeight.w700, color: Colors.grey[600]);
    return _buildListSettingsComponent(decoration, subTitleStyle);
  }

  SliverToBoxAdapter _buildListSettingsComponent(
      BoxDecoration decoration, TextStyle subTitleStyle) {
    return SliverToBoxAdapter(
      child: Container(
        decoration: decoration,
        padding: const EdgeInsets.all(24),
        margin: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
        child: Column(
          children: [
            Row(
              children: [
                Text(
                  "Cài đặt".toUpperCase(),
                  style: subTitleStyle,
                )
              ],
            ),
            SizedBox(
              height: 16,
            ),
            SettingComponent(
              iconData: Icons.surround_sound,
              isOn: storageService.settingsData.volumeStatus.value,
              title: "Âm lượng",
              primaryColor: Colors.yellow[800],
              onChangeValue: (value) {
                var settingsData = storageService.settingsData;
                settingsData.volumeStatus.switchStatus;
                storageService.updateSettings(settings: settingsData);
              },
            ),
            SizedBox(
              height: 16,
            ),
            SettingComponent(
              iconData: Icons.swap_calls,
              isOn: storageService.settingsData.repeatQuestionStatus.isOn,
              title: "Lặp lại câu hỏi",
              primaryColor: Colors.pink,
              onChangeValue: (value) {
                var settingsData = storageService.settingsData;
                settingsData.repeatQuestionStatus.switchStatus;
                storageService.updateSettings(settings: settingsData);
              },
            ),
            SizedBox(
              height: 16,
            ),
            SettingComponent(
              iconData: Icons.language,
              isOn: true,
              title: "Ngôn ngữ",
              primaryColor: Colors.teal,
              type: SettingComponentType.bottomSheet,
              onChangeValue: (value) {},
            ),
            SizedBox(
              height: 16,
            ),
            SettingComponent(
              iconData: Icons.phonelink_ring,
              isOn: storageService.settingsData.notificationStatus.isOn,
              title: "Thông báo",
              primaryColor: Colors.blue,
              onChangeValue: (value) {
                var settingsData = storageService.settingsData;
                settingsData.notificationStatus.switchStatus;
                storageService.updateSettings(settings: settingsData);
              },
            ),
          ],
        ),
      ),
    );
  }
}
