part of '../settings_page_view.dart';

extension _DonationSettingsSection on _SettingsPageState {
  Widget _buildDonationSettingsSectoin() {
    return DescriptionSettingComponent(
      title: "Ủng hộ bằng xem quảng cáo",
      description:
          "Bấm vào để xem quảng cáo, nhận gem và ủng hộ team bạn yêu nhé <3",
      onTap: () {},
    );
  }
}
