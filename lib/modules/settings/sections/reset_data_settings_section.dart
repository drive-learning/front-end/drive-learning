part of '../settings_page_view.dart';

extension _ResetDataSettingSection on _SettingsPageState {
  Widget _buildResetDataSettingSection() {
    return DescriptionSettingComponent(
      title:
          AppLocalizations.of(context).translate(AppLocalizationKeys.settings),
      description: AppLocalizations.of(context)
          .translate(AppLocalizationKeys.resetDescription),
      onTap: () {},
    );
  }
}
