import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:vn_drive_learning/common/constant/locale/app_localization_keys.dart';
import 'package:vn_drive_learning/design_system/ds_alert/alerts/user_info_edit_alert/config/user_info_edit_alert_config.dart';
import 'package:vn_drive_learning/design_system/ds_alert/ds_alert.dart';
import 'package:vn_drive_learning/locale/applocalizations.dart';
import 'package:vn_drive_learning/main.dart';
import 'package:vn_drive_learning/modules/purchase/purchase_page.dart';
import 'package:vn_drive_learning/modules/settings/components/des_setting_component.dart';
import 'package:vn_drive_learning/modules/settings/components/setting_component.dart';
import 'package:vn_drive_learning/modules/settings/settings_bloc.dart';
import 'package:vn_drive_learning/services/entity/user_info/user_info.dart';
import 'package:vn_drive_learning/services/storage/local_storage_service.dart';

part 'sections/profile_section.dart';
part 'sections/device_settings_section.dart';
part 'sections/update_tier_section.dart';
part 'sections/reset_data_settings_section.dart';
part 'sections/donation_settings_section.dart';
part 'sections/loading_view.dart';

class SettingsPageView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _SettingsPageState();
  }
}

class _SettingsPageState extends State<SettingsPageView> {
  SettingsBloc _bloc;

  var storageService = localtor<LocalStorageServiceType>();

  @override
  void initState() {
    _bloc = SettingsBloc()..initialization();
    super.initState();
  }

  @override
  void dispose() {
    _bloc.dispose();
    super.dispose();
  }

  @override
  Widget build(Object context) {
    final double avatarHeight = 76;
    final mainTitleStyle = TextStyle(
        fontSize: 20, fontWeight: FontWeight.w600, color: Colors.grey[800]);
    return Scaffold(
        body: StreamBuilder(
            stream: _bloc.stream,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return Container(
                  color: Colors.grey[300].withOpacity(0.3),
                  child: CustomScrollView(
                    slivers: [
                      SliverToBoxAdapter(
                        child: Container(
                          height: 56,
                        ),
                      ),
                      _buildProfileSection(avatarHeight, mainTitleStyle),
                      // _buildUpgradeTierSection(),
                      SliverToBoxAdapter(
                        child: Container(
                          height: 16,
                        ),
                      ),
                      // DescriptionSettingComponent(
                      //   title: AppLocalizations.of(context)
                      //       .translate(AppLocalizationKeys.upgrade),
                      //   description:
                      //       'Nâng cấp gói cước để loại bỏ tất cả quảng cáo cũng là 1 lựa chọn hay nè bạn',
                      //   onTap: () {
                      //     Navigator.push(
                      //         context,
                      //         MaterialPageRoute(
                      //           builder: (context) => PurchasePage(),
                      //           fullscreenDialog: true,
                      //         ));
                      //   },
                      // ),
                      _buildDeviceSettingsSection(),
                      _buildResetDataSettingSection(),
                      _buildDonationSettingsSectoin()
                    ],
                  ),
                );
              }

              return _buildLoadingView();
            }));
  }
}
