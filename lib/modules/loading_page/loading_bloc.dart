import 'dart:async';

import 'package:vn_drive_learning/common/base/bloc_base.dart';
import 'package:vn_drive_learning/services/database/tables/database_provider/db_provider.dart';
import 'package:vn_drive_learning/services/entity/set_question.dart';
import 'package:vn_drive_learning/services/entity/user_info/user_info.dart';

class QuickTestLoadingBloc extends BlocBase {
  StreamController _controller = StreamController();
  Stream get stream => _controller.stream;

  SetQuestion setQuestion;

  initialization() async {
    final categoryId = UserInfoSingleton.shared.licenseType.index + 1;
    setQuestion = (await DBProvider.db.fetSetQuestions(0, 99, categoryId))[4];
    Future.delayed(Duration(seconds: 2), () {
      _controller.sink.add(true);
    });
  }

  @override
  void dispose() {
    _controller.close();
  }
}
