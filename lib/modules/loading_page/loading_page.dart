import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/widgets.dart';
import 'package:lottie/lottie.dart';
import 'package:vn_drive_learning/common/constant/animation/lotties_constant.dart';
import 'package:vn_drive_learning/modules/loading_page/loading_bloc.dart';
import 'package:vn_drive_learning/modules/questions_preview/questions_preview_page.dart';
import 'package:vn_drive_learning/modules/questions_preview/strategies/question/question_group_presentation_mode.dart';

class QuickTestLoadingPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _LoadingPageState();
  }
}

class _LoadingPageState extends State<QuickTestLoadingPage> {
  QuickTestLoadingBloc _bloc;

  @override
  void initState() {
    _bloc = QuickTestLoadingBloc()..initialization();
    super.initState();
  }

  @override
  void dispose() {
    _bloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: StreamBuilder(
      stream: _bloc.stream,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          SchedulerBinding.instance.addPostFrameCallback((_) {
            Navigator.push(
                context,
                MaterialPageRoute(
                  fullscreenDialog: true,
                  builder: (context) {
                    return QuestionsPreviewPage(
                      presentationMode: QuestionGroupPresentationMode.learning,
                      primaryColor: Colors.red,
                      setQuestion: _bloc.setQuestion,
                      isFromNotification: true,
                    );
                  },
                ));
          });
        }

        return Column(
          children: [
            Flexible(
              flex: 5,
              child: Stack(
                children: [
                  Positioned(
                    left: 0,
                    right: 0,
                    bottom: 0,
                    height: 240,
                    child: Center(
                      child: Container(
                        child: Lottie.asset(Lottiesconstant.preLoadingBanner,
                            fit: BoxFit.fill),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Flexible(
              flex: 3,
              child: Stack(
                children: [
                  Positioned(
                    top: 0,
                    left: 0,
                    right: 0,
                    height: 200,
                    child: Center(
                      child: Container(
                        child: Lottie.asset(
                            Lottiesconstant.preLoadingProgressBar,
                            fit: BoxFit.fitWidth),
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        );
      },
    ));
  }
}
