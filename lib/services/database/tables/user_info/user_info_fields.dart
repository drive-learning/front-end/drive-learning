class UserInfoTableFields {
  UserInfoTableFields();

  String id = "id";
  String fullName = "full_name";
  String userName = "userName";
  String gender = "gender";
  String accountTier = "account_tier";
  String totalGem = "total_gem";
  String licenseType = "license_type";
}
