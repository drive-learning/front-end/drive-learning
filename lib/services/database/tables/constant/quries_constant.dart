import 'package:vn_drive_learning/services/database/tables/constant/tables_name_constant.dart';
import 'package:vn_drive_learning/services/database/tables/infos/tables_info.dart';

class QueriesConstant {
  static final String createUserInfoTable = ''' 
    Create table if not exists ${TableNamesConstant.userInfo} (
      ${TablesInfo.userInfo.id} integer primary key autoincrement,
      ${TablesInfo.userInfo.fullName} text,
      ${TablesInfo.userInfo.userName} text,
      ${TablesInfo.userInfo.licenseType} integer,
      ${TablesInfo.userInfo.gender} integer,
      ${TablesInfo.userInfo.totalGem} integer,
      ${TablesInfo.userInfo.accountTier} integer
    )
  ''';

  static final String getCategories = "";

  static final String getAllQuestions = '''
    select * from ${TableNamesConstant.questions}
  ''';
}
