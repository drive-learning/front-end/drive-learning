class TableNamesConstant {
  static final String userInfo = "user_info";
  static final String histories = "histories";
  static final String answers = "answers";
  static final String setQuestions = "set_questions";
  static final String questions = "questions";
  static final String categories = "categories";
}
