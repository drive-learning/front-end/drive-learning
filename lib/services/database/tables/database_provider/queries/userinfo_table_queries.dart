part of '../db_provider.dart';

extension UserInfoTable on DBProvider {
  createUserInfo() async {
    final db = await database;
    final resultMappings = await db.query(TableNamesConstant.userInfo);
    if (resultMappings.isEmpty) {
      await db.insert(
          TableNamesConstant.userInfo, UserInfoSingleton.shared.toMap(),
          conflictAlgorithm: ConflictAlgorithm.replace);
    }
  }

  Future<int> updateUserGem(int totalGem) async {
    final db = await database;
    return db.update(
        TableNamesConstant.userInfo, {TablesInfo.userInfo.totalGem: totalGem},
        where: "${TablesInfo.userInfo.id} = ?",
        whereArgs: [UserInfoSingleton.shared.id]);
  }

  fetchUserInfo() async {
    final db = await database;
    final resultMappings = await db.query(TableNamesConstant.userInfo);
    if (resultMappings.isNotEmpty) {
      UserInfoSingleton.shared.fromMap(resultMappings.first);
    }
  }
}
