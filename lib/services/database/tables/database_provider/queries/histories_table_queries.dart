part of '../db_provider.dart';

extension HistoriesTable on DBProvider {
  fetchHistories(int offset, int limit) async {
    final db = await database;
    final resultMappings = await db.query(TableNamesConstant.histories,
        limit: limit, offset: offset, orderBy: 'test_time');

    if (resultMappings.isNotEmpty) {
      return resultMappings.map<History>((e) => History.fromMap(e)).toList();
    }

    return List.empty();
  }

  createHistory(History history) async {
    final db = await database;
    await db.insert(TableNamesConstant.histories, history.toMap(),
        conflictAlgorithm: ConflictAlgorithm.replace);
  }

  Future<List<History>> fetchTopTrendingHistories({int numberItem}) async {
    final db = await database;
    return await db
        .query(TableNamesConstant.histories,
            groupBy: TablesInfo.histories.setQuestionId,
            orderBy: "count(*) desc",
            limit: numberItem)
        .then((resultMappings) {
      if (resultMappings.isNotEmpty) {
        return resultMappings.map((e) => History.fromMap(e)).toList();
      }

      return List.empty();
    });
  }
}
