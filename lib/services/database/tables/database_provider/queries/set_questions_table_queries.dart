part of '../db_provider.dart';

extension SetQuestionsTable on DBProvider {
  fetSetQuestions(int offset, int limit, int categoryId) async {
    final db = await database;
    final query =
        "select * from set_questions where category_id = $categoryId limit $offset,$limit";
    final resultMappings = await db.rawQuery(query);
    if (resultMappings.isNotEmpty) {
      return resultMappings.map((e) => SetQuestion.fromMap(e)).toList();
    }

    return List();
  }

  Future<SetQuestion> getSetQuestion({@required int byId}) async {
    final db = await database;
    return db.query(TableNamesConstant.setQuestions,
        where: "id = ?", whereArgs: [byId]).then((resultMappings) {
      if (resultMappings.isNotEmpty) {
        return resultMappings.map((e) => SetQuestion.fromMap(e)).toList().first;
      } else {
        throw ("Empty data for set_question_id $byId");
      }
    }).catchError((e) {
      debugPrint(e);
    });
  }
}
