part of '../db_provider.dart';

extension QuestionsTable on DBProvider {
  fetQuestions(int setQuestionId) async {
    final db = await database;
    final query =
        "select * from questions where set_question_id = $setQuestionId order by id";
    final resultMappings = await db.rawQuery(query);
    if (resultMappings.isNotEmpty) {
      final List<Question> questions =
          resultMappings.map((e) => Question.fromMap(e)).toList();
      for (var index = 0; index < questions.length; index++) {
        final answers = await fetchAnswers(questions[index].id);
        questions[index].answers = answers;
      }
      return questions;
    }

    return List();
  }

  Future<List<Question>> fetchAllQuestions() async {
    final db = await database;
    return db.query(TableNamesConstant.questions).then((resultMappings) {
      if (resultMappings.isNotEmpty) {
        final List<Question> questions =
            resultMappings.map((e) => Question.fromMap(e)).toList();
        return questions;
      }

      return List();
    });
  }
}
