part of '../db_provider.dart';

extension CategoriesTable on DBProvider {
  fetchCategoriesList(int offset, int limit, CategoryType type) async {
    final isLearning = type == CategoryType.learning ? 1 : 0;
    final db = await database;
    final resultMappings = await db.rawQuery(
        "select * from categories where is_learning_type=$isLearning");
    if (resultMappings.isNotEmpty) {
      return resultMappings.map((e) => Category.fromMap(e)).toList();
    }

    return List();
  }

  fetchCategoryTitle(int id) async {
    final db = await database;
    final resultMappings = await db
        .query(TableNamesConstant.categories, where: "id = ?", whereArgs: [id]);
    if (resultMappings.isNotEmpty) {
      final title =
          resultMappings.map((e) => Category.fromMap(e)).toList().first.title;
      return title;
    }

    return "";
  }
}
