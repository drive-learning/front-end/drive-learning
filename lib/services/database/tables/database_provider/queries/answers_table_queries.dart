part of '../db_provider.dart';

extension AnswersTable on DBProvider {
  fetchAnswers(int questionId) async {
    final db = await database;
    final query = "select * from answers where question_id = $questionId";
    final resultMappings = await db.rawQuery(query);
    if (resultMappings.isNotEmpty) {
      return resultMappings.map((e) => Answer.fromMap(e)).toList();
    }

    return List();
  }
}
