import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:sqflite/sqlite_api.dart';
import 'package:vn_drive_learning/services/database/tables/constant/quries_constant.dart';
import 'package:vn_drive_learning/services/database/tables/constant/tables_name_constant.dart';
import 'package:vn_drive_learning/services/database/tables/infos/tables_info.dart';
import 'package:vn_drive_learning/services/entity/answer.dart';
import 'package:vn_drive_learning/services/entity/category.dart';
import 'package:vn_drive_learning/services/entity/categoryType.dart';
import 'package:vn_drive_learning/services/entity/history.dart';
import 'package:vn_drive_learning/services/entity/question.dart';
import 'package:vn_drive_learning/services/entity/set_question.dart';
import 'package:vn_drive_learning/services/entity/user_info/user_info.dart';

part 'queries/histories_table_queries.dart';
part 'queries/questions_table_queries.dart';
part 'queries/answers_table_queries.dart';
part 'queries/userinfo_table_queries.dart';
part 'queries/categories_table_queries.dart';
part 'queries/set_questions_table_queries.dart';

class DBProvider {
  DBProvider._();

  static final DBProvider db = DBProvider._();

  Database _database;

  Future<Database> get database async {
    if (_database == null) {
      _database = await initDatabase();
    }

    return _database;
  }

  createDatabse() async {
    if (_database == null) {
      _database = await initDatabase();
      debugPrint(_database.toString());
    }
  }

  static int get version => 1;

  initDatabase() async {
    Directory directory = await getApplicationDocumentsDirectory();
    var databasePath = join(directory.path, "learn_drive.sqlite");
    debugPrint(databasePath);
    if (FileSystemEntity.typeSync(databasePath) ==
        FileSystemEntityType.notFound) {
      ByteData data = await rootBundle.load("file_db/learn_drive.sqlite");
      await writeToFile(data, databasePath);
    }
    return await openDatabase(
      databasePath,
      version: version,
      onCreate: (db, version) async {
        await db.execute(QueriesConstant.createUserInfoTable);
      },
    );
  }

  Future writeToFile(ByteData data, String path) async {
    final buffer = data.buffer;
    await File(path).writeAsBytes(
        buffer.asUint8List(data.offsetInBytes, data.lengthInBytes));
  }
}
