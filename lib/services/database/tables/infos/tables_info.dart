import 'package:vn_drive_learning/services/database/tables/histories/histories_info_fields.dart';
import 'package:vn_drive_learning/services/database/tables/user_info/user_info_fields.dart';

class TablesInfo {
  static final UserInfoTableFields userInfo = UserInfoTableFields();
  static final HistoriesInfoFields histories = HistoriesInfoFields();
}
