import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:vn_drive_learning/services/entity/settings_data.dart';

class LocalStorageServiceType {
  void setIsFirstTimeLunching({bool value}) {}
  bool get isFirstTimelunching {
    return false;
  }

  void updateSettings({SettingsData settings}) {}
  SettingsData get settingsData => throw UnimplementedError();
}

class LocalStorageService implements LocalStorageServiceType {
  static LocalStorageService _instance;
  static SharedPreferences _preferences;

  static Future<LocalStorageService> getInstance() async {
    if (_instance == null) {
      _instance = LocalStorageService();
    }

    if (_preferences == null) {
      _preferences = await SharedPreferences.getInstance();
    }

    return _instance;
  }

  @override
  void setIsFirstTimeLunching({bool value}) async {
    await _preferences.setBool(LocalStorageKeys.isFirstTimeLunching, value);
  }

  @override
  bool get isFirstTimelunching {
    var value = _preferences.getBool(LocalStorageKeys.isFirstTimeLunching);
    if (value == null) {
      return true;
    }

    return value;
  }

  @override
  void updateSettings({SettingsData settings}) async {
    String json = jsonEncode(settings.toJson());
    await _preferences.setString(LocalStorageKeys.settingsKey, json);
  }

  @override
  SettingsData get settingsData {
    String jsonString =
        _preferences.getString(LocalStorageKeys.settingsKey) ?? "";
    if (jsonString.isEmpty) {
      return SettingsData.defaultSettings;
    }

    Map json = jsonDecode(jsonString);
    return SettingsData.fromMap(json);
  }
}

class LocalStorageKeys {
  static final SettingsStorageKeys settings = SettingsStorageKeys();
  static final isFirstTimeLunching = "is_first_time_lunching";
  static final settingsKey = "settings";
}

class SettingsStorageKeys {
  final volumeStatus = "volume_status";
  final currentSeletectLanguage = "current_selected_language";
  final repeatQuestionStatus = "repeat_questions";
  final notificationStatus = "notification_status";
}
