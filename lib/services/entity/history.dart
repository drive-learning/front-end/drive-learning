import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:vn_drive_learning/common/constant/color/colors_constant.dart';
import 'package:vn_drive_learning/common/constant/images/icons_constant.dart';
import 'package:vn_drive_learning/services/entity/test_result.dart';

class History {
  final int id;
  final String description;
  final String setQuestionName;
  final double testResult;
  final double testResultType;
  final DateTime testTime;
  final int setQuestionId;
  final int categoryId;

  const History(
      {this.id,
      this.description,
      this.setQuestionName,
      this.testResult,
      this.testResultType,
      this.testTime,
      this.setQuestionId,
      this.categoryId});

  factory History.fromMap(Map<String, dynamic> json) {
    return History(
        id: json["id"],
        description: json["description"],
        setQuestionName: json["set_question_name"],
        testResult: json["test_result"],
        testResultType: json["test_result_type"],
        testTime: DateTime.fromMillisecondsSinceEpoch(
            (json["test_time"] * 1000).round(),
            isUtc: false),
        setQuestionId: json["set_question_id"],
        categoryId: json["category_id"]);
  }

  Map<String, dynamic> toMap() {
    return {
      'description': description,
      'set_question_name': setQuestionName,
      'test_result': testResult,
      'test_result_type': testResultType,
      'test_time': (DateTime.now().millisecondsSinceEpoch / 1000).round(),
      'set_question_id': setQuestionId,
      'category_id': categoryId,
      'updated_at': (DateTime.now().millisecondsSinceEpoch / 1000).round(),
      'created_at': (DateTime.now().millisecondsSinceEpoch / 1000).round()
    };
  }

  TestResultType get getTestResultType {
    if (testResultType == 0) {
      return TestResultType.failed;
    }

    return TestResultType.passed;
  }
}

enum RankingType { winner, second, thirth, other }

class HistoryViewModel {
  final History history;

  const HistoryViewModel({@required this.history});

  String get title {
    return "${history.setQuestionName} ";
  }

  String get moreDescription {
    /// failed case
    if (history.testResultType == 0) {
      return "thất bại";
    }

    if (history.testResult <= 0.9) {
      return "Thành công";
    }

    if (history.testResult > 0.9) {
      return "Thật sự suất sắc";
    }

    return "Trên cả tuyệt vời";
  }

  Color get textColor {
    return Colors.white;
  }

  Color get progressColor {
    switch (history.getTestResultType) {
      case TestResultType.passed:
        return ColorsContant.passsedColor;

      default:
        return ColorsContant.failedColor;
    }
  }

  /// convert from time in date time to time in string
  String get timeInStandartFormator {
    final timerFormated = DateFormat('dd-MM-yy hh:mm');
    return timerFormated.format(history.testTime);
  }

  Color get categoryColor {
    final List<Color> listColors = [
      Colors.yellow[800],
      Colors.pinkAccent[400],
      Colors.blue[600],
      Color(0xff2ECC71)
    ];

    return listColors[history.categoryId % listColors.length];
  }

  String get status {
    switch (history.getTestResultType) {
      case TestResultType.failed:
        return "thất bại";
      case TestResultType.passed:
        return "Thành công";
      case TestResultType.warning:
        return "warning";
    }

    return "";
  }

  String get categoryIconPath {
    List<String> categoriesIcon = [
      IconsConstant.a1Category,
      IconsConstant.a2Category,
      IconsConstant.b2Category
    ];
    return categoriesIcon[history.categoryId % categoriesIcon.length];
  }
}
