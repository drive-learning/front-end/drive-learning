class Answer {
  int id;
  String content;
  bool isCorrect;

  Answer({this.id, this.content, this.isCorrect});

  factory Answer.fromMap(Map<String, dynamic> json) {
    return Answer(
        id: json["id"],
        content: json["content"],
        isCorrect: json["is_correct"] == 1 ? true : false);
  }
}
