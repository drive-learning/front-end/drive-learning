import 'package:flutter/material.dart';
import 'package:vn_drive_learning/modules/histories/test_histories_page.dart';
import 'package:vn_drive_learning/modules/loading_page/loading_page.dart';
import 'package:vn_drive_learning/modules/settings/settings_page_view.dart';
import 'package:vn_drive_learning/services/database/tables/database_provider/db_provider.dart';
import 'package:vn_drive_learning/services/entity/user_info/user_info.dart';

class NavigationKeys {
  static final String historiesPage = "/History";
  static final String settingsPage = "/Settings";
  static final String quickTestPage = "/QuickTest";
}

class NavigationRoutes {
  static final routes = <String, WidgetBuilder>{
    NavigationKeys.historiesPage: (BuildContext context) => TestHistoriesPage(
          hasFloatingButton: true,
        ),
    NavigationKeys.settingsPage: (BuildContext context) => SettingsPageView(),
    NavigationKeys.quickTestPage: (BuildContext context) =>
        QuickTestLoadingPage()
  };
}

class SetQuestionRadomer {
  static getRadomSetQuestion() async {
    final int categoryId = UserInfoSingleton.shared.licenseType.index + 1;
    final setQuestions =
        await DBProvider.db.fetSetQuestions(0, 999, categoryId);
    return setQuestions[4];
  }
}
