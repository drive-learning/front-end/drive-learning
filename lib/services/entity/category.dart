import 'package:vn_drive_learning/services/entity/categoryType.dart';

class Category {
  int id;
  String title;
  String description;
  String updateStatus;
  String imageName;
  CategoryType type;

  Category(
      {this.id,
      this.title,
      this.description,
      this.updateStatus,
      this.imageName,
      this.type});

  factory Category.fromMap(Map<String, dynamic> json) {
    return Category(
        id: json["id"],
        title: json["title"],
        description: json["description"],
        updateStatus: json["update_status"],
        imageName: json["image_name"],
        type: json["is_learning_type"] == 1
            ? CategoryType.learning
            : CategoryType.testing);
  }
}
