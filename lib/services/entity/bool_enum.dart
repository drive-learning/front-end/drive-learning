class BoolEnum {
  bool _value;

  BoolEnum(this._value);

  bool get isOn => _value == true;
  bool get isOff => _value == false;

  BoolEnum get switchStatus {
    _value = !_value;
    return this;
  }

  bool get value => _value;

  bool setValue(bool value) {
    _value = value;
  }
}
