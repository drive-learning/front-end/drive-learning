import 'package:vn_drive_learning/services/entity/bool_enum.dart';
import 'package:vn_drive_learning/services/entity/language_type.dart';
import 'package:vn_drive_learning/services/storage/local_storage_service.dart';

class SettingsData {
  BoolEnum volumeStatus;
  BoolEnum repeatQuestionStatus;
  BoolEnum notificationStatus;
  LanguageType languageType;

  static SettingsData defaultSettings = SettingsData(
      languageType: LanguageType.vietnamess,
      notificationStatus: BoolEnum(true),
      repeatQuestionStatus: BoolEnum(true),
      volumeStatus: BoolEnum(true));

  SettingsData(
      {this.volumeStatus,
      this.repeatQuestionStatus,
      this.notificationStatus,
      this.languageType});

  factory SettingsData.fromMap(Map<String, dynamic> json) {
    return SettingsData(
        volumeStatus: BoolEnum(json[LocalStorageKeys.settings.volumeStatus]),
        notificationStatus:
            BoolEnum(json[LocalStorageKeys.settings.notificationStatus]),
        repeatQuestionStatus:
            BoolEnum(json[LocalStorageKeys.settings.repeatQuestionStatus]),
        languageType:
            json[LocalStorageKeys.settings.currentSeletectLanguage] == "vi"
                ? LanguageType.vietnamess
                : LanguageType.english);
  }

  Map<String, dynamic> toJson() {
    return {
      LocalStorageKeys.settings.volumeStatus: this.volumeStatus.value,
      LocalStorageKeys.settings.notificationStatus:
          this.notificationStatus.value,
      LocalStorageKeys.settings.repeatQuestionStatus:
          this.repeatQuestionStatus.value,
      LocalStorageKeys.settings.currentSeletectLanguage:
          this.languageType == LanguageType.vietnamess ? "vi" : "us"
    };
  }
}
