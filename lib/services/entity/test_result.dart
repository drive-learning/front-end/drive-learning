enum TestResultType { failed, passed, warning }

class TestResult {
  int totalQuestion;
  int totalCorrectQuestion;

  TestResult({this.totalCorrectQuestion, this.totalQuestion});

  bool get isPassed {
    return (this.totalCorrectQuestion / this.totalQuestion) >= 0.8;
  }

  bool get isFailed {
    return !isPassed;
  }

  TestResultType get type {
    if (isPassed) {
      return TestResultType.passed;
    }

    return TestResultType.failed;
  }

  String get description => "$totalCorrectQuestion / $totalQuestion ";

  double get resultInDouble => totalCorrectQuestion / totalQuestion;
}
