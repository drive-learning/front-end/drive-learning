import 'package:vn_drive_learning/common/constant/animation/lotties_constant.dart';
import 'package:vn_drive_learning/services/database/tables/infos/tables_info.dart';
import 'package:vn_drive_learning/services/entity/user_info/account_tier.dart';
import 'package:vn_drive_learning/services/entity/user_info/gender.dart';
import 'package:vn_drive_learning/services/entity/user_info/license_type.dart';
import 'package:vn_drive_learning/utils/user_info/user_info_utils.dart';

class UserInfoSingleton extends LicenseTypeDisplayed {
  int _id = -1;
  String _fullName = "";
  String _userName = "";
  int _age = -1;
  LicenseType _licenseType = LicenseType.A1;
  Gender _gender = Gender.male;
  int _totalGem = 0;
  AccountTier _accountTier = AccountTier.bronze;

  // static final UserInfoSingleton _instance = UserInfoSingleton._internal();

  // factory UserInfoSingleton() => _instance;

  UserInfoSingleton._internal();

  static UserInfoSingleton shared = UserInfoSingleton._internal();

  factory UserInfoSingleton() => shared;

  void fromMap(Map<String, dynamic> json) {
    _id = json[TablesInfo.userInfo.id];
    _fullName = json[TablesInfo.userInfo.fullName];
    _userName = json[TablesInfo.userInfo.userName];
    _accountTier = UserInfoUtils.getAccountTier(
        value: json[TablesInfo.userInfo.accountTier]);
    _gender = UserInfoUtils.getGender(value: json[TablesInfo.userInfo.gender]);
    _licenseType = UserInfoUtils.getLicenseType(
      value: json[TablesInfo.userInfo.licenseType],
    );
    _totalGem = json[TablesInfo.userInfo.totalGem];
  }

  Map<String, dynamic> toMap() {
    return {
      TablesInfo.userInfo.fullName: this._fullName,
      TablesInfo.userInfo.userName: this._userName,
      TablesInfo.userInfo.accountTier:
          UserInfoUtils.accountTierToValue(accountTier: this._accountTier),
      TablesInfo.userInfo.gender:
          UserInfoUtils.genderToValue(gender: this._gender),
      TablesInfo.userInfo.licenseType:
          UserInfoUtils.licenseTypeToValue(licenseType: this._licenseType),
      TablesInfo.userInfo.totalGem: this._totalGem
    };
  }

  int get id => _id;

  String get fullName => _fullName;
  void setFullName(String newValue) => _fullName = newValue;

  String get userName => _userName;
  void setUserName(String newValue) => _userName = newValue;

  int get age => _age;
  void setAge(int newValue) {
    _age = newValue;
  }

  // LicenseType get licenseType => _licenseType;
  @override
  LicenseType get licenseType => _licenseType;
  void setLicenseType(LicenseType newValue) => _licenseType = newValue;

  Gender get gender => _gender;
  void setGender(Gender newValue) => _gender = newValue;

  AccountTier get accountTier => _accountTier;
  void setAccountTier(AccountTier newValue) => _accountTier = newValue;

  int get totalGem => _totalGem;
  void setTotalGem(int newValue) => _totalGem = newValue;

  bool get isEnoughGem => _totalGem >= 30;

  String get avatarLottieAsset {
    switch (gender) {
      case Gender.male:
        return Lottiesconstant.menAvatar;

      case Gender.female:
        return Lottiesconstant.womenAvatar;

      case Gender.other:
        return Lottiesconstant.defaultAvatar;
    }

    return "";
  }
}

class LicenseTypeDisplayed {
  LicenseType get licenseType => LicenseType.A1;
  String getLicenseDisplayedName() {
    switch (licenseType) {
      case LicenseType.A1:
        return "Bằng lái xe A1";
        break;
      case LicenseType.A2:
        return "Bằng lái xe A2";
        break;
      case LicenseType.B1:
        return "Bằng lái xe B1";
        break;
    }
    return "";
  }
}
