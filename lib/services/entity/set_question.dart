import 'package:vn_drive_learning/services/database/tables/database_provider/db_provider.dart';

class SetQuestion {
  int id;
  int categoryId;
  double learningProgress;
  String title;

  SetQuestion({this.id, this.categoryId, this.learningProgress, this.title});

  factory SetQuestion.fromMap(Map<String, dynamic> json) {
    return SetQuestion(
        id: json["id"],
        categoryId: json["category_id"],
        learningProgress: json["learning_progress"],
        title: json["title"]);
  }

  String get displayTitle {
    return "Bộ đề ${id + 1}";
  }

  categoryTitle() async {
    return await DBProvider.db.fetchCategoryTitle(categoryId);
  }
}
