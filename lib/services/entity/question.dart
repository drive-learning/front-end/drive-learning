import 'package:vn_drive_learning/common/components/question_view_type.dart';
import 'package:vn_drive_learning/services/entity/answer.dart';

class Question {
  int id;
  int setQuestionId;
  String description;
  String imageName;
  bool isHasImage;
  bool isAnswered;
  int currentSelectedAnswerIndex = -1;
  QuestionViewType currentViewMode = QuestionViewType.testing;

  List<Answer> answers;

  Question(
      {this.id,
      this.setQuestionId,
      this.description,
      this.imageName,
      this.isHasImage,
      this.answers,
      this.isAnswered = false});

  bool get isCorrect {
    return currentSelectedAnswerIndex != -1 &&
        answers[currentSelectedAnswerIndex].isCorrect;
  }

  factory Question.fromMap(Map<String, dynamic> json) {
    return Question(
        id: json["id"],
        setQuestionId: json["set_question_id"],
        description: json["description"],
        imageName: json["image_name"],
        isHasImage: json["is_has_image"] == 1 ? true : false,
        answers: List.empty());
  }

  bool get isWrongQuestion {
    return isAnswered && !isCorrect;
  }

  bool get isNotAnsweredQuestion {
    return !isAnswered;
  }

  reset() {
    isAnswered = false;
    currentSelectedAnswerIndex = -1;
  }
}
