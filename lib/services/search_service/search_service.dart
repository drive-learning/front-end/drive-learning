import 'package:flutter/material.dart';
import 'package:vn_drive_learning/services/database/tables/database_provider/db_provider.dart';
import 'package:vn_drive_learning/services/search_service/search_service_type.dart';

class SearchService implements SearchDataServiceType {
  search({@required String text, Function(List<SearchItem>) callback}) {
    DBProvider.db.fetchAllQuestions().then((value) {
      List<SearchItem> searchedItem = value
          .where((element) =>
              element.description.toLowerCase().contains(text.toLowerCase()))
          .map((e) => SearchItem(title: e.description, question: e))
          .toList();
      callback(searchedItem);
    });
  }
}

/**
 * Build Search logic form all data in database
 */
