import 'package:flutter/material.dart';
import 'package:vn_drive_learning/services/entity/question.dart';

class SearchItem {
  final String title;
  final Question question;
  const SearchItem({this.title = "", this.question});
}

class SearchDataServiceType {
  search({@required String text, Function(List<SearchItem>) callback}) {
    throw UnimplementedError();
  }
}
