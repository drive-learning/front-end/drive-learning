import 'package:vn_drive_learning/services/database/tables/database_provider/db_provider.dart';

class UserInfoRepository {
  createUserInfo() {
    return DBProvider.db.createUserInfo();
  }

  fetchUserInfo() {
    return DBProvider.db.fetchUserInfo();
  }
}
