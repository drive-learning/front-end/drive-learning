import 'package:flutter/material.dart';
import 'package:vn_drive_learning/services/database/tables/database_provider/db_provider.dart';
import 'package:vn_drive_learning/services/entity/history.dart';

final limit = 10;

class QuestionsRepository {
  fetchQuestions(int setQuestionId) {
    return DBProvider.db.fetQuestions(setQuestionId);
  }
}

class HistoriesRepository {
  createHistory(History history) {
    return DBProvider.db.createHistory(history);
  }

  fetchHistories(int offset) {
    return DBProvider.db.fetchHistories(offset, limit);
  }

  fetchTopTrendingHistories(
      {int numberItem = 3, @required DatabaseHandleCallBack completion}) {
    return DBProvider.db
        .fetchTopTrendingHistories(numberItem: numberItem)
        .then((histories) {
      if (histories.isEmpty) {
        completion.onFailure();
      } else {
        completion.onSuccess(histories);
      }
    });
  }

  fetchSetQuestion(
      {@required int id, @required DatabaseHandleCallBack completion}) {
    return DBProvider.db.getSetQuestion(byId: id).then((setQuestion) {
      completion.onSuccess(setQuestion);
    }).catchError((e) => completion.onFailure);
  }
}

class DatabaseHandleCallBack<T, E extends Error> {
  final Function(T) onSuccess;
  final Function() onFailure;

  const DatabaseHandleCallBack({this.onSuccess, this.onFailure});
}
