import 'package:vn_drive_learning/services/database/tables/database_provider/db_provider.dart';
import 'package:vn_drive_learning/services/entity/categoryType.dart';

class CategoriesRepository {
  final int limit = 10;

  fetchCategories(int offset, CategoryType type) {
    return DBProvider.db.fetchCategoriesList(offset, limit, type);
  }

  fetSetQuestions(int offset, int categoryId) {
    return DBProvider.db.fetSetQuestions(offset, limit, categoryId);
  }
}
