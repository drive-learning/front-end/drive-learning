// import 'package:admob_flutter/admob_flutter.dart';
// import 'package:firebase_admob/firebase_admob.dart';
import 'package:firebase_admob/firebase_admob.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:in_app_purchase/in_app_purchase.dart';
import 'package:vn_drive_learning/common/constant/color/colors_constant.dart';
import 'package:vn_drive_learning/locale/applocalizations.dart';
import 'package:vn_drive_learning/modules/profile_steps/profile_steps_page.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:vn_drive_learning/modules/tabbar/tabbar_view.dart';
import 'package:vn_drive_learning/services/database/tables/database_provider/db_provider.dart';
import 'package:vn_drive_learning/services/entity/navigation_keys.dart';
import 'package:vn_drive_learning/services/search_service/search_service.dart';
import 'package:vn_drive_learning/services/search_service/search_service_type.dart';
import 'package:vn_drive_learning/services/storage/local_storage_service.dart';
import 'package:vn_drive_learning/utils/adds/ad_manager/ad_id_manager.dart';
import 'package:vn_drive_learning/utils/notification_manager/notifycation_manager.dart';

GetIt localtor = GetIt.instance;
Future setupRegisteriesService() async {
  /*
    Registery Local Storage Service
   */
  localtor.registerSingleton<LocalStorageServiceType>(
      await LocalStorageService.getInstance());

  /** 
   *  Registery UserInfo Service
  **/

  /**
   * Resgitery Search Service
   */
  localtor.registerSingleton<SearchDataServiceType>(SearchService());
}

Future<void> _initAdMob() {
  return FirebaseAdMob.instance.initialize(appId: AdManager.appId);
}

configureInAppPurchase() {
  InAppPurchaseConnection.enablePendingPurchases();
}

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await setupRegisteriesService();
  await DBProvider.db.fetchUserInfo();
  // await _initAdMob();
  configureInAppPurchase();
  runApp(MyApp());
  await ScheduleNotificationsController.startSchedule();
}

final GlobalKey<NavigatorState> navigatorKey = new GlobalKey<NavigatorState>();

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "Demo",
      routes: NavigationRoutes.routes,
      navigatorKey: navigatorKey,
      theme: ThemeData(
          primaryColor: ColorsContant.appPrimaryColor,
          scaffoldBackgroundColor: ColorsContant.appBackgroundColor,
          textTheme: Theme.of(context)
              .textTheme
              .apply(bodyColor: ColorsContant.appTextColor)),
      supportedLocales: [Locale('vi', 'VN'), Locale('en', 'US')],
      localeResolutionCallback: (locale, supportedLocales) {
        for (var supportedLocale in supportedLocales) {
          if (supportedLocale.languageCode == locale.languageCode &&
              supportedLocale.countryCode == locale.countryCode) {
            return supportedLocale;
          }
        }
        return supportedLocales.first;
      },
      localizationsDelegates: [
        AppLocalizations.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate
      ],
      home: localtor<LocalStorageServiceType>().isFirstTimelunching
          ? ProfileStepsPage()
          : MainTabbarView(),
    );
  }
}
