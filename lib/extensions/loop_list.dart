extension LoopList<T> on List {
  T loop(int index) => this[index % this.length];
}
