import 'package:flutter/material.dart';
import 'package:vn_drive_learning/services/entity/user_info/account_tier.dart';
import 'package:vn_drive_learning/services/entity/user_info/gender.dart';
import 'package:vn_drive_learning/services/entity/user_info/license_type.dart';

class UserInfoUtils {
  static LicenseType getLicenseType({@required int value}) {
    if (value == 1) {
      return LicenseType.A1;
    }

    if (value == 2) {
      return LicenseType.A2;
    }

    return LicenseType.A2;
  }

  static int licenseTypeToValue({@required LicenseType licenseType}) {
    switch (licenseType) {
      case LicenseType.A1:
        return 1;

      case LicenseType.A2:
        return 2;
        break;
      default:
        return 3;
    }
  }

  static Gender getGender({@required int value}) {
    switch (value) {
      case 0:
        return Gender.male;
      case 1:
        return Gender.female;

      default:
        return Gender.other;
    }
  }

  static int genderToValue({@required Gender gender}) {
    switch (gender) {
      case Gender.male:
        return 0;
      case Gender.female:
        return 1;
      default:
        return 2;
    }
  }

  static AccountTier getAccountTier({@required int value}) {
    switch (value) {
      case 1:
        return AccountTier.bronze;

      case 2:
        return AccountTier.silver;

      default:
        return AccountTier.gold;
    }
  }

  static int accountTierToValue({@required AccountTier accountTier}) {
    switch (accountTier) {
      case AccountTier.bronze:
        return 1;

      case AccountTier.silver:
        return 2;
      case AccountTier.gold:
      default:
        return 3;
    }
  }
}
