import 'package:vn_drive_learning/utils/sounds_managaer/sound_interface.dart';
import 'package:vn_drive_learning/utils/sounds_managaer/sound_type.dart';
import 'package:vn_drive_learning/utils/sounds_managaer/sounds/correct_answer_sound.dart';
import 'package:vn_drive_learning/utils/sounds_managaer/sounds/loser_sound.dart';
import 'package:vn_drive_learning/utils/sounds_managaer/sounds/selected_answer_sound.dart';
import 'package:vn_drive_learning/utils/sounds_managaer/sounds/winner_sound.dart';
import 'package:vn_drive_learning/utils/sounds_managaer/sounds/wrong_answer_sound.dart';

class SoundsManager {
  static final SoundsManager shared = SoundsManager();

  play({SoundType soundType}) async {
    switch (soundType) {
      case SoundType.correctAnswer:
        CorrectAnswerSound().play();
        break;

      case SoundType.wrongAnswer:
        WrongAnswerSound().play();
        break;

      case SoundType.seletedAnswer:
        SlectedAnswerSound().play();
        break;

      case SoundType.winner:
        WinnerSound().play();
        break;

      case SoundType.loser:
        Losersound().play();
        break;
    }
  }
}
