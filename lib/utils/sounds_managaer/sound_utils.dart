import 'package:audioplayers/audio_cache.dart';
import 'package:vn_drive_learning/main.dart';
import 'package:vn_drive_learning/services/storage/local_storage_service.dart';

class SoundUtils {
  final AudioCache audioPlayer = AudioCache();
  final storageService = localtor<LocalStorageServiceType>();

  SoundUtils._internal();

  static SoundUtils shared = SoundUtils._internal();
  factory SoundUtils() => shared;

  play({String resourcePath}) async {
    if (storageService.settingsData.volumeStatus.isOn) {
      await audioPlayer.play(resourcePath);
    }
  }
}
