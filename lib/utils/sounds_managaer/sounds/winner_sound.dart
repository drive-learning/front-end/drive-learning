import 'package:vn_drive_learning/common/constant/sound/sounds_constant.dart';
import 'package:vn_drive_learning/utils/sounds_managaer/sound_interface.dart';

class WinnerSound extends SoundInterface {
  @override
  String get soundResourcePath => Soundsconstant.passedSounds;
}
