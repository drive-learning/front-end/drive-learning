import 'package:vn_drive_learning/utils/sounds_managaer/sound_utils.dart';

class SoundInterface {
  String soundResourcePath = "";

  play() {
    SoundUtils.shared.play(resourcePath: soundResourcePath);
  }
}
