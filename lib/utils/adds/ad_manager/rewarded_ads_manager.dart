import 'package:firebase_admob/firebase_admob.dart';
import 'package:flutter/material.dart';
import 'package:vn_drive_learning/utils/adds/ad_manager/ad_id_manager.dart';

class RewardedVideoAdManager {
  RewardedVideoAdManager._internal();

  static RewardedVideoAdManager instance = RewardedVideoAdManager._internal();
  factory RewardedVideoAdManager() => instance;

  bool _isRewardedAdReady = false;

  get isRewardedAdReady => _isRewardedAdReady;

  /*
   * Please set value for 3 fucntion bellow before  called for show rewarded 
   */
  VoidCallback onRewarded;
  VoidCallback onClosed;
  VoidCallback onReload;

  void configureRewardedAd() {
    _isRewardedAdReady = false;
    RewardedVideoAd.instance.listener = _onRewardedAdEvent;
    _loadRewardedAd();
  }

  void _loadRewardedAd() {
    RewardedVideoAd.instance.load(
        adUnitId: AdManager.rewardedAdUnitId,
        targetingInfo: MobileAdTargetingInfo());
  }

  void _onRewardedAdEvent(RewardedVideoAdEvent event,
      {String rewardType, int rewardAmount}) {
    debugPrint("💊💊💊 Reward video event ${event.toString()}  💊💊💊");
    switch (event) {
      case RewardedVideoAdEvent.loaded:
        if (onReload != null) {
          onReload();
        }
        _isRewardedAdReady = true;
        break;

      case RewardedVideoAdEvent.failedToLoad:
        if (onReload != null) {
          onReload();
        }
        _isRewardedAdReady = false;
        break;

      case RewardedVideoAdEvent.closed:
        if (onReload != null) {
          onReload();
        }
        _isRewardedAdReady = false;
        _loadRewardedAd();
        break;

      case RewardedVideoAdEvent.rewarded:
        if (onReload != null) {
          onReload();
        }

        _isRewardedAdReady = false;
        if (onRewarded != null) {
          onRewarded();
        }
        break;

      default:
        break;
    }
  }
}
