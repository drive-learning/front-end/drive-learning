import 'package:flutter/foundation.dart';

class MobileAdEventHandler {
  final VoidCallback onLoaded;
  final VoidCallback onFailedToLoad;
  final VoidCallback onClicked;
  final VoidCallback onImpression;
  final VoidCallback onOpened;
  final VoidCallback onLeftApplication;
  final VoidCallback onClosed;
  final VoidCallback onRewarded;
  final VoidCallback onStarted;
  final VoidCallback onCompleted;

  const MobileAdEventHandler(
      {this.onLoaded,
      this.onFailedToLoad,
      this.onClicked,
      this.onImpression,
      this.onOpened,
      this.onLeftApplication,
      this.onClosed,
      this.onRewarded,
      this.onStarted,
      this.onCompleted});
}
