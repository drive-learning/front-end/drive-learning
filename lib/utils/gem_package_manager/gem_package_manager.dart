import 'package:flutter/material.dart';
import 'package:vn_drive_learning/services/database/tables/database_provider/db_provider.dart';
import 'package:vn_drive_learning/services/entity/user_info/user_info.dart';

class GemPackageManager {
  final int _gemRewarded = 30;
  VoidCallback changedDataCallBack;
  VoidCallback onClosedReward;

  GemPackageManager._internal();

  static GemPackageManager shared = GemPackageManager._internal();

  factory GemPackageManager() => shared;

  void onIncreasing({int gemIncreasing, VoidCallback callback}) {
    UserInfoSingleton.shared
        .setTotalGem(UserInfoSingleton.shared.totalGem + gemIncreasing);
    DBProvider.db
        .updateUserGem(UserInfoSingleton.shared.totalGem)
        .then((value) {
      if (callback != null) {
        callback();
      }
    });
  }

  void onDecrasing({int gemDecreasing, VoidCallback callback}) {
    UserInfoSingleton.shared
        .setTotalGem(UserInfoSingleton.shared.totalGem - gemDecreasing);
    DBProvider.db
        .updateUserGem(UserInfoSingleton.shared.totalGem)
        .then((value) {
      if (callback != null) {
        callback();
      }
    });
  }

  void onBought() {}

  int get currentTotalGem => UserInfoSingleton.shared.totalGem;

  bool get isEnounghtGemForLearn => currentTotalGem >= this._gemRewarded;
  bool get isNotEnounghtGemForLearn => currentTotalGem < this._gemRewarded;

  // void useGemForLearn(VoidCallback callback) {
  //   if (isEnounghtGemForLearn) {
  //     onDecrasing(gemDecreasing: this._gemRewarded, callback: callback);
  //   } else {
  //     RewardedVideoAd.instance.show();
  //   }
  // }

  int get gemPaymentPerLearningIime => this._gemRewarded;
}

class DatabaseUpdateResponseCallBack {
  final VoidCallback onSuccess;
  final VoidCallback onFailed;

  const DatabaseUpdateResponseCallBack(
      {@required this.onSuccess, @required this.onFailed});
}

enum GemPackage { ten, oneHundred, thousand }
