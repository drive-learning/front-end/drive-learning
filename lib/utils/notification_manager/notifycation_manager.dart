import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
// import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:timezone/data/latest.dart' as tz;
import 'package:timezone/timezone.dart' as tz;
import 'package:vn_drive_learning/main.dart';

class NotificationManager {
  NotificationManager._internal();

  static NotificationManager shared = NotificationManager._internal();

  factory NotificationManager() => shared;

  ///
  /// this is setting for flutter notification
  ///
  // FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;
  // AndroidInitializationSettings androidInitializationSettings;
  // IOSInitializationSettings iosInitializationSettings;
  // InitializationSettings initializationSettings;

  void initializing() async {
    // flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
    // androidInitializationSettings = AndroidInitializationSettings('app_icon');
    // iosInitializationSettings = IOSInitializationSettings(
    //   onDidReceiveLocalNotification: onDidReceiveLocalNotification,
    // );
    // initializationSettings = InitializationSettings(
    //     android: androidInitializationSettings, iOS: iosInitializationSettings);
    // await flutterLocalNotificationsPlugin.initialize(initializationSettings,
    //     onSelectNotification: onSelectNotification);
  }

  MethodChannel platform =
      MethodChannel('dexterx.dev/flutter_local_notifications_example');

  Future<void> configureLocalTimeZone() async {
    tz.initializeTimeZones();
    final String timeZoneName = 'Asia/Ho_Chi_Minh';
    tz.setLocalLocation(tz.getLocation(timeZoneName));
  }

  void showNotifications() async {
    await notifications();
  }

  Future<void> notifications() async {
    // AndroidNotificationDetails androidNotificationDetails =
    //     AndroidNotificationDetails(
    //         'channelId', 'channelName', 'channelDescription',
    //         priority: Priority.high,
    //         importance: Importance.max,
    //         ticker: 'test');
    //
    // IOSNotificationDetails iosNotificationDetails = IOSNotificationDetails();
    //
    // NotificationDetails notificationDetails = NotificationDetails(
    //     android: androidNotificationDetails, iOS: iosNotificationDetails);
    // await flutterLocalNotificationsPlugin.show(0, 'Hello there',
    //     'Please subscribe channel for more', notificationDetails);
  }

  Future onSelectNotification(String payLoad) async {
    if (payLoad != null) {
      navigatorKey.currentState.pushNamed(payLoad);
    }
  }

  Future onDidReceiveLocalNotification(
      int id, String title, String body, String payload) async {
    return CupertinoAlertDialog(
      title: Text(title),
      content: Text(body),
      actions: [
        CupertinoDialogAction(
          child: Text("OK"),
          isDefaultAction: true,
          onPressed: () {
            print("on preset");
          },
        )
      ],
    );
  }

  Future<void> repeatNotification() async {
    //   const AndroidNotificationDetails androidPlatformChannelSpecifics =
    //       AndroidNotificationDetails('repeating channel id',
    //           'repeating channel name', 'repeating description');
    //   const NotificationDetails platformChannelSpecifics =
    //       NotificationDetails(android: androidPlatformChannelSpecifics);
    //   await flutterLocalNotificationsPlugin.periodicallyShow(0, 'repeating title',
    //       'repeating body', RepeatInterval.everyMinute, platformChannelSpecifics,
    //       androidAllowWhileIdle: true, payload: NavigationKeys.quickTestPage);
  }

  tz.TZDateTime _nextInstanceOfTenAM() {
    final tz.TZDateTime now = tz.TZDateTime.now(tz.local);
    tz.TZDateTime scheduledDate =
        tz.TZDateTime(tz.local, now.year, now.month, now.day, 10);
    if (scheduledDate.isBefore(now)) {
      scheduledDate = scheduledDate.add(const Duration(days: 1));
    }
    return scheduledDate;
  }

  Future<void> scheduleDialyTenAMNotification() async {
    // await flutterLocalNotificationsPlugin.zonedSchedule(
    //     0,
    //     'daily scheduled notification title',
    //     'daily scheduled notification body',
    //     _nextInstanceOfTenAM(),
    //     const NotificationDetails(
    //       android: AndroidNotificationDetails(
    //           'daily notification channel id',
    //           'daily notification channel name',
    //           'daily notification description'),
    //     ),
    //     androidAllowWhileIdle: true,
    //     uiLocalNotificationDateInterpretation:
    //         UILocalNotificationDateInterpretation.absoluteTime);
  }
}

class ScheduleNotificationsController {
  static startSchedule() async {
    await NotificationManager.shared.configureLocalTimeZone();
    NotificationManager.shared.initializing();
    await NotificationManager.shared.repeatNotification();
    // await NotificationManager.shared.scheduleDialyTenAMNotification();
  }
}
