import 'package:flutter/material.dart';

enum DSButtonType {
  primarySuccess,
  primaryFailed,
  primaryWarning,
  priamaryInfo
}

enum DSButtonIconPositon { left, right }

class DSButtonIconfig {
  final DSButtonIconPositon position;
  final String assertUrl;

  const DSButtonIconfig({this.position, this.assertUrl});
}

class DSButtonConfig {
  final DSButtonType type;
  final String title;
  final DSButtonIconfig iconConfig;
  final VoidCallback onTap;

  DSButtonIconPositon imagePosition;

  DSButtonConfig(
      {@required this.type,
      @required this.title,
      this.imagePosition,
      this.iconConfig,
      this.onTap})
      : assert(onTap != null);

  setImagePosition({DSButtonIconPositon positon}) {
    imagePosition = positon;
  }

  Color get backgroundColor {
    switch (type) {
      case DSButtonType.primarySuccess:
        return Colors.greenAccent;
      case DSButtonType.primaryFailed:
        return Colors.red;
      case DSButtonType.primaryWarning:
        return Colors.yellow[800];

      case DSButtonType.priamaryInfo:
        break;
    }

    return Colors.greenAccent;
  }
}
