import 'package:flutter/material.dart';
import 'package:vn_drive_learning/design_system/ds_button/ds_button_config.dart';

class DSButton extends StatelessWidget {
  final DSButtonConfig config;

  DSButton({@required this.config});
  @override
  Widget build(Object context) {
    return InkWell(
      onTap: () {
        config.onTap();
      },
      child: Container(
          decoration: BoxDecoration(
              color: config.backgroundColor,
              borderRadius: BorderRadius.circular(8),
              boxShadow: [
                BoxShadow(
                    offset: Offset(0, 10),
                    blurRadius: 10,
                    color: Colors.pink[200].withOpacity(0.25))
              ]),
          child: Center(
            child: Text(
              config.title.toUpperCase(),
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.bold,
              ),
            ),
          )),
    );
  }
}
