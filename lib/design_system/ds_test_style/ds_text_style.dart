import 'package:flutter/material.dart';
import 'package:vn_drive_learning/design_system/ds_test_style/text_styles/failed_text_style.dart';
import 'package:vn_drive_learning/design_system/ds_test_style/text_styles/successed_text_style.dart';
import 'package:vn_drive_learning/design_system/ds_test_style/text_styles/title_text_style.dart';

abstract class DSTextStyleInterface {
  TextStyle getStyle();

  TextStyle getStyleWithColor({Color color});
}

class DSTextStyleProvider {
  static final shared = DSTextStyleProvider();

  TextStyle get successedTitle => SuccessedTextStyle().getStyle();
  TextStyle get failedTitle => FailedTextStyle().getStyle();
  TextStyle get titleStyle => TitleTextStyle().getStyle();
  TextStyle get subTitleStyle => SubTitleStyle().getStyle();
  TextStyle get descriptionStyle => DescriptionStyle().getStyle();

  TextStyle titleSyleWith(Color color) =>
      TitleTextStyle().getStyleWithColor(color: color);
}
