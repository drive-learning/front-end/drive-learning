import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:vn_drive_learning/design_system/ds_test_style/ds_text_style.dart';

class SuccessedTextStyle implements DSTextStyleInterface {
  @override
  TextStyle getStyle() {
    return TextStyle(
        color: Colors.green, fontSize: 16, fontWeight: FontWeight.bold);
  }

  @override
  TextStyle getStyleWithColor({Color color}) {
    return getStyle();
  }
}
