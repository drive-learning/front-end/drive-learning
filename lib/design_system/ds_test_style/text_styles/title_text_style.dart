import 'package:flutter/material.dart';
import 'package:vn_drive_learning/design_system/ds_test_style/ds_text_style.dart';

class TitleTextStyle implements DSTextStyleInterface {
  @override
  TextStyle getStyle() {
    return TextStyle(
        color: Colors.blueGrey[900], fontSize: 16, fontWeight: FontWeight.w600);
  }

  @override
  TextStyle getStyleWithColor({Color color}) {
    return TextStyle(color: color, fontSize: 16, fontWeight: FontWeight.w600);
  }
}

class SubTitleStyle implements DSTextStyleInterface {
  @override
  TextStyle getStyle() {
    return TextStyle(
        color: Colors.blueGrey[900],
        fontSize: 13.5,
        fontWeight: FontWeight.w500);
  }

  @override
  TextStyle getStyleWithColor({Color color}) {
    return getStyle();
  }
}

class DescriptionStyle implements DSTextStyleInterface {
  @override
  TextStyle getStyle() {
    return TextStyle(
        color: Colors.grey[600].withOpacity(0.5),
        fontSize: 12,
        fontWeight: FontWeight.w400);
  }

  @override
  TextStyle getStyleWithColor({Color color}) {
    return getStyle();
  }
}
