import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'ds_bottom_sheet_config.dart';

class DSBottomSheetModalFit extends StatelessWidget {
  final DSBottomSheetConfig config;
  final ScrollController scrollController;

  const DSBottomSheetModalFit({Key key, this.scrollController, this.config})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
        child: CupertinoPageScaffold(
            navigationBar: _buildNavigationBar(),
            child: SafeArea(
                top: true,
                child: SingleChildScrollView(
                  child: Column(
                    children: <Widget>[
                      SizedBox(
                        height: 16,
                      ),
                      config.bottomSheetView,
                    ],
                  ),
                ))));
  }

  Widget _buildNavigationBar() {
    /// Build left item view
    Widget leftWidget = Center(
      child: _buildNavigationItem(config.leftItem, isLeftItem: true),
    );

    /// Build right item view
    Widget rightWidget = Padding(
      padding: EdgeInsets.zero,
      child: _buildNavigationItem(config.rightItem),
    );

    Widget titleWidget = Text(
      config.title,
      style: TextStyle(fontWeight: FontWeight.w500, fontSize: 14),
    );

    /// Default
    ///
    /// ```
    /// No has left item
    /// No has right item
    /// ```
    if (config.leftItem == null && config.rightItem == null) {
      return CupertinoNavigationBar(
        automaticallyImplyLeading: false,
        middle: titleWidget,
      );
    }

    /// Has right item
    ///
    /// ```
    /// No has left item
    /// Has right item
    /// ```
    if (config.leftItem == null) {
      return CupertinoNavigationBar(
        trailing: rightWidget,
        middle: titleWidget,
      );
    }

    /// Has left item
    ///
    /// ```
    /// Has left item
    /// No has right item
    ///
    if (config.rightItem == null) {
      return CupertinoNavigationBar(
        leading: leftWidget,
        middle: titleWidget,
      );
    }

    /// Full case
    ///
    /// ```
    /// Has left item
    /// Has right item
    ///
    return CupertinoNavigationBar(
      leading: leftWidget,
      middle: titleWidget,
      trailing: rightWidget,
    );
  }

  Widget _buildNavigationItem(DSBottomSheetNavItem item,
      {bool isLeftItem = false}) {
    if (item == null) {
      return Container();
    }

    TextStyle textStyle = TextStyle(
        fontWeight: FontWeight.w200, fontSize: 12, color: Colors.blue[300]);
    if (isLeftItem) {
      textStyle = TextStyle(fontWeight: FontWeight.w500, fontSize: 14);
    }

    /// Case navigation bar item is icon
    switch (item.type) {
      case DSBottomSheetNavItemType.icon:
        return InkWell(
          onTap: item.callback,
          child: Center(
              child: Image.asset(
            item.assetName,
            width: 24,
            height: 24,
          )),
        );
        break;

      /// Case navigation bar item is text
      case DSBottomSheetNavItemType.text:
        return InkWell(
            onTap: item.callback,
            child: Padding(
              padding: const EdgeInsets.only(right: 24),
              child: Text(
                item.title,
                style: textStyle,
              ),
            ));
        break;
    }

    return Container();
  }
}
