import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';

import 'ds_bottom_sheet_config.dart';
import 'ds_bottom_sheet_modal_fit.dart';

class DSBottomSheetProvider {
  static DSBottomSheetProvider get shared => DSBottomSheetProvider();

  Future showBottomSheet(BuildContext context, DSBottomSheetConfig config) {
    if (config.useDefaultNavigation) {
      return showBarModalBottomSheet(
        context: context,
        expand: false,
        bounce: true,
        duration: Duration(milliseconds: 200),
        elevation: 0,
        isDismissible: true,
        useRootNavigator: true,
        builder: (context, scrollController) {
          return DSBottomSheetModalFit(
            key: UniqueKey(),
            scrollController: scrollController,
            config: config,
          );
        },
      );
    }

    return showBarModalBottomSheet(
      context: context,
      expand: false,
      useRootNavigator: false,
      builder: (context, scrollController) {
        return config.bottomSheetView;
      },
    );
  }
}
