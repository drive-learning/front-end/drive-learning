import 'package:flutter/material.dart';

enum DSBottomSheetNavItemType { icon, text }

class DSBottomSheetNavItem {
  DSBottomSheetNavItemType type;

  String assetName;
  String title;
  VoidCallback callback;

  DSBottomSheetNavItem(this.type, {this.assetName, this.title, this.callback});
}

class DSBottomSheetConfig {
  DSBottomSheetNavItem leftItem;
  DSBottomSheetNavItem rightItem;
  String title;
  Widget bottomSheetView;
  bool useDefaultNavigation;

  DSBottomSheetConfig(this.bottomSheetView,
      {this.leftItem,
      this.title = "",
      this.rightItem,
      this.useDefaultNavigation = true});
}
