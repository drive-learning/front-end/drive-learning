import 'package:flutter/material.dart';
import 'package:path/path.dart';
import 'package:vn_drive_learning/design_system/ds_alert/alerts/confirm_alert/ds_alert_config.dart';
import 'package:vn_drive_learning/design_system/ds_alert/alerts/confirm_alert/ds_confirm_alert.dart';
import 'package:vn_drive_learning/design_system/ds_alert/alerts/purchase_gem_alert/purchase_point_alert.dart';
import 'package:vn_drive_learning/design_system/ds_alert/alerts/purchase_gem_alert/purchase_point_alert_config.dart';
import 'package:vn_drive_learning/design_system/ds_alert/alerts/test_result_alert/test_result_alert.dart';
import 'package:vn_drive_learning/design_system/ds_alert/alerts/test_result_alert/test_result_alert_config.dart';
import 'package:vn_drive_learning/design_system/ds_alert/alerts/user_info_edit_alert/user_info_edit_alert.dart';
import 'package:vn_drive_learning/design_system/ds_alert/alerts/user_info_edit_alert/config/user_info_edit_alert_config.dart';

class DSAlert {
  static DSAlert get shared => DSAlert();

  Future showConfirmAlert(BuildContext context,
      {@required DSConfirmAlertConfig config}) {
    return showDialog(
      context: context,
      builder: (context) {
        return DSConfirmAlert(
          config: config,
        );
      },
    );
  }

  Future showTestResultAlert(BuildContext context, DSTestResultConfig config) {
    return showDialog(
      context: context,
      builder: (context) {
        return DSTestResultAlert(config: config);
      },
    );
  }

  Future showUserInfoEditForm(
      BuildContext context, DSUserInfoEditAlertConfig config) {
    return showDialog(
      context: context,
      builder: (context) {
        return DSUserInfoEditFormAlert(
          config: config,
        );
      },
    );
  }

  Future showPointPurchaseGemAlert(BuildContext context,
      {@required DSPurchasePointAlertConfig config}) {
    return showDialog(
      context: context,
      builder: (context) => DSPurchasePointConfirmAlert(
        config: config,
      ),
    );
  }
}
