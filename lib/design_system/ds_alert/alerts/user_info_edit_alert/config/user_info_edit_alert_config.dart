import 'package:flutter/material.dart';

class DSUserInfoEditAlertConfig {
  final VoidCallback onUpdateData;

  const DSUserInfoEditAlertConfig({@required this.onUpdateData});
}
