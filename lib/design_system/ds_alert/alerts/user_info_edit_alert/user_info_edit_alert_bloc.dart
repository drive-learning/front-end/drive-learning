import 'dart:async';

import 'package:vn_drive_learning/common/base/bloc_base.dart';
import 'package:vn_drive_learning/modules/profile_steps/profile_steps_bloc.dart';
import 'package:vn_drive_learning/services/entity/user_info/gender.dart';
import 'package:vn_drive_learning/services/entity/user_info/license_type.dart';
import 'package:vn_drive_learning/services/entity/user_info/user_info.dart';

class DSUserInfoEditFormAlertBloc extends BlocBase {
  StreamController _controller = StreamController();
  Stream get stream => _controller.stream;

  List<GenderOptionViewModel> _genderOptions =
      Gender.values.map((e) => GenderOptionViewModel(gender: e)).toList();
  int currentSelectedGenderIndex = 0;
  List<GenderOptionViewModel> get genders => _genderOptions;

  List<LicenseOptionViewModel> _licenseOptions = LicenseType.values
      .map((e) => LicenseOptionViewModel(licenseType: e))
      .toList();
  List<LicenseOptionViewModel> get licenses => _licenseOptions;
  int currentSelectedLicenseIndex = 0;

  initialization() {
    currentSelectedGenderIndex = _genderOptions
        .map((e) => e.gender)
        .toList()
        .indexOf(UserInfoSingleton.shared.gender);

    currentSelectedLicenseIndex = _licenseOptions
        .map((e) => e.licenseType)
        .toList()
        .indexOf(UserInfoSingleton.shared.licenseType);
    _controller.sink.add(true);
  }

  onUpdateSeletedGender({int selectedIndex}) {
    if (currentSelectedGenderIndex == selectedIndex) {
      return;
    }

    UserInfoSingleton.shared.setGender(_genderOptions[selectedIndex].gender);
    currentSelectedGenderIndex = selectedIndex;
    _controller.sink.add(true);
  }

  onUpdateSeletedLicense({int selectedIndex}) {
    if (currentSelectedLicenseIndex == selectedIndex) {
      return;
    }

    UserInfoSingleton.shared
        .setLicenseType(_licenseOptions[selectedIndex].licenseType);
    currentSelectedLicenseIndex = selectedIndex;
    _controller.sink.add(true);
  }

  @override
  void dispose() {
    _controller.close();
  }
}
