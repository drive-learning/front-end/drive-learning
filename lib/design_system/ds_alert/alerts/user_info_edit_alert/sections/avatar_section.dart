part of '../user_info_edit_alert.dart';

extension _AvatarSection on _DSUserInfoEditFormAlertState {
  Column _buildAvatarInfoView(BuildContext context) {
    return Column(
      children: [
        Container(
            // margin: const EdgeInsets.all(16),
            decoration: BoxDecoration(
              color: Colors.transparent,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(16), topRight: Radius.circular(16)),
            ),
            height: 200,
            child: Stack(children: [
              ClipRRect(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(16),
                    topRight: Radius.circular(16)),
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  height: 136,
                  decoration: BoxDecoration(
                    color: Colors.red,
                  ),
                  child: ClipRRect(
                    child: Container(
                      color: Colors.blue[200],
                    ),
                  ),
                ),
              ),
              Positioned(
                top: 72,
                left: 0,
                right: 0,
                child: Center(
                  child: Container(
                    height: 128,
                    width: 128,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(64),
                        border: Border.all(color: Colors.white, width: 2.5)),
                    child: Lottie.asset(
                        UserInfoSingleton.shared.avatarLottieAsset,
                        fit: BoxFit.fill),
                  ),
                ),
              )
            ])),
      ],
    );
  }
}
