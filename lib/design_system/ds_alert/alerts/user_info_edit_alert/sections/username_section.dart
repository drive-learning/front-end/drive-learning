part of '../user_info_edit_alert.dart';

extension _UserNameSection on _DSUserInfoEditFormAlertState {
  Widget _buildUserNameEditView() {
    return Container(
      padding: const EdgeInsets.all(24),
      child: TextField(
        maxLength: 15,
        controller: TextEditingController()
          ..text = UserInfoSingleton.shared.userName,
        decoration: InputDecoration(
          hintStyle: TextStyle(
              fontStyle: FontStyle.italic,
              color: Colors.grey[400],
              fontWeight: FontWeight.w300),
          hintText: "Enter your name",
          border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(8),
              borderSide: BorderSide(
                  color: Colors.grey[600], style: BorderStyle.solid)),
        ),
        onSubmitted: (value) {
          UserInfoSingleton.shared.setUserName(value);
          widget.config.onUpdateData();
        },
        keyboardType: TextInputType.name,
      ),
    );
  }
}
