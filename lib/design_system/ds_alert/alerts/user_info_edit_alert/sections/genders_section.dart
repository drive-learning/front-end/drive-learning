part of '../user_info_edit_alert.dart';

extension _GendersSection on _DSUserInfoEditFormAlertState {
  Widget _buildGendersSection() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(
          height: 24,
        ),
        Padding(
          padding: const EdgeInsets.only(left: 24),
          child: Text(
            AppLocalizations.of(context).translate(AppLocalizationKeys.gender),
            style: TextStyle(
                color: Colors.grey[800],
                fontSize: 20,
                fontWeight: FontWeight.w500),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 8),
          child: ChipsChoice<int>.single(
              spacing: 16,
              value: _bloc.currentSelectedGenderIndex,
              onChanged: (value) {
                _bloc.onUpdateSeletedGender(selectedIndex: value);
                widget.config.onUpdateData();
              },
              choiceItems: C2Choice.listFrom<int, GenderOptionViewModel>(
                source: _bloc.genders,
                value: (index, item) => index,
                label: (index, item) => item.getDisplayedName(),
              )),
        ),
        SizedBox(
          height: 36,
        )
      ],
    );
  }
}
