part of '../user_info_edit_alert.dart';

extension _LicensesTypeSection on _DSUserInfoEditFormAlertState {
  Widget _buildLicensesTypeSection() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(
          height: 8,
        ),
        Padding(
          padding: const EdgeInsets.only(left: 24),
          child: Text(
            AppLocalizations.of(context)
                .translate(AppLocalizationKeys.licenseType),
            style: TextStyle(
                color: Colors.grey[800],
                fontSize: 20,
                fontWeight: FontWeight.w500),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 8),
          child: ChipsChoice<int>.single(
              spacing: 16,
              value: _bloc.currentSelectedLicenseIndex,
              onChanged: (value) {
                _bloc.onUpdateSeletedLicense(selectedIndex: value);
                widget.config.onUpdateData();
              },
              choiceItems: C2Choice.listFrom<int, LicenseOptionViewModel>(
                source: _bloc.licenses,
                value: (index, item) => index,
                label: (index, item) => item.getDisplayedName(),
              )),
        ),
      ],
    );
  }
}
