import 'package:chips_choice/chips_choice.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:vn_drive_learning/common/constant/animation/lotties_constant.dart';
import 'package:vn_drive_learning/common/constant/locale/app_localization_keys.dart';
import 'package:vn_drive_learning/design_system/ds_alert/alerts/user_info_edit_alert/config/user_info_edit_alert_config.dart';
import 'package:vn_drive_learning/design_system/ds_alert/alerts/user_info_edit_alert/user_info_edit_alert_bloc.dart';
import 'package:vn_drive_learning/locale/applocalizations.dart';
import 'package:vn_drive_learning/modules/profile_steps/profile_steps_bloc.dart';
import 'package:vn_drive_learning/services/entity/user_info/user_info.dart';

part 'sections/avatar_section.dart';
part 'sections/username_section.dart';
part 'sections/licenses_type_section.dart';
part 'sections/genders_section.dart';

class _DSUserInfoEditFormAlertState extends State<DSUserInfoEditFormAlert> {
  DSUserInfoEditFormAlertBloc _bloc;

  @override
  void initState() {
    _bloc = DSUserInfoEditFormAlertBloc()..initialization();
    super.initState();
  }

  @override
  Widget build(Object context) {
    // final double alertHeight = MediaQuery.of(context).size.height / 2;
    final double alertWidth = MediaQuery.of(context).size.width - 4;

    return StreamBuilder(
      stream: _bloc.stream,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return SimpleDialog(
            elevation: 0,
            contentPadding: const EdgeInsets.all(0),
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(16)),
            children: [
              Container(
                // margin: const EdgeInsets.symmetric(horizontal: 24, vertical: 24),
                width: alertWidth,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(16),
                  color: Colors.white,
                ),

                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      _buildAvatarInfoView(context),
                      _buildUserNameEditView(),
                      _buildLicensesTypeSection(),
                      _buildGendersSection()
                    ],
                  ),
                ),
              )
            ],
          );
        }

        return SimpleDialog();
      },
    );
  }
}

class DSUserInfoEditFormAlert extends StatefulWidget {
  final DSUserInfoEditAlertConfig config;

  const DSUserInfoEditFormAlert({@required this.config});
  @override
  State<StatefulWidget> createState() {
    return _DSUserInfoEditFormAlertState();
  }
}
