import 'package:flutter/material.dart';
import 'package:vn_drive_learning/common/constant/images/icons_constant.dart';
import 'package:vn_drive_learning/design_system/ds_alert/alerts/purchase_gem_alert/purchase_point_alert_config.dart';

class DSPurchasePointConfirmAlert extends StatelessWidget {
  final DSPurchasePointAlertConfig config;

  const DSPurchasePointConfirmAlert({@required this.config});
  @override
  Widget build(BuildContext context) {
    return SimpleDialog(
      contentPadding: const EdgeInsets.all(0),
      children: [
        Container(
          padding: const EdgeInsets.all(16),
          child: Column(
            children: [
              /**
               * title view
               */
              Container(
                height: 36,
                child: Row(
                  children: [
                    Image.asset(
                      IconsConstant.notification,
                      fit: BoxFit.fill,
                      width: 16,
                      height: 16,
                    ),
                    SizedBox(
                      width: 8,
                    ),
                    Text(
                      "Thông báo",
                      style: TextStyle(
                          color: Colors.blueGrey,
                          fontSize: 16,
                          fontWeight: FontWeight.w800),
                    )
                  ],
                ),
              ),
              SizedBox(height: 16),
              Text(
                "Bạn có muốn dùng ${config.neededPaymentTotalGem} gem để tham gia kiểm tra nhanh hơn không ???",
                style: TextStyle(
                    color: Colors.blueGrey,
                    fontSize: 16,
                    fontWeight: FontWeight.w500),
              ),
              SizedBox(
                height: 28,
              ),
              InkWell(
                onTap: () {
                  debugPrint("⛳️⛳️⛳️ on acceptet purhcase gem ⛳️⛳️⛳️");
                  config.onConfirm();
                  Navigator.pop(context);
                },
                child: Container(
                  height: 48,
                  child: Center(
                      child: Container(
                          height: 48,
                          width: 164,
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                              color: Colors.green,
                              borderRadius: BorderRadius.circular(24),
                              boxShadow: [
                                BoxShadow(
                                    offset: Offset(0, 2),
                                    blurRadius: 4,
                                    color: Colors.greenAccent[200])
                              ]),
                          child: Text("OK",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 20,
                                  fontWeight: FontWeight.w500)))),
                ),
              )
            ],
          ),
        )
      ],
    );
  }
}
