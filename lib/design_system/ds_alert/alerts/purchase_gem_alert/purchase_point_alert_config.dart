import 'package:flutter/material.dart';

class DSPurchasePointAlertConfig {
  final VoidCallback onConfirm;
  final int neededPaymentTotalGem;

  const DSPurchasePointAlertConfig(
      {@required this.onConfirm, @required this.neededPaymentTotalGem});
}
