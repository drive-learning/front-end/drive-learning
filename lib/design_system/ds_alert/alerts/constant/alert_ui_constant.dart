class DSAlertUIConstant {
  static final double horizontalPadding = 24.0;
  static final double alertContentCircularRadius = 16.0;
}

class DSTestResulAlerttUIConstant {
  static final double resultBannerHeight = 120;
  static final double resultContentHeight = 240;
  static final double emotionPadding = 4.0;
}
