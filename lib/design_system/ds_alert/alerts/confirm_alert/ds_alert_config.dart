import 'package:flutter/material.dart';

class DSConfirmAlertConfig {
  final String title;
  final String message;
  final AlertAcitonConfig confirmAction;
  final AlertAcitonConfig cancelAction;

  DSConfirmAlertConfig(
      {this.title, this.message, this.confirmAction, this.cancelAction});
}

class AlertAcitonConfig {
  final String title;
  final VoidCallback onTap;

  AlertAcitonConfig({this.title, this.onTap});
}
