import 'package:flutter/material.dart';
import 'package:vn_drive_learning/common/constant/images/icons_constant.dart';
import 'package:vn_drive_learning/design_system/ds_alert/ds_alert_package.dart';

class DSConfirmAlert extends StatelessWidget {
  final DSConfirmAlertConfig config;
  const DSConfirmAlert({Key key, this.config}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SimpleDialog(
      contentPadding: const EdgeInsets.all(0),
      children: [
        Container(
          padding: const EdgeInsets.all(16),
          child: Column(
            children: [
              Container(
                height: 36,
                child: Row(
                  children: [
                    Image.asset(
                      IconsConstant.notification,
                      fit: BoxFit.fill,
                      width: 16,
                      height: 16,
                    ),
                    SizedBox(
                      width: 8,
                    ),
                    Text(
                      "Thông báo",
                      style: TextStyle(
                          color: Colors.blueGrey,
                          fontSize: 16,
                          fontWeight: FontWeight.w800),
                    )
                  ],
                ),
              ),
              SizedBox(height: 8),
              Text(
                config.message,
                style: TextStyle(fontSize: 13.5, color: Colors.grey[600]),
              ),
              SizedBox(
                height: 16,
              ),
              Container(
                height: 48,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    DSConfirmButton(
                      title: config.confirmAction.title,
                      backgroundColor: Colors.green,
                      onTap: config.confirmAction.onTap,
                    ),
                    SizedBox(width: 16),
                    DSConfirmButton(
                      title: config.cancelAction.title,
                      backgroundColor: Colors.red[400],
                      onTap: config.cancelAction.onTap,
                    )
                  ],
                ),
              )
            ],
          ),
        )
      ],
    );
  }
}

class DSConfirmButton extends StatelessWidget {
  final String title;
  final Color backgroundColor;
  final VoidCallback onTap;
  const DSConfirmButton(
      {Key key,
      @required this.title,
      this.backgroundColor = Colors.green,
      @required this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        onTap();
      },
      child: Container(
        height: 36,
        width: 116,
        padding: const EdgeInsets.all(8),
        decoration: BoxDecoration(
            color: backgroundColor,
            borderRadius: BorderRadius.circular(16),
            boxShadow: [
              BoxShadow(
                  offset: Offset(0, 3),
                  blurRadius: 6,
                  color: backgroundColor.withOpacity(0.35))
            ]),
        child: Text(
          title,
          style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
          textAlign: TextAlign.center,
          overflow: TextOverflow.ellipsis,
          softWrap: false,
        ),
      ),
    );
  }
}
