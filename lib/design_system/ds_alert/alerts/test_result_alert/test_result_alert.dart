import 'package:flutter/material.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:shimmer/shimmer.dart';
import 'package:vn_drive_learning/common/constant/images/icons_constant.dart';
import 'package:vn_drive_learning/design_system/ds_alert/alerts/confirm_alert/ds_confirm_alert.dart';
import 'package:vn_drive_learning/design_system/ds_alert/alerts/test_result_alert/test_result_alert_config.dart';
import 'package:vn_drive_learning/design_system/ds_button/ds_button.dart';
import 'package:vn_drive_learning/design_system/ds_button/ds_button_config.dart';

class DSTestResultAlert extends StatelessWidget {
  final DSTestResultConfig config;

  const DSTestResultAlert({@required this.config});

  @override
  Widget build(BuildContext context) {
    config.playSound();
    return SimpleDialog(
      contentPadding: const EdgeInsets.all(0),
      children: [
        Container(
            padding: const EdgeInsets.all(16),
            child: Column(children: [
              Container(
                height: 36,
                child: Row(
                  children: [
                    Image.asset(
                      IconsConstant.notification,
                      fit: BoxFit.fill,
                      width: 16,
                      height: 16,
                    ),
                    SizedBox(
                      width: 8,
                    ),
                    Text(
                      config.testResultTitle,
                      style: TextStyle(
                          color: Colors.blueGrey,
                          fontSize: 16,
                          fontWeight: FontWeight.w800),
                    ),
                    SizedBox(
                      width: 24,
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 4,
              ),
              Row(
                children: [
                  Flexible(
                    child: Text(
                      config.testResultFullDescription,
                      style: TextStyle(fontSize: 14.5, color: Colors.blueGrey),
                    ),
                  ),
                  SizedBox(
                    width: 8,
                  ),
                  Container(
                    width: 92,
                    height: 92,
                    child: CircularPercentIndicator(
                      radius: 80,
                      animation: true,
                      backgroundColor: Colors.grey[200],
                      progressColor: config.progresColor,
                      lineWidth: 8,
                      percent: config.testResult,
                      circularStrokeCap: CircularStrokeCap.round,
                      center: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            config.testResultDescription.toUpperCase(),
                            style: TextStyle(
                                color: Colors.grey[700],
                                fontWeight: FontWeight.w600,
                                fontSize: 14),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 8,
              ),
              Container(
                height: 48,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    DSConfirmButton(
                      title: "Thi lại",
                      onTap: config.onTapTryAgain,
                      backgroundColor: Colors.yellow[800],
                    ),
                    SizedBox(
                      width: 16,
                    ),
                    DSConfirmButton(
                      title: "Xem lịch sử",
                      onTap: config.onTapViewHistories,
                      backgroundColor: Colors.red[400],
                    )
                  ],
                ),
              ),
              SizedBox(
                height: 8,
              ),
              InkWell(
                onTap: () {
                  config.onTapViewResult();
                },
                child: Container(
                  height: 56,
                  width: 248,
                  child: Stack(
                    children: [
                      Container(
                        height: 48,
                        decoration: BoxDecoration(
                            gradient: LinearGradient(
                                begin: Alignment.centerLeft,
                                end: Alignment.centerRight,
                                colors: [Colors.teal, Colors.green]),
                            borderRadius: BorderRadius.circular(16)),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              "Xem kết quả".toUpperCase(),
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold),
                              textAlign: TextAlign.center,
                            ),
                          ],
                        ),
                      ),
                      Shimmer(
                          child: Container(
                            height: 48,
                            decoration: BoxDecoration(
                                color: Colors.teal,
                                borderRadius: BorderRadius.circular(16)),
                          ),
                          gradient: LinearGradient(
                              begin: Alignment.topLeft,
                              end: Alignment.bottomRight,
                              stops: [
                                0.4,
                                0.5,
                                0.6
                              ],
                              colors: [
                                Colors.white.withOpacity(0),
                                Colors.white.withOpacity(0.7),
                                Colors.white.withOpacity(0)
                              ])),
                    ],
                  ),
                ),
              )
            ]))
      ],
    );
  }

  Padding _buildActions(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: Container(
        height: 48,
        child: Row(
          children: [
            Expanded(
              child: DSButton(
                  config: DSButtonConfig(
                type: DSButtonType.primaryFailed,
                title: "Thi lại",
                onTap: () {
                  Navigator.pop(context);
                  config.onTapTryAgain();
                },
              )),
            ),
            SizedBox(
              width: 16,
            ),
            Expanded(
              child: DSButton(
                  config: DSButtonConfig(
                type: DSButtonType.primaryFailed,
                title: "Xem lịch sử",
                onTap: () {
                  Navigator.pop(context);
                  config.onTapViewHistories();
                },
              )),
            ),
          ],
        ),
      ),
    );
  }
}
