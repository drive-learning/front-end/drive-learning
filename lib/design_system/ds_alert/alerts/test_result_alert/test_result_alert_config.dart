import 'package:flutter/material.dart';
import 'package:vn_drive_learning/common/constant/animation/lotties_constant.dart';
import 'package:vn_drive_learning/design_system/ds_test_style/ds_text_style.dart';
import 'package:vn_drive_learning/services/entity/test_result.dart';
import 'package:vn_drive_learning/utils/sounds_managaer/sound_type.dart';
import 'package:vn_drive_learning/utils/sounds_managaer/sounds_manager.dart';

class DSTestResultConfig {
  /// - `type`
  ///
  /// Consist 2 state of type is:
  ///   - `success` for show `success` test alert type
  ///   - `failed` for show `falied` test aler type
  final TestResultType type;

  /// `testresult`
  ///
  /// Test result of a test
  /// The `test result's maximum` value is is `1.0`
  /// The `test restult's minimum` vakue is `zero`
  final double testResult;

  /// `testResultDescription`
  ///
  /// The `description` will be display at `center test result ui`
  /// for make more infomation about test result
  final String testResultDescription;

  final VoidCallback onTapTryAgain;
  final VoidCallback onTapViewHistories;
  final VoidCallback onTapViewResult;

  const DSTestResultConfig(
      {@required this.type,
      this.testResult = 0.0,
      this.testResultDescription,
      this.onTapTryAgain,
      this.onTapViewHistories,
      this.onTapViewResult})
      : assert(testResult < 1.0);

  String get lottieEmotion {
    switch (type) {
      case TestResultType.passed:
        return Lottiesconstant.winnerCup;
        break;

      case TestResultType.failed:
        return Lottiesconstant.losedEmotion;
      default:
        return "";
    }
  }

  String get testResultFullDescription {
    switch (type) {
      case TestResultType.failed:
        return "Chắc chỉ là chưa may mắn thô :(( cố gắng hơn bạn mình nhé";
      case TestResultType.passed:
        return "Bạn yêu mến. Chúc mừng bạn đã suất sắc hoàn thành bài thi. Thi thật tốt bạn nhé";
      case TestResultType.warning:
        return "";
    }

    return "";
  }

  String get lottieBackground {
    switch (type) {
      case TestResultType.passed:
        return Lottiesconstant.winnerBackground;
        break;

      default:
        return "";
    }
  }

  String get testResultTitle {
    switch (type) {
      case TestResultType.passed:
        return "Suất sắc vượt qua";
        break;

      case TestResultType.failed:
        return "Thất bại";
      default:
        return "";
    }
  }

  Color get progresColor {
    switch (type) {
      case TestResultType.passed:
        return Colors.greenAccent;

      case TestResultType.failed:
        return Colors.red[600];
      default:
        return Colors.yellow[400];
    }
  }

  TextStyle get titleTextStyle {
    switch (type) {
      case TestResultType.passed:
        return DSTextStyleProvider.shared.successedTitle;
        break;

      case TestResultType.failed:
        return DSTextStyleProvider.shared.failedTitle;
      default:
        return DSTextStyleProvider.shared.failedTitle;
    }
  }

  playSound() {
    switch (type) {
      case TestResultType.passed:
        SoundsManager.shared.play(soundType: SoundType.winner);
        break;

      case TestResultType.failed:
        SoundsManager.shared.play(soundType: SoundType.loser);
        break;
      default:
        break;
    }
  }
}
