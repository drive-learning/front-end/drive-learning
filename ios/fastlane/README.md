fastlane documentation
================
# Installation

Make sure you have the latest version of the Xcode command line tools installed:

```
xcode-select --install
```

Install _fastlane_ using
```
[sudo] gem install fastlane -NV
```
or alternatively using `brew install fastlane`

# Available Actions
### create_app
```
fastlane create_app
```
Create on Developer Portal an App Store Connect

----

## iOS
### ios signing
```
fastlane ios signing
```
Sync singing certification for Apple Store
### ios build
```
fastlane ios build
```
Build binary for applicaton
### ios release
```
fastlane ios release
```
Release binayr for application

----

This README.md is auto-generated and will be re-generated every time [fastlane](https://fastlane.tools) is run.
More information about fastlane can be found on [fastlane.tools](https://fastlane.tools).
The documentation of fastlane can be found on [docs.fastlane.tools](https://docs.fastlane.tools).
